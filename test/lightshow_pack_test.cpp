#include <gmock/gmock-actions.h>
#include <gmock/gmock-cardinalities.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <piwo/frame.h>
#include <piwo/proto.h>
#include <piwo/protodef.h>
#include <system_error>

#include "frame_provider.hpp"
#include "network.hpp"

using namespace testing;

static constexpr uint8_t DEFAULT_TTF = 50;

class fake_tx_eth_connection : public tx_eth_connection
{
public:
  using packet_buffer_t = std::basic_string<std::byte>;
  using packet_queue_t = std::vector<packet_buffer_t>;

public:
  MOCK_METHOD(void, enqueue, (packet_t packet));

public:
  packet_queue_t packet_buffer;
};

struct fake_provider : public frame_provider
{
  explicit fake_provider(piwo::frames_intersection::frame_t frame)
    : frame_provider(false)
    , dummy_name("fake_provider")
    , dummy_frame(std::move(frame))
  {
  }

  id
  get_name() override
  {
    return this->dummy_name;
  }

  piwo::frames_intersection::frame_t
  get_frame_() override
  {
    return this->dummy_frame;
  }

  void resize_(size_t, size_t) override{

  };

  std::string dummy_name;
  piwo::frames_intersection::frame_t dummy_frame;
};

template<typename... Ts>
std::array<std::byte, sizeof...(Ts)>
make_bytes(Ts&&... bytes)
{
  return { std::byte(std::forward<Ts>(bytes))... };
}

TEST(lightshow_config_test,
     lightshow_config_correctly_extracts_pixels_based_on_owned_info1)
{
  using piwo::alloc_frame;
  using piwo::color;
  using piwo::raw_packet;
  using packet_buffer_t = fake_tx_eth_connection::packet_buffer_t;

  fake_tx_eth_connection connection;

  EXPECT_CALL(connection, enqueue(_))
    .Times(Exactly(4))
    .WillRepeatedly(Invoke(
      [&connection](raw_packet packet)
      {
        packet_buffer_t buffer;
        buffer.resize(packet.size());
        std::copy(std::begin(packet), std::end(packet), std::begin(buffer));

        connection.packet_buffer.emplace_back(std::move(buffer));
      }));

  /*
   * Prepare frame with config layout:
   * -------------
   * |R1|R2|R3|R4|
   * |R1|R2|R3|R4|
   * |R1|R2|R3|R4|
   * -------------
   *
   * And color layout:
   * -------------
   * |RR|GG|BB|WW|
   * |RR|GG|BB|WW|
   * |RR|GG|BB|WW|
   * -------------
   * Where:
   *   RR = .r = 0xFF, .g = 0x00, .b = 0x00
   *   GG = .r = 0x00, .g = 0xFF, .b = 0x00
   *   BB = .r = 0x00, .g = 0x00, .b = 0xFF
   *   WW = .r = 0xFF, .g = 0xFF, .b = 0xFF
   */
  constexpr int r1_id = 0;
  constexpr int r2_id = 1;
  constexpr int r3_id = 2;
  constexpr int r4_id = 3;

  const auto frame = piwo::alloc_frame_shared(4, 3);

  frame->at_(0, 0) = color(0xFF, 0x00, 0x00);
  frame->at_(0, 1) = color(0xFF, 0x00, 0x00);
  frame->at_(0, 2) = color(0xFF, 0x00, 0x00);

  frame->at_(1, 0) = color(0x00, 0xFF, 0x00);
  frame->at_(1, 1) = color(0x00, 0xFF, 0x00);
  frame->at_(1, 2) = color(0x00, 0xFF, 0x00);

  frame->at_(2, 0) = color(0x00, 0x00, 0xFF);
  frame->at_(2, 1) = color(0x00, 0x00, 0xFF);
  frame->at_(2, 2) = color(0x00, 0x00, 0xFF);

  frame->at_(3, 0) = color(0xFF, 0xFF, 0xFF);
  frame->at_(3, 1) = color(0xFF, 0xFF, 0xFF);
  frame->at_(3, 2) = color(0xFF, 0xFF, 0xFF);

  tx_pixel_owner_info own_info_r1, own_info_r2, own_info_r3, own_info_r4;

  own_info_r1.own(*frame, 0, 0);
  own_info_r1.own(*frame, 0, 1);
  own_info_r1.own(*frame, 0, 2);

  own_info_r2.own(*frame, 1, 0);
  own_info_r2.own(*frame, 1, 1);
  own_info_r2.own(*frame, 1, 2);

  own_info_r3.own(*frame, 2, 0);
  own_info_r3.own(*frame, 2, 1);
  own_info_r3.own(*frame, 2, 2);

  own_info_r4.own(*frame, 3, 0);
  own_info_r4.own(*frame, 3, 1);
  own_info_r4.own(*frame, 3, 2);

  const size_t r1_first_la = 0;
  const size_t r2_first_la = r1_first_la + own_info_r1.owned_offsets.size();
  const size_t r3_first_la = r2_first_la + own_info_r2.owned_offsets.size();
  const size_t r4_first_la = r3_first_la + own_info_r3.owned_offsets.size();

  lightshow_config config_r1{ .device_offset = r1_first_la,
                              .owner_info = own_info_r1,
                              .id = { 0, r1_id },
                              .frame = frame };

  lightshow_config config_r2{ .device_offset = r2_first_la,
                              .owner_info = own_info_r2,
                              .id = { 0, r2_id },
                              .frame = frame };

  lightshow_config config_r3{ .device_offset = r3_first_la,
                              .owner_info = own_info_r3,
                              .id = { 0, r3_id },
                              .frame = frame };

  lightshow_config config_r4{ .device_offset = r4_first_la,
                              .owner_info = own_info_r4,
                              .id = { 0, r4_id },
                              .frame = frame };

  send_frame_via_lightshow(config_r1, connection);
  send_frame_via_lightshow(config_r2, connection);
  send_frame_via_lightshow(config_r3, connection);
  send_frame_via_lightshow(config_r4, connection);

  constexpr auto expected_fw_type = piwo::packet_type::FORWARD_W_CID;
  constexpr auto expected_ls_type = piwo::packet_type::LIGHT_SHOW;
  constexpr auto expected_ls_packet_len = 13;
  constexpr auto expected_ttf = DEFAULT_TTF;
  constexpr auto expected_fw_packet_len_first_byte =
    expected_ls_packet_len + piwo::forward_w_cid_without_any_packet_length +
    piwo::forward_w_cid_cid_size;
  constexpr auto expected_fw_packet_len_second_byte = 0;

  /* Check expectation of lightshow send to R1 */
  // clang-format off
  const auto expected_ls_r1 = make_bytes(

    expected_fw_type,
    expected_fw_packet_len_first_byte,
    expected_fw_packet_len_second_byte,
    r1_id,
    /* Header */
    expected_ls_type,
    expected_ls_packet_len,
    expected_ttf,
    r1_first_la,

    /* Color 1 */
    0xff, 0x0, 0x0,

    /* Color 2 */
    0xff, 0x0, 0x0,

    /* Color 3 */
    0xff, 0x0, 0x0
  );
  // clang-format on

  const auto& ls_r1 = connection.packet_buffer[0];
  EXPECT_THAT(ls_r1, ElementsAreArray(expected_ls_r1));

  /* Check expectation of lightshow send to R2 */
  // clang-format off
  const auto expected_ls_r2 = make_bytes(

    expected_fw_type,
    expected_fw_packet_len_first_byte,
    expected_fw_packet_len_second_byte,
    r2_id,
    /* Header */
    expected_ls_type,
    expected_ls_packet_len,
    expected_ttf,
    r2_first_la,

    /* Color 1 */
    0x0, 0xff, 0x0,

    /* Color 1 */
    0x0, 0xff, 0x0,

    /* Color 1 */
    0x0, 0xff, 0x0
  );
  // clang-format on

  const auto& ls_r2 = connection.packet_buffer[1];
  EXPECT_THAT(ls_r2, ElementsAreArray(expected_ls_r2));

  /* Check expectation of lightshow send to R3 */
  // clang-format off
  const auto expected_ls_r3 = make_bytes(

    expected_fw_type,
    expected_fw_packet_len_first_byte,
    expected_fw_packet_len_second_byte,
    r3_id,
    /* ls Header */
    expected_ls_type,
    expected_ls_packet_len,
    expected_ttf,
    r3_first_la,

    /* Color 1 */
    0x0, 0x0, 0xff,

    /* Color 1 */
    0x0, 0x0, 0xff,

    /* Color 1 */
    0x0, 0x0, 0xff
  );
  // clang-format on

  const auto& ls_r3 = connection.packet_buffer[2];
  EXPECT_THAT(ls_r3, ElementsAreArray(expected_ls_r3));

  /* Check expectation of lightshow send to R4 */
  // clang-format off
  const auto expected_ls_r4 = make_bytes(

    expected_fw_type,
    expected_fw_packet_len_first_byte,
    expected_fw_packet_len_second_byte,
    r4_id,
    /* Header */
    expected_ls_type,
    expected_ls_packet_len,
    expected_ttf,
    r4_first_la,

    /* Color 1 */
    0xff, 0xff, 0xff,

    /* Color 1 */
    0xff, 0xff, 0xff,

    /* Color 1 */
    0xff, 0xff, 0xff
  );
  // clang-format on

  const auto& ls_r4 = connection.packet_buffer[3];
  EXPECT_THAT(ls_r4, ElementsAreArray(expected_ls_r4));
}

TEST(lightshow_config_test,
     lightshow_config_correctly_extracts_pixels_based_on_owned_info2)
{
  using piwo::alloc_frame;
  using piwo::color;
  using piwo::raw_packet;
  using packet_buffer_t = fake_tx_eth_connection::packet_buffer_t;

  fake_tx_eth_connection connection;

  EXPECT_CALL(connection, enqueue(_))
    .Times(Exactly(4))
    .WillRepeatedly(Invoke(
      [&connection](raw_packet packet)
      {
        packet_buffer_t buffer;
        buffer.resize(packet.size());
        std::copy(std::begin(packet), std::end(packet), std::begin(buffer));

        connection.packet_buffer.emplace_back(std::move(buffer));
      }));

  /*
   * Prepare frame with config layout:
   * -------------
   * |R1|R4|R3|R2|
   * |R2|R1|R4|R3|
   * |R3|R2|R1|R4|
   * -------------
   *
   * And color layout:
   * -------------
   * |RR|GG|BB|WW|
   * |RR|GG|BB|WW|
   * |RR|GG|BB|WW|
   * -------------
   * Where:
   *   RR = .r = 0xFF, .g = 0x00, .b = 0x00
   *   GG = .r = 0x00, .g = 0xFF, .b = 0x00
   *   BB = .r = 0x00, .g = 0x00, .b = 0xFF
   *   WW = .r = 0xFF, .g = 0xFF, .b = 0xFF
   */
  const auto frame = piwo::alloc_frame_shared(4, 3);
  constexpr int r1_id = 0;
  constexpr int r2_id = 1;
  constexpr int r3_id = 2;
  constexpr int r4_id = 3;

  frame->at_(0, 0) = color(0xFF, 0x00, 0x00);
  frame->at_(0, 1) = color(0xFF, 0x00, 0x00);
  frame->at_(0, 2) = color(0xFF, 0x00, 0x00);

  frame->at_(1, 0) = color(0x00, 0xFF, 0x00);
  frame->at_(1, 1) = color(0x00, 0xFF, 0x00);
  frame->at_(1, 2) = color(0x00, 0xFF, 0x00);

  frame->at_(2, 0) = color(0x00, 0x00, 0xFF);
  frame->at_(2, 1) = color(0x00, 0x00, 0xFF);
  frame->at_(2, 2) = color(0x00, 0x00, 0xFF);

  frame->at_(3, 0) = color(0xFF, 0xFF, 0xFF);
  frame->at_(3, 1) = color(0xFF, 0xFF, 0xFF);
  frame->at_(3, 2) = color(0xFF, 0xFF, 0xFF);

  tx_pixel_owner_info own_info_r1, own_info_r2, own_info_r3, own_info_r4;

  own_info_r1.own(*frame, 0, 2);
  own_info_r1.own(*frame, 1, 1);
  own_info_r1.own(*frame, 2, 0);

  own_info_r2.own(*frame, 0, 1);
  own_info_r2.own(*frame, 1, 0);
  own_info_r2.own(*frame, 3, 2);

  own_info_r3.own(*frame, 0, 0);
  own_info_r3.own(*frame, 2, 2);
  own_info_r3.own(*frame, 3, 1);

  own_info_r4.own(*frame, 1, 2);
  own_info_r4.own(*frame, 2, 1);
  own_info_r4.own(*frame, 3, 0);

  const size_t r1_first_la = 0;
  const size_t r2_first_la = r1_first_la + own_info_r1.owned_offsets.size();
  const size_t r3_first_la = r2_first_la + own_info_r2.owned_offsets.size();
  const size_t r4_first_la = r3_first_la + own_info_r3.owned_offsets.size();

  lightshow_config config_r1{ .device_offset = r1_first_la,
                              .owner_info = own_info_r1,
                              .id = { 0, r1_id },
                              .frame = frame };

  lightshow_config config_r2{ .device_offset = r2_first_la,
                              .owner_info = own_info_r2,
                              .id = { 0, r2_id },
                              .frame = frame };

  lightshow_config config_r3{ .device_offset = r3_first_la,
                              .owner_info = own_info_r3,
                              .id = { 0, r3_id },
                              .frame = frame };

  lightshow_config config_r4{ .device_offset = r4_first_la,
                              .owner_info = own_info_r4,
                              .id = { 0, r4_id },
                              .frame = frame };

  send_frame_via_lightshow(config_r1, connection);
  send_frame_via_lightshow(config_r2, connection);
  send_frame_via_lightshow(config_r3, connection);
  send_frame_via_lightshow(config_r4, connection);

  constexpr auto expected_fw_type = piwo::packet_type::FORWARD_W_CID;
  constexpr auto expected_ls_type = piwo::packet_type::LIGHT_SHOW;
  constexpr auto expected_ls_packet_len = 13;
  constexpr auto expected_fw_packet_len_first_byte =
    expected_ls_packet_len + piwo::forward_w_cid_without_any_packet_length +
    piwo::forward_w_cid_cid_size;
  constexpr auto expected_fw_packet_len_second_byte = 0;
  constexpr auto expected_ttf = DEFAULT_TTF;

  /* Check expectation of lightshow send to R1 */
  const auto expected_ls_r1 = make_bytes(

    /* Header FW*/
    expected_fw_type,
    expected_fw_packet_len_first_byte,
    expected_fw_packet_len_second_byte,
    r1_id,
    expected_ls_type,
    expected_ls_packet_len,
    expected_ttf,
    r1_first_la,

    /* Color 1 */
    0xff,
    0x00,
    0x00,

    /* Color 2 */
    0x00,
    0xff,
    0x00,

    /* Color 3 */
    0x00,
    0x00,
    0xff);
  const auto& ls_r1 = connection.packet_buffer[0];
  EXPECT_THAT(ls_r1, ElementsAreArray(expected_ls_r1));

  /* Check expectation of lightshow send to R2 */
  const auto expected_ls_r2 = make_bytes(

    expected_fw_type,
    expected_fw_packet_len_first_byte,
    expected_fw_packet_len_second_byte,
    r2_id,
    /* Header */
    expected_ls_type,
    expected_ls_packet_len,
    expected_ttf,
    r2_first_la,

    /* Color 1 */
    0xff,
    0x00,
    0x00,

    /* Color 2 */
    0x00,
    0xff,
    0x00,

    /* Color 3 */
    0xff,
    0xff,
    0xff);
  const auto& ls_r2 = connection.packet_buffer[1];
  EXPECT_THAT(ls_r2, ElementsAreArray(expected_ls_r2));

  /* Check expectation of lightshow send to R3 */
  const auto expected_ls_r3 = make_bytes(

    expected_fw_type,
    expected_fw_packet_len_first_byte,
    expected_fw_packet_len_second_byte,
    r3_id,
    /* Header */
    expected_ls_type,
    expected_ls_packet_len,
    expected_ttf,
    r3_first_la,

    /* Color 1 */
    0xff,
    0x00,
    0x00,

    /* Color 2 */
    0x00,
    0x00,
    0xff,

    /* Color 3 */
    0xff,
    0xff,
    0xff);
  const auto& ls_r3 = connection.packet_buffer[2];
  EXPECT_THAT(ls_r3, ElementsAreArray(expected_ls_r3));

  /* Check expectation of lightshow send to R4 */
  // clang-format off
  const auto expected_ls_r4 = make_bytes(

    expected_fw_type,
    expected_fw_packet_len_first_byte,
    expected_fw_packet_len_second_byte,
    r4_id,
    /* Header */
    expected_ls_type,
    expected_ls_packet_len,
    expected_ttf,
    r4_first_la,

    /* Color 1 */
    0x00, 0xff, 0x00,

    /* Color 2 */
    0x00, 0x00, 0xff,

    /* Color 3 */
    0xff, 0xff, 0xff
  );
  // clang-format on

  const auto& ls_r4 = connection.packet_buffer[3];
  EXPECT_THAT(ls_r4, ElementsAreArray(expected_ls_r4));
}
