#include <gtest/gtest.h>

#include <sstream>

#include <rapidjson/rapidjson.h>

#include "json.hpp"
#include "modinfo.hpp"

TEST(modinfo_serialization_test, modinfo_serialization_to_stream)
{
  modinfo mi = {
    .base = {
      .id =10,
      .uid = piwo::uid::from_ascii("AA:BB:CC:DD:EE:FF:AA:BB:CC:DD:EE:FF").value(),
    },
    .logic_address = 0,
    .gateway_id = 1,
    .radio_id = 2,
    .static_color = piwo::color(0xFF00FF),
    .position = {
      .x = 3,
      .y = 12
    },
  };

  std::stringstream ss;
  serialize(mi, ss);

  rapidjson::Document d;
  d.Parse(ss.str().c_str());

  EXPECT_TRUE(d.HasMember("base"));
  EXPECT_TRUE(d["base"].IsObject());

  EXPECT_TRUE(d.HasMember("logic_address"));
  EXPECT_TRUE(d["logic_address"].IsUint());
  const auto la = d["logic_address"].GetUint();
  EXPECT_TRUE(la <= 255);

  EXPECT_TRUE(d.HasMember("gateway_id"));
  EXPECT_TRUE(d["gateway_id"].IsUint());
  const auto gid = d["gateway_id"].GetUint();
  EXPECT_TRUE(gid <= 255);

  EXPECT_TRUE(d.HasMember("radio_id"));
  EXPECT_TRUE(d["radio_id"].IsUint());
  const auto rid = d["radio_id"].GetUint();
  EXPECT_TRUE(rid <= 255);

  EXPECT_TRUE(d.HasMember("static_color"));
  EXPECT_TRUE(d["static_color"].IsUint64());

  EXPECT_TRUE(d.HasMember("position"));
  EXPECT_TRUE(d["position"].IsObject());

  auto dbase = d["base"].GetObject();

  EXPECT_TRUE(dbase.HasMember("id"));
  EXPECT_TRUE(dbase["id"].IsInt64());
  EXPECT_TRUE(dbase.HasMember("uid"));
  EXPECT_TRUE(dbase["uid"].IsString());

  auto dpos = d["position"].GetObject();

  EXPECT_TRUE(dpos.HasMember("x"));
  EXPECT_TRUE(dpos["x"].IsUint64());
  EXPECT_TRUE(dpos.HasMember("y"));
  EXPECT_TRUE(dpos["y"].IsUint64());
}

TEST(modinfo_serialization_test, modinfo_deserialization_from_stream)
{
  modinfo mi;

  std::stringstream ss("{"
                       "  \"base\": {"
                       "    \"id\": 10,"
                       "    \"uid\": \"AA:BB:CC:DD:EE:FF:AA:BB:CC:DD:EE:FF\""
                       "  },"
                       "  \"logic_address\": 0,"
                       "  \"gateway_id\": 1,"
                       "  \"radio_id\": 2,"
                       "  \"static_color\": 16711935,"
                       "  \"position\": {"
                       "  \"x\": 3,"
                       "  \"y\": 12"
                       "  }"
                       "}");

  EXPECT_TRUE(deserialize(mi, ss));

  EXPECT_EQ(mi.base.id, 10);

  EXPECT_EQ(mi.base.uid.data[0], 0xAA);
  EXPECT_EQ(mi.base.uid.data[1], 0xBB);
  EXPECT_EQ(mi.base.uid.data[2], 0xCC);
  EXPECT_EQ(mi.base.uid.data[3], 0xDD);
  EXPECT_EQ(mi.base.uid.data[4], 0xEE);
  EXPECT_EQ(mi.base.uid.data[5], 0xFF);
  EXPECT_EQ(mi.base.uid.data[6], 0xAA);
  EXPECT_EQ(mi.base.uid.data[7], 0xBB);
  EXPECT_EQ(mi.base.uid.data[8], 0xCC);
  EXPECT_EQ(mi.base.uid.data[9], 0xDD);
  EXPECT_EQ(mi.base.uid.data[10], 0xEE);
  EXPECT_EQ(mi.base.uid.data[11], 0xFF);

  EXPECT_EQ(mi.logic_address, 0);
  EXPECT_EQ(mi.gateway_id, 1);
  EXPECT_EQ(mi.radio_id, 2);
  EXPECT_EQ(mi.static_color, 16711935);
  EXPECT_EQ(mi.position.x, 3);
  EXPECT_EQ(mi.position.y, 12);
}
