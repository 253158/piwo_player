#include <gtest/gtest.h>

#include <rapidjson/rapidjson.h>

#include "config.hpp"

TEST(config_load_test, load_raw_modules_from_stream)
{
  std::stringstream ss("["
                       "  {"
                       "    \"id\": 13,"
                       "    \"uid\": \"54:FF:6B:6:49:72:51:49:33:59:11:87\""
                       "  },"
                       "  {"
                       "    \"id\": 14,"
                       "    \"uid\": \"54:FF:6B:6:49:72:FF:49:33:59:11:87\""
                       "  }"
                       "]");

  global_config.load_raw_modules(ss);
  const auto& modules = std::get<1>(global_config.acquire_modules());
  EXPECT_EQ(modules.size(), 2);

  if (auto module_it =
        std::find_if(modules.begin(),
                     modules.end(),
                     [](const auto& module) { return module.base.id == 13; });
      module_it != modules.end())
  {
    EXPECT_EQ(module_it->base.uid.data[0], 0x54);
    EXPECT_EQ(module_it->base.uid.data[1], 0xFF);
    EXPECT_EQ(module_it->base.uid.data[2], 0x6B);
    EXPECT_EQ(module_it->base.uid.data[3], 0x06);
    EXPECT_EQ(module_it->base.uid.data[4], 0x49);
    EXPECT_EQ(module_it->base.uid.data[5], 0x72);
    EXPECT_EQ(module_it->base.uid.data[6], 0x51);
    EXPECT_EQ(module_it->base.uid.data[7], 0x49);
    EXPECT_EQ(module_it->base.uid.data[8], 0x33);
    EXPECT_EQ(module_it->base.uid.data[9], 0x59);
    EXPECT_EQ(module_it->base.uid.data[10], 0x11);
    EXPECT_EQ(module_it->base.uid.data[11], 0x87);
  }
  else
  {
    FAIL();
  }

  if (auto module_it =
        std::find_if(modules.begin(),
                     modules.end(),
                     [](const auto& module) { return module.base.id == 14; });
      module_it != modules.end())
  {
    EXPECT_EQ(module_it->base.uid.data[0], 0x54);
    EXPECT_EQ(module_it->base.uid.data[1], 0xFF);
    EXPECT_EQ(module_it->base.uid.data[2], 0x6B);
    EXPECT_EQ(module_it->base.uid.data[3], 0x06);
    EXPECT_EQ(module_it->base.uid.data[4], 0x49);
    EXPECT_EQ(module_it->base.uid.data[5], 0x72);
    EXPECT_EQ(module_it->base.uid.data[6], 0xFF);
    EXPECT_EQ(module_it->base.uid.data[7], 0x49);
    EXPECT_EQ(module_it->base.uid.data[8], 0x33);
    EXPECT_EQ(module_it->base.uid.data[9], 0x59);
    EXPECT_EQ(module_it->base.uid.data[10], 0x11);
    EXPECT_EQ(module_it->base.uid.data[11], 0x87);
  }
  else
  {
    FAIL();
  }
}