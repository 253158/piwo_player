#include <functional>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <piwo/lp_proto.h>
#include <piwo/lp_protodef.h>
#include <piwo/protodef.h>
#include <rapidjson/rapidjson.h>

#include "launchpad.hpp"
using namespace testing;

namespace test
{

struct conn_t
{
  MOCK_METHOD(int, write, (const char*, size_t));
  MOCK_METHOD(int, read, (char*, size_t));
  MOCK_METHOD(int, wait, (int));
  MOCK_METHOD(int, close, ());
  MOCK_METHOD(bool, is_valid, (), (const));
};

struct condition_variable
{
  MOCK_METHOD(int, notify_all, (const char*, size_t));
  MOCK_METHOD(bool,
              wait_for,
              (std::unique_lock<std::mutex>&,
               const std::chrono::seconds&,
               std::function<int()>));
};
} // namespace test

TEST(lp_state_machine,
     correctly_send_resize_packet_and_receive_accept_cmd_from_launchpad)
{
  launchpad::state_machine<test::conn_t> sm;
  test::conn_t pipe;

  EXPECT_CALL(pipe, write(_, _))
    .Times(4)
    .WillRepeatedly(
      [&pipe, &sm](const char* data, size_t size)
      {
        auto packet_opt =
          piwo::lp_raw_packet_safe::make_packet((uint8_t*)data, size);
        auto packet = packet_opt.value();

        piwo::lp_data_type buffer[piwo::launch_accept_length];
        piwo::lp_raw_packet raw_packet(buffer, piwo::launch_accept_length);

        auto l_accept_builder_opt =
          piwo::l_accept_builder::make_laccept_builder(raw_packet);

        auto l_accept_builder = l_accept_builder_opt.value();

        l_accept_builder.set_cmd(packet.data()[piwo::lp_common_type_pos]);
        piwo::l_accept l_accept_packet(l_accept_builder);

        auto status = sm.handle_received(pipe, l_accept_packet);
        EXPECT_EQ(status, launchpad::transaction::ok);
        return 0;
      });
  EXPECT_EQ(sm.resize(pipe), launchpad::transaction::ok);
  EXPECT_EQ(sm.resize(pipe), launchpad::transaction::ok);
}

TEST(lp_state_machine,
     correctly_send_resize_packet_and_receive_reject_cmd_from_launchpad)
{
  launchpad::state_machine<test::conn_t> sm;
  test::conn_t pipe;

  EXPECT_CALL(pipe, write(_, _))
    .Times(4)
    .WillRepeatedly(
      [&pipe, &sm](const char* data, size_t size)
      {
        auto packet_opt =
          piwo::lp_raw_packet_safe::make_packet((uint8_t*)data, size);
        auto packet = packet_opt.value();

        piwo::lp_data_type buffer[piwo::launch_reject_length];
        piwo::lp_raw_packet raw_packet(buffer, piwo::launch_reject_length);

        auto l_reject_builder_opt =
          piwo::l_reject_builder::make_lreject_builder(raw_packet);

        auto l_reject_builder = l_reject_builder_opt.value();

        l_reject_builder.set_cmd(packet.data()[piwo::lp_common_type_pos]);
        piwo::l_reject l_reject_packet(l_reject_builder);

        auto status = sm.handle_received(pipe, l_reject_packet);
        EXPECT_EQ(status, launchpad::transaction::ok);
        return 0;
      });

  EXPECT_EQ(sm.resize(pipe), launchpad::transaction::rejected);
  EXPECT_EQ(sm.resize(pipe), launchpad::transaction::rejected);
}

TEST(lp_state_machine, correctly_send_resize_packet_without_launchpad_response)
{
  launchpad::state_machine<test::conn_t, test::condition_variable> sm;
  test::conn_t pipe;
  test::condition_variable cv;

  EXPECT_CALL(cv, wait_for(_, _, _))
    .WillRepeatedly([](std::unique_lock<std::mutex>&,
                       const std::chrono::seconds,
                       std::function<int()>) { return false; });

  EXPECT_CALL(pipe, write(_, _))
    .Times(2)
    .WillRepeatedly(
      []([[maybe_unused]] const char* data, [[maybe_unused]] size_t size)
      {
        // do not send response
        return 0;
      });

  EXPECT_EQ(sm.resize(pipe), launchpad::transaction::timed_out);
}

TEST(lp_state_machine,
     correctly_send_resize_packet_with_state_machine_async_reset)
{
  launchpad::state_machine<test::conn_t> sm;
  test::conn_t pipe;

  EXPECT_CALL(pipe, write(_, _))
    .Times(2)
    .WillRepeatedly(
      [&sm]([[maybe_unused]] const char* data, [[maybe_unused]] size_t size)
      {
        sm.reset();
        return 0;
      });

  EXPECT_EQ(sm.resize(pipe), launchpad::transaction::rejected);
}
