#pragma once

#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>
#include <variant>

class event_loop
{
public:
  enum class event_t
  {
    resize,
    eth_connect_tx,
    eth_disconnect_tx,
    snake_connect,
    snake_disconnect,
    finish_loop,
  };

  struct resize_data
  {
    size_t width, height;
  };

  struct eth_data
  {
    std::string ip, tcp_port, udp_port;
  };

  struct snake_conn_data
  {
    std::string ip;
  };

  struct no_args
  {
  };

  using event_variant =
    std::variant<resize_data, eth_data, snake_conn_data, no_args>;

  struct event_packet_t
  {
    event_t ev;
    event_variant data;
  };

  using queue_t = std::queue<event_packet_t>;

  void
  send_event(event_packet_t event);

  event_loop();

  ~event_loop();

private:
  void
  process_events();
  void
  handle_resize(size_t width, size_t height);
  void
  handle_tx_connect(const std::string& ip,
                    const std::string& tcp_port,
                    const std::string& udp_port);
  void
  handle_tx_disconnect();
  void
  handle_snake_connect(const std::string& ip);
  void
  handle_snake_disconnect();

  event_packet_t
  pop_event();

  std::condition_variable _cv;
  std::mutex _mtx;
  std::thread _th;
  queue_t _events;
};
