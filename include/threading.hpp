#pragma once

#define MOS_REQUIRES(x)
#define MOS_ACQUIRES(x)
#define MOS_RELEASES(x)

#define MOS_OWN_PROOF [[maybe_unused]]
