#pragma once
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstdlib>
#include <memory>
#include <mutex>
#include <piwo/common_intersection_operators.h>
#include <spdlog/spdlog.h>
#include <vector>

#include <mipc/timer.h>
#include <piwo/frame.h>
#include <piwo/frames_intersection.h>

#include "frame_provider.hpp"
#include "lock.hpp"
#include "pipeline_config.hpp"
#include "suspendable_thread.hpp"

class render_engine : public frame_provider
{
  static constexpr auto SEC = std::chrono::seconds{ 0 };
  static constexpr auto MILLI = std::chrono::milliseconds{ 50 };

public:
  enum class state_t
  {
    re_busy,
    re_failed,
    re_ok,
  };

  render_engine(const std::string& name, size_t rows, size_t cols)
    : frame_provider(true)
    , _name(name)
  {
    this->_frame = piwo::alloc_frame(rows, cols);
  }

  ~render_engine() { this->stop(); }

  void
  stop()
  {
    this->_tim.stop();
    if (this->_th.joinable())
      this->_th.join();
  }

  // start with last saved time
  void
  start()
  {
    this->start(this->_sec, this->_mili);
  }

  void
  start(const std::chrono::seconds& sec, const std::chrono::milliseconds& mili)
  {
    locked_scope(
      [&]
      {
        if (this->_render_active)
          return;

        this->_render_active = true;
      },
      this->_mtx);

    this->_th = std::thread(&render_engine::render, this);
    this->_sec = sec;
    this->_mili = mili;
    this->_tim.start(sec, mili);
  }

  [[nodiscard]] state_t
  blocking_add(const render_engine_step& config)
  {
    this->stop();
    return this->add_step(config);
  }

  bool
  is_active()
  {
    return this->_tim.is_active();
  }

  [[nodiscard]] state_t
  blocking_refresh(step_id id)
  {
    this->stop();
    return this->refresh_step(id);
  }

  [[nodiscard]] state_t
  blocking_remove(step_id id)
  {
    this->stop();
    return this->remove_step(id);
  }

  [[nodiscard]] state_t
  add_step(const render_engine_step& config)
  {
    if (this->_render_active == true)
    {
      spdlog::warn("Adding step failed. Render engine busy");
      return state_t::re_busy;
    }
    this->_steps.push_back(config);
    return state_t::re_ok;
  }

  [[nodiscard]] state_t
  remove_step(step_id id)
  {
    if (this->_render_active == true)
    {
      spdlog::warn("Step removal failed. Render engine busy");
      return state_t::re_busy;
    }

    auto step = std::find(this->_steps.begin(), this->_steps.end(), id);
    if (step == this->_steps.end())
    {
      spdlog::warn(
        "Step removal failed. Configuration with specified id does not exist");
      return state_t::re_failed;
    }

    this->_steps.erase(step);
    return state_t::re_ok;
  }

  [[nodiscard]] state_t
  refresh_step(step_id id)
  {
    if (this->_render_active == true)
    {
      spdlog::warn("Refreshing step failed. Render engine busy");
      return state_t::re_busy;
    }

    auto step = std::find(this->_steps.begin(), this->_steps.end(), id);
    if (step == this->_steps.end())
    {
      spdlog::warn(
        "Step removal failed. Configuration with specified id does not exist");
      return state_t::re_failed;
    }

    step->refresh();

    return state_t::re_ok;
  }

  [[nodiscard]] state_t
  set_step_state(step_id id, bool state)
  {
    auto step = std::find(this->_steps.begin(), this->_steps.end(), id);
    if (step == this->_steps.end())
    {
      spdlog::warn(
        "Step removal failed. Configuration with specified id does not exist");
      return state_t::re_failed;
    }

    step->active = state;

    return state_t::re_ok;
  }

  void
  render()
  {
    while (true)
    {
      if (!this->_tim.wait())
      {
        locked_scope(
          [&]
          {
            this->_render_active = false;
            this->_cv.notify_all();
          },
          this->_mtx);

        return;
      }
      locked_scope(
        [&]
        {
          for (const auto& step : this->_steps)
          {
            if (!step.active)
              continue;
            std::invoke(step.effect, step._intersection);
          }
          this->_rendered = true;
          this->_cv.notify_all();
        },
        this->_mtx);
    }
  }

  piwo::frames_intersection::frame_t
  get_frame_() override
  {
    return this->_frame;
  }

  void
  resize_(size_t width, size_t height) override
  {
    spdlog::info("Changing frame resolution of\
        render engine to width->{} height->{}",
                 width,
                 height);
    this->_frame = piwo::alloc_frame_shared(width, height);
  }

  frame_provider::id
  get_name() override
  {
    return this->_name;
  }

  void
  wait() override
  {
    std::unique_lock lk(this->_mtx);
    this->_cv.wait(lk,
                   [this] { return this->_rendered || !this->_render_active; });
    this->_rendered = false;
  }

  auto&
  get_configuration()
  {
    return this->_steps;
  }

private:
  std::vector<render_engine_step> _steps;
  std::shared_ptr<piwo::frame> _frame;
  std::condition_variable _cv;
  std::mutex _mtx;
  mipc::timer _tim;
  std::thread _th;
  bool _rendered = false;
  std::atomic_bool _render_active = false;

  std::chrono::seconds _sec = SEC;
  std::chrono::milliseconds _mili = MILLI;
  std::string _name;
};
