#pragma once

#include <algorithm>
#include <fstream>
#include <memory>
#include <mutex>
#include <optional>
#include <piwo/proto.h>
#include <piwo/protodef.h>
#include <set>
#include <spdlog/spdlog.h>
#include <sstream>
#include <string>

#include <piwo/animation.h>
#include <piwo/frame.h>

#if defined(GetObject)
#undef GetObject
#endif

// without it compilation fails when including boost/asio
#ifdef _WIN32
#include <winsock2.h>
#endif

#include <piwo/animation.h>
#include <rapidjson/document.h>

#include "common_types.hpp"
#include "event_loop.hpp"
#include "json.hpp"
#include "launchpad.hpp"
#include "lock.hpp"
#include "modinfo.hpp"
#include "network.hpp"
#include "piwo_core.hpp"
#include "playlist.hpp"
#include "providers_register.hpp"
#include "render_engine.hpp"
#include "snake_connection.hpp"
#include "state.hpp"
#include "ui_iface.hpp"

#ifdef _WIN32
#undef GetObject
#endif

constexpr int MIN_RESOLUTION_WIDTH = 0;
constexpr int MAX_RESOLUTION_WIDTH = 120;

constexpr int MIN_RESOLUTION_HEIGHT = 0;
constexpr int MAX_RESOLUTION_HEIGHT = 120;

constexpr int MIN_LOGIC_ADDRESS = 0;
constexpr int MAX_LOGIC_ADDRESS = 255;

constexpr int MIN_GATEWAY_ID = 0;
constexpr int MAX_GATEWAY_ID = 255;

constexpr int MIN_RADIO_ID = 0;
constexpr int MAX_RADIO_ID = 255;

constexpr size_t INITIAL_SNAKE_MOVE_TIME_MS = 200;
constexpr size_t SNAKE_BOARD_MIN_LENGTH = 2;

constexpr unsigned int MAX_TX_COUNT = 1;
// currently we are supporting only one transmitter
static_assert(MAX_TX_COUNT == 1);

struct config_t
{
  using uresolution_t = resolution_t<size_t>;

  auto
  acquire_providers()
  {
    return this->_providers.acquire_providers();
  }

  bool
  add_step(pipeline_config& config)
  {
    auto [lck_status, lck] = scoped_lock_try(
      this->_providers._render_engine_mtx, this->_application_state_mtx);

    if (lck_status != SCOPED_LOCK_SUCCESS)
    {
      spdlog::warn("Some other operation is being taken on player. Render "
                   "engine configuration failed!");
      return false;
    }

    if (this->_application_state != application_state_t::IDLE)
    {
      spdlog::warn(
        "Player is not in IDLE state. Render engine configuration failed!");
      return false;
    }

    auto source_provider =
      this->_providers.clone_provider(config.source_provider_name);

    auto destination_provider =
      this->_providers.clone_provider(config.destination_provider_name);

    auto step_opt = render_engine_step::make_step(source_provider,
                                                  destination_provider,
                                                  config.off_x,
                                                  config.off_y,
                                                  effect_lookup[config.effect]);
    if (!step_opt.has_value())
    {
      spdlog::warn(
        "Provider step cannot be created. Render engine configuration failed!");
      return false;
    }

    render_engine* re = this->_providers.get_render_engine();
    auto step = step_opt.value();
    auto status = re->add_step(step);
    if (status != render_engine::state_t::re_ok)
    {
      config.pipeline_id = INVALID_STEP_ID;
      return false;
    }
    config.pipeline_id = step.id;

    // send back the configuration with updated status
    render_engine_configured(config);
    return true;
  }

  bool
  remove_step(step_id id)
  {
    auto [lck_status, lck] = scoped_lock_try(
      this->_providers._render_engine_mtx, this->_application_state_mtx);

    if (lck_status != SCOPED_LOCK_SUCCESS)
    {
      spdlog::warn("Some other operation is being taken on player. Render "
                   "engine step removing failed!");
      return false;
    }

    if (this->_application_state != application_state_t::IDLE)
    {
      spdlog::warn(
        "Player is not in IDLE state. Render engine step removing failed!");
      return false;
    }

    render_engine* re = this->_providers.get_render_engine();
    auto status = re->blocking_remove(id);
    if (status != render_engine::state_t::re_ok)
    {
      return false;
    }

    render_engine_step_removed(id);
    return true;
  }

  bool
  refresh_step(step_id id)
  {
    auto [lck_status, lck] = scoped_lock_try(
      this->_providers._render_engine_mtx, this->_application_state_mtx);

    if (lck_status != SCOPED_LOCK_SUCCESS)
    {
      spdlog::warn("Some other operation is being taken on player. Render "
                   "engine step removing failed!");
      return false;
    }

    if (this->_application_state != application_state_t::IDLE)
    {
      spdlog::warn(
        "Player is not in IDLE state. Render engine step removing failed!");
      return false;
    }

    render_engine* re = this->_providers.get_render_engine();
    auto status = re->blocking_refresh(id);
    if (status != render_engine::state_t::re_ok)
    {
      return false;
    }

    return true;
  }

  bool
  change_step_state(step_id id, bool state)
  {
    auto [lck_status, lck] = scoped_lock_try(
      this->_providers._render_engine_mtx, this->_application_state_mtx);

    if (lck_status != SCOPED_LOCK_SUCCESS)
    {
      spdlog::warn("Some other operation is being taken on player. Render "
                   "engine step removing failed!");
      return false;
    }

    if (this->_application_state != application_state_t::IDLE)
    {
      spdlog::warn(
        "Player is not in IDLE state. Render engine step removing failed!");
      return false;
    }

    render_engine* re = this->_providers.get_render_engine();
    auto status = re->set_step_state(id, state);
    if (status != render_engine::state_t::re_ok)
    {
      return false;
    }

    render_engine_step_state_changed(id, state);
    return true;
  }

  void
  enable_render_engine_configuration()
  {
    auto [lck_status, lck] = scoped_lock_try(
      this->_providers._render_engine_mtx, this->_application_state_mtx);

    if (lck_status != SCOPED_LOCK_SUCCESS)
    {
      spdlog::warn("Some other operation is being taken on player. Enabling "
                   "render engine rejected!");
      return;
    }

    if (this->_application_state != application_state_t::IDLE)
    {
      spdlog::warn(
        "Player is not in IDLE state. Enabling render engine rejected.");
      return;
    }

    provider_list_changed(this->_providers._providers);
    render_engine_state_chnaged(true);
  }

  void
  disable_render_engine_configuration()
  {
    auto [lck_status, lck] = scoped_lock_try(
      this->_providers._render_engine_mtx, this->_application_state_mtx);
    if (lck_status != SCOPED_LOCK_SUCCESS)
    {
      spdlog::warn("Some other operation is being taken on player. Enabling "
                   "render engine rejected!");
      return;
    }

    if (this->_application_state != application_state_t::IDLE)
    {
      spdlog::warn(
        "Player is not in IDLE state. Disabling render engine rejected.");
      return;
    }

    provider_list_changed({});
    render_engine_state_chnaged(false);
  }

  void
  auto_configure_modules([[maybe_unused]] const std::vector<piwo::uid>& uid_vec)
  {
    locked_scope(
      [&]
      {
        if (this->_application_state != application_state_t::IDLE)
        {
          spdlog::warn("Player is not in IDLE state. Rejecting display auto "
                       "configuraction request!");
          return;
        }
        this->_application_state = application_state_t::DISPLAY_AUTO_CONFIG;
        application_state_changed(this->_application_state);
      },
      this->_application_state_mtx);

    state_handler_scope(
      [&]
      {
        spdlog::warn("Bump! auto_configure_modules uid not implemented");
        // TODO(all) Implement LSD
      },
      this->_application_state);
  }

  void
  auto_configure_modules([[maybe_unused]] const std::vector<ipos_t>& tiles_vec)
  {
    locked_scope(
      [&]
      {
        if (this->_application_state != application_state_t::IDLE)
        {
          spdlog::warn("Player is not in IDLE state. Rejecting display auto "
                       "configuraction request!");
          return;
        }
        this->_application_state = application_state_t::DISPLAY_AUTO_CONFIG;
        application_state_changed(this->_application_state);
      },
      this->_application_state_mtx);

    state_handler_scope(
      [&]
      {
        spdlog::warn("Bump! auto_configure_modules ipos not implemented");
        // TODO(all) Implement LSD
      },
      this->_application_state);
  }

  auto
  acquire_modules()
  {
    return std::make_tuple(std::unique_lock(this->_modules_mtx),
                           std::ref(_modules));
  }

  auto
  acquire_resolution()
  {
    return std::make_tuple(std::unique_lock(this->_resolution_mtx),
                           std::ref(_resolution));
  }

  application_state_t
  get_application_state()
  {
    return this->_application_state;
  }

  auto
  acquire_playlist()
  {
    return this->_providers.acquire_playlist();
  }

  auto
  acquire_animations()
  {
    return this->_providers.get_playlist()->acquire_animations();
  }

  auto
  acquire_render_engine()
  {
    return this->_providers.acquire_render_engine();
  }

  auto
  acquire_event_loop()
  {
    return std::make_tuple(std::unique_lock(this->_event_loop_mtx),
                           std::ref(this->_event_loop));
  }

  void
  set_resolution(uresolution_t::underlying_type width,
                 uresolution_t::underlying_type height)
  {
    if (width > MAX_RESOLUTION_WIDTH || height > MAX_RESOLUTION_HEIGHT ||
        width < MIN_RESOLUTION_WIDTH || height < MIN_RESOLUTION_HEIGHT)
    {
      spdlog::warn(
        "Resolution value is out of bounds. Rejecting set resolution request!");
      return;
    }

    std::vector<piwo::uid> modules_out_of_range;

    locked_scope(
      [&]
      {
        if (this->_application_state != application_state_t::IDLE)
        {
          spdlog::warn(
            "Player is not in IDLE state. Rejecting resolution change!");
          return;
        }
        locked_scope(
          [&]
          {
            for (auto module : this->_modules)
            {
              if (module.position.x >=
                    static_cast<ipos_t::underlying_type>(width) ||
                  module.position.y >=
                    static_cast<ipos_t::underlying_type>(height))
              {
                modules_out_of_range.push_back(module.base.uid);
              }
            }
          },
          this->_modules_mtx);

        for (auto module_out_of_range : modules_out_of_range)
        {
          ipos_t unassigned_position{ .x = MODULE_INVALID_POS,
                                      .y = MODULE_INVALID_POS };
          this->assign_module_position(module_out_of_range,
                                       unassigned_position);
        }

        locked_scope(
          [&]
          {
            this->_resolution.width = width;
            this->_resolution.height = height;
          },
          this->_resolution_mtx);

        spdlog::info("New resolution {}x{}", width, height);
        event_loop::resize_data resize_packet{ width, height };
        _event_loop.send_event({ event_loop::event_t::resize, resize_packet });

        if (this->_snake_connection.is_socket_connected())
        {
          if (width >= SNAKE_BOARD_MIN_LENGTH &&
              height >= SNAKE_BOARD_MIN_LENGTH)
          {
            this->snake_send_board_size(width, height);
          }
          else
          {
            this->stop_snake_game();
          }
        }
        config_resolution_changed();
      },
      this->_application_state_mtx);
  }

  void
  assign_module_position(piwo::uid& uid, ipos_t& position)
  {
    bool emit_data_changed_signal = false;
    if ((position.x < 0 && position.y >= 0) ||
        (position.y < 0 && position.x >= 0))
    {
      spdlog::warn("Inconsistent position x: {} y: {}. Rejecting assing module "
                   "position request!",
                   position.x,
                   position.y);
      return;
    }
    std::optional<piwo::uid> module_that_was_swapped;
    modinfo key{};
    key.base.uid = uid;

    locked_scope(
      [&]
      {
        if (this->_application_state != application_state_t::IDLE)
        {
          spdlog::warn("Player is not in IDLE state. Rejecting assing module "
                       "position request!");
          return;
        }

        locked_scope(
          [&]
          {
            auto module_handle = this->_modules.extract(key);

            if (!module_handle)
            {
              spdlog::error(
                "Failed to assign module position. UID[{:s}] not found.",
                uid.to_ascii());
              return;
            }

            if (position.x >= 0 && position.y >= 0)
            {
              for (auto module : this->_modules)
              {
                if (module.position.x == position.x &&
                    module.position.y == position.y)
                {
                  auto uid_module_to_swap_pos_with = module.base.uid;
                  modinfo key_second{};
                  key_second.base.uid = uid_module_to_swap_pos_with;
                  auto module_handle_to_swap_pos_with =
                    this->_modules.extract(key_second);
                  if (!module_handle_to_swap_pos_with)
                  {
                    spdlog::critical("This should never happen");
                    return;
                  }

                  module_handle_to_swap_pos_with.value().position =
                    module_handle.value().position;
                  spdlog::info(
                    "Module uid[{}] assigned to pos x: {} y: {}",
                    module.base.uid.to_ascii(),
                    module_handle_to_swap_pos_with.value().position.x,
                    module_handle_to_swap_pos_with.value().position.y);
                  this->_modules.insert(
                    std::move(module_handle_to_swap_pos_with));
                  module_that_was_swapped = uid_module_to_swap_pos_with;
                  break;
                }
              }
            }

            module_handle.value().position = position;
            this->_modules.insert(std::move(module_handle));
            emit_data_changed_signal = true;
            spdlog::info("Module uid[{}] assigned to pos x: {} y: {}",
                         uid.to_ascii(),
                         position.x,
                         position.y);
          },
          this->_modules_mtx); // locked scope
      },
      this->_application_state_mtx); // locked scope

    if (emit_data_changed_signal)
    {
      if (module_that_was_swapped.has_value())
      {
        config_module_data_changed(*module_that_was_swapped);
      }
      config_module_data_changed(uid);
    }
  }

  void
  set_module_id(piwo::uid& uid, int64_t id)
  {
    bool emit_data_changed_signal = false;
    modinfo key{};
    key.base.uid = uid;

    locked_scope(
      [&]
      {
        if (this->_application_state != application_state_t::IDLE)
        {
          spdlog::warn(
            "Player is not in IDLE state. Rejecting set module id request!");
          return;
        }
        locked_scope(
          [&]
          {
            for (auto module : this->_modules)
            {
              if (module.base.id == id)
              {
                spdlog::warn("Module with ID {} already exists. Rejecting", id);
                return;
              }
            }

            auto module_handle = this->_modules.extract(key);

            if (!module_handle)
            {
              spdlog::error("Failed to set module ID. UID[{:s}] not found.",
                            uid.to_ascii());
              return;
            }
            module_handle.value().base.id = id;
            this->_modules.insert(std::move(module_handle));
            emit_data_changed_signal = true;
            spdlog::info("Module uid[{}] ID set to {}", uid.to_ascii(), id);
          },
          this->_modules_mtx); // locked scope
      },
      this->_application_state_mtx); // locked scope

    if (emit_data_changed_signal)
    {
      config_module_data_changed(uid);
    }
  }

  void
  set_module_logic_address(piwo::uid& uid, piwo::logic_address_t logic_address)
  {
    bool emit_data_changed_signal = false;
    modinfo key{};
    key.base.uid = uid;

    locked_scope(
      [&]
      {
        if (this->_application_state != application_state_t::IDLE)
        {
          spdlog::warn("Player is not in IDLE state. Rejecting set module "
                       "logic address request!");
          return;
        }
        locked_scope(
          [&]
          {
            for (auto module : this->_modules)
            {
              if (module.logic_address == logic_address)
              {
                spdlog::warn(
                  "Module with logic address {} already exists. Rejecting",
                  logic_address);
                return;
              }
            }

            auto module_handle = this->_modules.extract(key);

            if (!module_handle)
            {
              spdlog::error(
                "Failed to set module logic address. UID[{:s}] not found.",
                uid.to_ascii());
              return;
            }
            module_handle.value().logic_address = logic_address;
            this->_modules.insert(std::move(module_handle));
            emit_data_changed_signal = true;
            spdlog::info("Module uid[{}] logic address set to {}",
                         uid.to_ascii(),
                         logic_address);
          },
          this->_modules_mtx); // locked scope
      },
      this->_application_state_mtx); // locked scope

    if (emit_data_changed_signal)
    {
      config_module_data_changed(uid);
    }
  }

  void
  set_modules_gateway_id(const std::vector<piwo::uid>& uids,
                         piwo::gateway_id_t gateway_id)
  {
    locked_scope(
      [&]
      {
        if (this->_application_state != application_state_t::IDLE)
        {
          spdlog::warn("Player is not in IDLE state. Rejecting set modules "
                       "gateway id request!");
          return;
        }

        if (gateway_id >= MAX_TX_COUNT)
        {
          spdlog::warn("Setting gatway id to {} failed. Max number of active "
                       "gateways is {}",
                       gateway_id,
                       MAX_TX_COUNT);
          return;
        }

        for (auto uid : uids)
        {
          bool emit_data_changed_signal = false;
          modinfo key{};
          key.base.uid = uid;

          locked_scope(
            [&]
            {
              auto module_handle = this->_modules.extract(key);

              if (!module_handle)
              {
                spdlog::error(
                  "Failed to set module gateway id. UID[{:s}] not found.",
                  uid.to_ascii());
                return;
              }
              module_handle.value().gateway_id = gateway_id;
              this->_modules.insert(std::move(module_handle));
              emit_data_changed_signal = true;
              spdlog::info("Module uid[{}] gateway id set to {}",
                           uid.to_ascii(),
                           gateway_id);
            },
            this->_modules_mtx); // locked scope

          if (emit_data_changed_signal)
          {
            config_module_data_changed(uid);
          }
        }
      },
      this->_application_state_mtx); // locked scope
  }

  void
  set_modules_radio_id(const std::vector<piwo::uid>& uids,
                       piwo::radio_id_t radio_id)
  {
    locked_scope(
      [&]
      {
        if (this->_application_state != application_state_t::IDLE)
        {
          spdlog::warn("Player is not in IDLE state. Rejecting set modules "
                       "radio id request!");
          return;
        }
        for (auto uid : uids)
        {
          bool emit_data_changed_signal = false;
          modinfo key{};
          key.base.uid = uid;

          locked_scope(
            [&]
            {
              auto module_handle = this->_modules.extract(key);

              if (!module_handle)
              {
                spdlog::error(
                  "Failed to set module radio id. UID[{:s}] not found.",
                  uid.to_ascii());
                return;
              }
              module_handle.value().radio_id = radio_id;
              this->_modules.insert(std::move(module_handle));
              emit_data_changed_signal = true;
              spdlog::info(
                "Module uid[{}] radio id set to {}", uid.to_ascii(), radio_id);
            },
            this->_modules_mtx); // locked scope

          if (emit_data_changed_signal)
          {
            config_module_data_changed(uid);
          }
        }
      },
      this->_application_state_mtx); // locked scope
  }

  void
  set_modules_static_color(const std::vector<piwo::uid>& uids,
                           piwo::color static_color)
  {
    locked_scope(
      [&]
      {
        if (this->_application_state != application_state_t::IDLE)
        {
          spdlog::warn("Player is not in IDLE state. Rejecting set modules "
                       "static color request!");
          return;
        }

        for (auto uid : uids)
        {
          bool emit_data_changed_signal = false;
          modinfo key{};
          key.base.uid = uid;

          locked_scope(
            [&]
            {
              auto module_handle = this->_modules.extract(key);

              if (!module_handle)
              {
                spdlog::error(
                  "Failed to set module radio id. UID[{:s}] not found.",
                  uid.to_ascii());
                return;
              }
              module_handle.value().static_color = static_color;
              this->_modules.insert(std::move(module_handle));
              emit_data_changed_signal = true;
              spdlog::info("Module uid[{}] static color set to #{:x}",
                           uid.to_ascii(),
                           static_color.packed());
            },
            this->_modules_mtx); // locked scope

          if (emit_data_changed_signal)
          {
            config_module_data_changed(uid);
          }
        }
      },
      this->_application_state_mtx); // locked scope
  }

  void
  blink_modules([[maybe_unused]] const std::vector<piwo::uid>& modules)
  {
    bool failed = false;
    locked_scope(
      [&]
      {
        if (this->_application_state != application_state_t::IDLE)
        {
          spdlog::warn("Player is not in IDLE state. Rejecting blink "
                       "modules request!");
          failed = true;
          return;
        }
      },
      this->_application_state_mtx);

    if (failed)
    {
      return;
    }

    std::array<piwo::net_byte_t, FORAWRDING_BUFFER_SIZE> packet_buffer{};
    piwo::raw_packet raw_packet(packet_buffer.data(), packet_buffer.size());
    std::optional forward_builder_opt =
      piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);
    auto forward_builder = forward_builder_opt.value();
    piwo::gateway_id_t last_used_tx = 0;

    for (uint8_t i = 0; const auto& m_uid : modules)
    {
      auto mod = std::find_if(this->_modules.begin(),
                              this->_modules.end(),
                              [&m_uid](const modinfo& m) {
                                return m.base.uid.packed() == m_uid.packed();
                              });
      if (mod == this->_modules.end())
        continue;

      std::optional buff_slot_opt = forward_builder.get_next_slot();
      // no more memory send packet and continue
      if (!buff_slot_opt.has_value())
      {
        forward_packet_and_reset_builder(
          last_used_tx, forward_builder, raw_packet);
        buff_slot_opt = forward_builder.get_next_slot();
      }

      if (last_used_tx != mod->gateway_id)
      {
        forward_packet_and_reset_builder(
          last_used_tx, forward_builder, raw_packet);
        last_used_tx = mod->gateway_id;
        buff_slot_opt = forward_builder.get_next_slot();
      }

      auto buff_slot = *buff_slot_opt;

      std::optional blink_builder_opt =
        piwo::blink_builder::make_blink_builder(buff_slot);

      // not enough memory for blink packet
      // send packet and create new one
      if (!blink_builder_opt.has_value())
      {
        forward_packet_and_reset_builder(
          last_used_tx, forward_builder, raw_packet);
        buff_slot_opt = forward_builder.get_next_slot();

        blink_builder_opt =
          piwo::blink_builder::make_blink_builder(buff_slot_opt.value());
      }
      auto blink_builder = *blink_builder_opt;

      blink_builder.set_uid(mod->base.uid);

      auto blink_packet = piwo::blink(blink_builder);
      forward_builder.commit(blink_packet, mod->radio_id);
      i++;
    }
    forward_packet_and_reset_builder(last_used_tx, forward_builder, raw_packet);
  }

  void
  color_modules([[maybe_unused]] const std::vector<piwo::uid>& modules)
  {
    bool failed = false;
    locked_scope(
      [&]
      {
        if (this->_application_state != application_state_t::IDLE)
        {
          spdlog::warn("Player is not in IDLE state. Rejecting color "
                       "modules request!");
          failed = true;
          return;
        }
      },
      this->_application_state_mtx);

    if (failed)
    {
      return;
    }

    std::array<piwo::net_byte_t, FORAWRDING_BUFFER_SIZE> packet_buffer{};
    piwo::raw_packet raw_packet(packet_buffer.data(), packet_buffer.size());
    std::optional forward_builder_opt =
      piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);
    auto forward_builder = forward_builder_opt.value();
    piwo::gateway_id_t last_used_tx = 0;

    for (uint8_t i = 0; const auto& m_uid : modules)
    {
      auto mod = std::find_if(this->_modules.begin(),
                              this->_modules.end(),
                              [&m_uid](const modinfo& m) {
                                return m.base.uid.packed() == m_uid.packed();
                              });
      if (mod == this->_modules.end())
        continue;

      std::optional buff_slot_opt = forward_builder.get_next_slot();
      // no more memory send packet and continue
      if (!buff_slot_opt.has_value())
      {
        forward_packet_and_reset_builder(
          last_used_tx, forward_builder, raw_packet);
        buff_slot_opt = forward_builder.get_next_slot();
      }

      if (last_used_tx != mod->gateway_id)
      {
        forward_packet_and_reset_builder(
          last_used_tx, forward_builder, raw_packet);
        last_used_tx = mod->gateway_id;
        buff_slot_opt = forward_builder.get_next_slot();
      }

      auto buff_slot = *buff_slot_opt;

      std::optional constant_builder_opt =
        piwo::constant_color_builder::make_constant_color_builder(buff_slot);

      // not enough memory for const color packet
      // send packet and create new one with color
      if (!constant_builder_opt.has_value())
      {
        forward_packet_and_reset_builder(
          last_used_tx, forward_builder, raw_packet);
        buff_slot_opt = forward_builder.get_next_slot();

        constant_builder_opt =
          piwo::constant_color_builder::make_constant_color_builder(
            buff_slot_opt.value());
      }
      auto constant_color_builder = *constant_builder_opt;

      constant_color_builder.set_seq(i);
      constant_color_builder.set_uid(mod->base.uid);
      constant_color_builder.set_color(
        { mod->static_color.r, mod->static_color.g, mod->static_color.b });

      auto color_packet = piwo::constant_color(constant_color_builder);
      forward_builder.commit(color_packet, mod->radio_id);
      i++;
    }
    forward_packet_and_reset_builder(last_used_tx, forward_builder, raw_packet);
  }

  void
  assign_la_for_modules([[maybe_unused]] const std::vector<piwo::uid>& modules)
  {
    bool failed = false;
    locked_scope(
      [&]
      {
        if (this->_application_state != application_state_t::IDLE)
        {
          spdlog::warn("Player is not in IDLE state. Rejecting color "
                       "modules request!");
          failed = true;
          return;
        }
      },
      this->_application_state_mtx);

    if (failed)
    {
      return;
    }

    std::array<piwo::net_byte_t, FORAWRDING_BUFFER_SIZE> packet_buffer{};
    piwo::raw_packet raw_packet(packet_buffer.data(), packet_buffer.size());
    std::optional forward_builder_opt =
      piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);
    auto forward_builder = forward_builder_opt.value();
    piwo::gateway_id_t last_used_tx = 0;

    for (uint8_t i = 0; const auto& m_uid : modules)
    {
      auto mod = std::find_if(this->_modules.begin(),
                              this->_modules.end(),
                              [&m_uid](const modinfo& m) {
                                return m.base.uid.packed() == m_uid.packed();
                              });
      if (mod == this->_modules.end())
        continue;

      std::optional buff_slot_opt = forward_builder.get_next_slot();
      // no more memory send packet and continue
      if (!buff_slot_opt.has_value())
      {
        forward_packet_and_reset_builder(
          last_used_tx, forward_builder, raw_packet);
        buff_slot_opt = forward_builder.get_next_slot();
      }

      if (last_used_tx != mod->gateway_id)
      {
        forward_packet_and_reset_builder(
          last_used_tx, forward_builder, raw_packet);
        last_used_tx = mod->gateway_id;
        buff_slot_opt = forward_builder.get_next_slot();
      }

      auto buff_slot = *buff_slot_opt;

      auto la_builder_opt =
        piwo::assign_la_builder::make_assign_la_builder(buff_slot);

      // not enough memory for assign logic address packet
      // send packet and create new one
      if (!la_builder_opt.has_value())
      {
        forward_packet_and_reset_builder(
          last_used_tx, forward_builder, raw_packet);
        buff_slot_opt = forward_builder.get_next_slot();

        la_builder_opt =
          piwo::assign_la_builder::make_assign_la_builder(buff_slot);
      }
      auto la_builder = *la_builder_opt;

      la_builder.set_seq(i);
      la_builder.set_uid(mod->base.uid);
      la_builder.set_la(mod->logic_address);

      auto la_packet = piwo::assign_la(la_builder);
      forward_builder.commit(la_packet, mod->radio_id);
      i++;
    }
    forward_packet_and_reset_builder(last_used_tx, forward_builder, raw_packet);
  }

  void
  load_raw_modules(std::istream& f)
  {
    rapidjson::Document d;
    rapidjson::BasicIStreamWrapper bsw(f);
    d.ParseStream(bsw);

    if (!d.IsArray())
    {
      spdlog::warn("Invalid or corrupted raw modules file!");
      return;
    }

    std::vector<piwo::uid> new_modules;
    locked_scope(
      [&]
      {
        for (rapidjson::SizeType i = 0; i < d.Size(); i++)
        {
          modinfo module{};
          module.static_color.r = 255;
          module.static_color.g = 255;
          module.static_color.b = 255;
          module.position.x = MODULE_INVALID_POS;
          module.position.y = MODULE_INVALID_POS;
          deserialize(module.base, d[i].GetObject());
          if (auto [module_it, inserted] =
                this->_modules.insert(std::move(module));
              inserted)
          {
            new_modules.push_back(module_it->base.uid);
          }
        }
      },
      this->_modules_mtx);

    for (auto new_module : new_modules)
    {
      config_module_added(new_module);
    }
  }

  void
  load_raw_modules_from_file(const char* path)
  {
    std::ifstream isf(path);
    if (!isf)
    {
      spdlog::warn(
        "Failed to open file {} Errno: {} ({}). Loading raw modules failed!",
        path,
        errno,
        strerror(errno));
      return;
    }
    load_raw_modules(isf);
  }

  void
  save_to_file(const char* path)
  {
    rapidjson::Document d;
    rapidjson::StringBuffer buffer;
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);

    writer.StartObject();
    writer.Key("display_configuration");

    writer.StartObject();
    writer.Key("modules");
    writer.StartArray();

    for (auto module_info : this->_modules)
    {
      serialize(module_info, writer);
    }

    writer.EndArray();
    writer.Key("display_width");
    writer.Int64(this->_resolution.width);
    writer.Key("display_height");
    writer.Int64(this->_resolution.height);
    writer.EndObject(); // display_configuration
    writer.EndObject();

    std::ofstream out_file(path, std::ofstream::out);
    if (!out_file)
    {
      spdlog::warn(
        "Failed to open file {} Errno: {} ({}). Saving configuration failed!",
        path,
        errno,
        strerror(errno));
      return;
    }

    out_file.write(buffer.GetString(), buffer.GetSize());
    out_file.close();
    // TODO(all) serialize rest of this structure
  }

  void
  load_from_file(const char* path)
  {
    std::ifstream input_file(path);
    if (!input_file)
    {
      spdlog::warn(
        "Failed to open file {} Errno: {} ({}). Loading configuration failed!",
        path,
        errno,
        strerror(errno));
      return;
    }

    rapidjson::Document d;
    rapidjson::IStreamWrapper bsw(input_file);
    d.ParseStream(bsw);

    if (!d.IsObject() || !d.HasMember("display_configuration") ||
        !d["display_configuration"].IsObject())
    {
      spdlog::warn("Invalid or corrupted configuration file!");
      return;
    }

    rapidjson::Document::Object display_configuration_object =
      d["display_configuration"].GetObject();

    if (!display_configuration_object.HasMember("modules") ||
        !display_configuration_object["modules"].IsArray())
    {
      spdlog::warn("Invalid or corrupted configuration file!");
      return;
    }

    if (!display_configuration_object.HasMember("display_width") ||
        !display_configuration_object["display_width"].IsInt64() ||
        !display_configuration_object.HasMember("display_height") ||
        !display_configuration_object["display_height"].IsInt64())
    {
      spdlog::warn("Invalid or corrupted configuration file!");
      return;
    }

    this->set_resolution(
      display_configuration_object["display_width"].GetInt64(),
      display_configuration_object["display_height"].GetInt64());

    rapidjson::Document::Array attributes =
      display_configuration_object["modules"].GetArray();

    std::vector<piwo::uid> new_modules;
    locked_scope(
      [&]
      {
        for (rapidjson::SizeType i = 0; i < attributes.Size(); i++)
        {
          modinfo module{};
          deserialize(module, attributes[i].GetObject());
          if (auto [module_it, inserted] =
                this->_modules.insert(std::move(module));
              inserted)
          {
            new_modules.push_back(module_it->base.uid);
          }
        }
      },
      this->_modules_mtx);

    for (auto new_module_uid : new_modules)
    {
      config_module_added(new_module_uid);
    }

    // TODO(all) deserialize rest of this structure
  }

  void
  load_animation(const char* path)
  {
    using po = piwo::animation::parse_option;

    named_animation anim;
    bool anim_added = false;

    po parse_op;
    parse_op.translate = po::translate_type::ms50_exact;
    const auto err = anim.anim.build_from_file(path, parse_op);

    if (err != piwo::animation::error_e::ok)
    {
      spdlog::warn("Failed to load animation from {}", path);
      return;
    }

    spdlog::info("Loaded new animation from {}", path);

    locked_scope(
      [&]
      {
        anim.name = path;
        spdlog::info(anim.name);
        anim_added =
          this->_providers.get_playlist()->add_animation(std::move(anim));
        this->_providers.get_playlist()->reset_current_animation();
      },
      this->_providers._playlist_mtx);
    if (anim_added)
      playlist_changed();
  }

  void
  enable_light_show()
  {
    spdlog::trace(__PRETTY_FUNCTION__);

    auto [lck_status, lck] = scoped_lock_try(
      this->_providers._render_engine_mtx, this->_application_state_mtx);

    if (lck_status != SCOPED_LOCK_SUCCESS)
    {
      spdlog::warn("Some other operation is being taken on player. Lightshow "
                   "hasn't been enabled!");
      return;
    }

    if (this->_application_state != application_state_t::IDLE)
    {
      spdlog::warn(
        "Player is not in IDLE state. Rejecting enable lightshow request!");
      return;
    }

    if (!this->_ls_task.run())
    {
      spdlog::warn("Lightshow not configured, first commit configuration. "
                   "Rejecting enable lightshow request!");
      return;
    }

    disable_render_engine_configuration();

    this->_application_state = application_state_t::LIGHTSHOW;
    application_state_changed(this->_application_state);

    spdlog::info("Lightshow enabled");

    auto* re = this->_providers.get_render_engine();
    re->start();

    light_show_started();
  }

  void
  disable_light_show()
  {
    spdlog::trace(__PRETTY_FUNCTION__);
    auto [lck_status, lck] = scoped_lock_try(
      this->_providers._render_engine_mtx, this->_application_state_mtx);

    if (lck_status != SCOPED_LOCK_SUCCESS)
    {
      spdlog::warn("Some other operation is being taken on player. Lightshow "
                   "hasn't been disabled!");
      return;
    }

    if (this->_application_state != application_state_t::LIGHTSHOW)
    {
      spdlog::warn("Player is not in LIGHTSHOW state. Rejecting disable "
                   "lightshow request!");
      return;
    }

    this->_application_state = application_state_t::IDLE;

    spdlog::info("Lightshow disabled");
    application_state_changed(this->_application_state);

    light_show_stopped();

    render_engine* re = this->_providers.get_render_engine();
    re->stop();
    this->_ls_task.stop();
    enable_render_engine_configuration();
  }

  void
  start_animation()
  {
    spdlog::trace(__PRETTY_FUNCTION__);

    auto [lock, playlist] = this->acquire_playlist();
    playlist.start();
  }

  void
  pause_animation()
  {
    spdlog::trace(__PRETTY_FUNCTION__);

    auto [lock, playlist] = this->acquire_playlist();
    playlist.pause();
  }

  void
  change_animation_to(size_t index)
  {
    spdlog::trace("{} {}", __PRETTY_FUNCTION__, index);

    auto [lock, playlist] = this->acquire_playlist();
    const auto result = playlist.set_animation(index);

    if (result)
    {
      spdlog::debug("Animation changed to one with index == {}", index);
      // display_animation_changed();
    }
    else
    {
      spdlog::error("Animation change failed");
    }
  }

  void
  connect_launchad()
  {
    // TODO implement lp as a frame provider
    // std::unique_lock lck(this->_providers_mtx);
    // locked_scope(
    //   [&]
    //   {
    //     this->_providers.push_back(std::make_unique<launchpad>(name));
    //     if (!_lp.connect())
    //     {
    //       spdlog::error("Connection to launchpad failed");
    //       return;
    //     }
    //     spdlog::info("Launchpad connected");
    //   },
    //   this->_lp_mtx);
  }

  void
  disconnect_launchad()
  {
    // locked_scope(
    //   [&]
    //   {
    //     if (!_lp.disconnect())
    //     {
    //       spdlog::error("Failed to disconnect from launchpad");
    //       return;
    //     }
    //     spdlog::info("Launchpad disconnected");
    //   },
    //   this->_lp_mtx);
  }

  template<typename T>
  bool
  add_provider(std::unique_ptr<T>&& provider)
  {
    auto [lck_status, lck] = scoped_lock_try(this->_application_state_mtx);

    if (lck_status != SCOPED_LOCK_SUCCESS)
    {
      spdlog::warn("Some other operation is being taken on player. Adding the "
                   "provider rejected!");
      return false;
    }

    if (this->_application_state != application_state_t::IDLE)
    {
      spdlog::warn(
        "Player is not in IDLE state. Adding the provider rejected!");
      return false;
    }

    this->_providers.add_provider(std::move(provider));
    provider_list_changed(this->_providers._providers);
    return true;
  }

  void
  reconnect_snake(const std::string& host)
  {
    this->disconnect_snake();

    if (this->_snake_connection.connect(host.c_str()))
    {
      this->on_snake_connected();
    }
  }

  void
  on_snake_connected()
  {
    size_t width, height;
    locked_scope(
      [&]
      {
        width = this->_resolution.width;
        height = this->_resolution.height;
      },
      this->_resolution_mtx);

    snake_connection_established();
    this->snake_send_board_size(width, height);
  }

  void
  disconnect_snake()
  {
    this->_snake_connection.disconnect();
    snake_disconnected();
    this->_is_snake_started = false;
    snake_move_state_changed(false);
  }

  void
  restart_snake_game()
  {
    bool is_resolution_proper = true;
    locked_scope(
      [&]
      {
        if (this->_resolution.width < SNAKE_BOARD_MIN_LENGTH ||
            this->_resolution.height < SNAKE_BOARD_MIN_LENGTH)
        {
          is_resolution_proper = false;
        }
      },
      this->_resolution_mtx);

    if (!is_resolution_proper)
    {
      spdlog::warn(
        "Current resolution is not proper. Starting snake rejected.");
      return;
    }

    this->_snake_connection.send_signal_with_large_number(
      signals_to_server::restart_game, this->_snake_move_time_ms);
    this->_is_snake_started = true;
    snake_move_state_changed(true);
  }

  void
  stop_snake_game()
  {
    snake_send_type stop_signal{ static_cast<int8_t>(
      signals_to_server::stop_game) };
    this->_snake_connection.send_data(stop_signal);
    this->_is_snake_started = false;
    snake_move_state_changed(false);
  }

  void
  snake_send_board_size(size_t width, size_t height)
  {
    auto height_opt = narrow<int8_t>(height);
    auto width_opt = narrow<int8_t>(width);
    if (!height_opt.has_value() || !width_opt.has_value())
    {
      return;
    }

    snake_send_type board_size_signal{ piwo::underlay_cast(
                                         signals_to_server::board_size),
                                       height_opt.value(),
                                       width_opt.value() };

    this->_snake_connection.send_data(board_size_signal);
  }

  void
  on_snake_clients_count_received(size_t clients_count)
  {
    snake_refresh_clients_count(clients_count);
  }

  void
  on_snake_frame_received(const snake_send_type& data_received)
  {
    std::lock_guard lg(this->_providers._snake_provider_mtx);
    snake_data_t snake_data(data_received.size());
    std::copy(data_received.begin(), data_received.end(), snake_data.begin());
    this->_providers.get_snake_provider()->fill_frame(snake_data);
  }

  void
  set_snake_move_time(size_t move_time)
  {
    if (move_time != this->_snake_move_time_ms)
    {
      this->_snake_move_time_ms = move_time;
      if (this->_is_snake_started)
      {
        this->_snake_connection.send_signal_with_large_number(
          signals_to_server::snake_move_time, this->_snake_move_time_ms);
      }
    }
  }

  std::optional<tx_eth_connection*>
  get_tx_eth_conn(unsigned int i)
  {
    if (i >= MAX_TX_COUNT)
      return std::nullopt;

    return &_tx_conn[i];
  }

  bool
  commit_configuration()
  {
    auto [lck_status, lck] = scoped_lock_try(
      this->_application_state_mtx, this->_modules_mtx, this->_resolution_mtx);

    if (lck_status != SCOPED_LOCK_SUCCESS)
    {
      spdlog::warn(
        "Some other operation is being taken on player. Logic address "
        "configuration failed!");
      return false;
    }

    if (this->_application_state != application_state_t::IDLE)
    {
      spdlog::warn("Player is not in IDLE state. Rejecting commit commiting!");
      return false;
    }

    frame_provider* re = this->_providers.get_render_engine();
    // assign to each group
    auto owners_info = own_offsets(this->_modules, re->get_frame());

    // calculate offsets
    configure_logic_address(
      this->_modules, owners_info, this->_resolution.width);

    std::weak_ptr<frame_provider> provider_weak =
      this->_providers.clone_provider(providers_register::RENDER_ENGINE_INDEX);
    auto provider = provider_weak.lock();
    if (!provider)
      return false;

    this->_ls_task.stop_and_configure(provider, owners_info);

    return true;
  }

  void
  tx_connect(const std::string& host,
             const std::string& tcp_port,
             const std::string& udp_port)
  {
    std::optional tx_conn_opt = this->get_tx_eth_conn(0);
    if (!tx_conn_opt.has_value())
      return;

    this->tx_disconnect();
    auto& tx_conn = *tx_conn_opt;

    const auto res =
      tx_conn->open(host.c_str(), tcp_port.c_str(), udp_port.c_str());

    eth_state_changed(res);
  }

  void
  tx_disconnect()
  {
    std::optional tx_conn_opt = this->get_tx_eth_conn(0);
    if (!tx_conn_opt.has_value())
      return;
    auto& tx_conn = *tx_conn_opt;

    tx_conn->close();
    eth_state_changed(false);
  }

private:
  void
  forward_packet_and_reset_builder(size_t tx,
                                   piwo::forward_w_cid_builder& forward_builder,
                                   piwo::raw_packet raw_packet)
  {
    std::optional tx_conn_opt = this->get_tx_eth_conn(tx);
    if (!tx_conn_opt.has_value())
    {
      // reset builder and exit
      std::optional forward_builder_opt =
        piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);

      forward_builder = forward_builder_opt.value();
      return;
    }
    // forward packet
    auto& tx_conn = *tx_conn_opt;
    auto forward = piwo::forward_w_cid(forward_builder);
    tx_conn->tcp_send(piwo::raw_packet(forward.data(), forward.size()));

    // reset builder
    std::optional forward_builder_opt =
      piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);

    forward_builder = forward_builder_opt.value();
  }

private:
  modinfo_t _modules;
  uresolution_t _resolution{ .width = 0, .height = 0 };
  application_state_t _application_state = application_state_t::IDLE;
  event_loop _event_loop;
  providers_register _providers;
  snake_connection _snake_connection;
  std::atomic<size_t> _snake_move_time_ms = INITIAL_SNAKE_MOVE_TIME_MS;
  std::atomic_bool _is_snake_started{ false };

  std::array<tx_eth_connection, MAX_TX_COUNT> _tx_conn;
  ls_sending_task _ls_task;

  std::recursive_mutex _modules_mtx;
  std::recursive_mutex _resolution_mtx;
  std::recursive_mutex _application_state_mtx;
  std::recursive_mutex _event_loop_mtx;
};

inline config_t
  global_config; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
