#pragma once

#include <atomic>
#include <list>
#include <memory>
#include <mutex>
#include <thread>

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

constexpr const char* server_port = "30001";
constexpr int8_t data_delimiter = std::numeric_limits<int8_t>::max();

enum class signals_to_server : int8_t
{
  restart_game = -4,
  stop_game = -5,
  board_size = -6,
  clients_count = -7,
  snake_move_time = -8
};

using tcp_socket_t = boost::asio::ip::tcp::socket;
using ssl_socket_t = boost::asio::ssl::stream<tcp_socket_t>;
using ssl_socket_ptr_t = std::shared_ptr<ssl_socket_t>;

using snake_send_type = std::vector<int8_t>;
using snake_send_iterator = std::list<snake_send_type>::iterator;

class snake_connection;

class snake_receive_task
{
public:
  snake_receive_task(ssl_socket_ptr_t& socket, snake_connection* observer);
  ~snake_receive_task();

  void
  replace_socket_ptr(const ssl_socket_ptr_t& socket);
  void
  start_receive_data();

private:
  void
  receive_data(const ssl_socket_ptr_t& socket);

  void
  wait_until_receive_task_finished();

  void
  notify_receive_task_finished();

  void
  refresh_data_buffer(size_t bytes_with_delimiter);

  void
  notify_data_received(const snake_send_type& data, size_t size);

  ssl_socket_ptr_t _socket_ptr;
  std::mutex _socket_mx;
  std::vector<int8_t> _data_received;

  bool _is_receive_started = false;
  std::mutex _is_started_mx;
  std::condition_variable _is_started_cv;

  snake_connection* _snake_observer;
};

class snake_send_task
{
public:
  snake_send_task(ssl_socket_ptr_t& socket, snake_connection* observer);
  ~snake_send_task();

  void
  replace_socket_ptr(const ssl_socket_ptr_t& socket);

  void
  send_data(const std::vector<int8_t>& data);

  void
  send_large_number(signals_to_server signal, size_t number);

  void
  start_task();

  void
  stop_task();

private:
  void
  erase_el_from_queue(const snake_send_iterator& it);

  void
  execute_send(std::unique_lock<std::mutex>&& ul_send_mx);

  void
  send_loop();

  ssl_socket_ptr_t _socket_ptr;
  std::mutex _socket_mx;
  std::atomic<bool> _send_executing{ false };
  std::atomic<bool> _end_task{ false };

  std::list<snake_send_type> _send_queue;
  std::mutex _send_mx;
  std::condition_variable _send_data_cv;
  std::thread _send_loop_th;

  snake_connection* _snake_observer;
};

class snake_connection
{
public:
  snake_connection();
  ~snake_connection();

  bool
  connect(const char* host);

  void
  disconnect();

  void
  send_data(const snake_send_type& data);

  void
  send_signal_with_large_number(signals_to_server signal, size_t number);

  void
  update_data_received(const snake_send_type& data);

  bool
  is_socket_connected() const;

private:
  void
  close_socket();

  void
  restart_io_context();

  size_t
  decode_clients_count(const snake_send_type& data_clients);

  void
  join_context_runner();

  void
  realloc_socket_ptr();

  void
  on_socket_connected();

  boost::asio::io_context _io_context;
  boost::asio::ssl::context _ssl_context;
  ssl_socket_ptr_t _socket_ptr;
  std::mutex _socket_mx;
  std::thread _io_context_runner;
  std::atomic<bool> _is_connected{ false };

  snake_receive_task _receive_task;
  snake_send_task _send_task;
};
