#pragma once
#include <algorithm>
#include <map>
#include <memory>
#include <mutex>
#include <optional>
#include <spdlog/spdlog.h>

#include "frame_provider.hpp"
#include "frame_provider_utilities.hpp"
#include "playlist.hpp"
#include "render_engine.hpp"
#include "snake_frame_provider.hpp"

struct config_t;

struct providers_register
{
  friend struct config_t;

  static inline const std::string RENDER_ENGINE_INDEX = "render";
  static inline const std::string PLAYLIST_INDEX = "playlist";
  static inline const std::string SNAKE_PROVIDER_INDEX = "snake";

  template<typename T>
  void
  add_provider(std::unique_ptr<T>&& provider)
  {
    std::unique_lock<std::recursive_mutex> lock(_providers_mtx);
    if (provider_can_be_removed(provider->get_name()))
    {
      spdlog::warn("overwriting the provider with given name prohibited");
      return;
    }
    _providers[provider->get_name()] = std::move(provider);
  }

  void
  remove_provider(const std::string& name)
  {
    std::unique_lock<std::recursive_mutex> lock(_providers_mtx);
    if (!provider_can_be_removed(name))
    {
      return;
    }
    _providers.erase(name);
  }

  providers_register()
  {
    _providers[RENDER_ENGINE_INDEX] =
      std::make_unique<render_engine>(RENDER_ENGINE_INDEX, 1, 1);

    _providers[PLAYLIST_INDEX] = std::make_unique<playlist_t>(
      resolution_t<size_t>{ .width = 0, .height = 0 }, PLAYLIST_INDEX);

    _providers[SNAKE_PROVIDER_INDEX] =
      std::make_unique<snake_frame_provider>(SNAKE_PROVIDER_INDEX, 0, 0);
  }

  auto
  acquire_render_engine()
  {
    auto* re =
      static_cast<render_engine*>(_providers[RENDER_ENGINE_INDEX].get());
    return std::make_tuple(std::unique_lock(this->_render_engine_mtx),
                           std::ref(*re));
  }

  auto
  acquire_playlist()
  {
    auto* pl = static_cast<playlist_t*>(_providers[PLAYLIST_INDEX].get());
    return std::make_tuple(std::unique_lock(this->_playlist_mtx),
                           std::ref(*pl));
  }

  auto
  acquire_providers()
  {
    return std::make_tuple(std::unique_lock(this->_providers_mtx),
                           std::ref(this->_providers));
  }

  std::weak_ptr<frame_provider>
  clone_provider(const std::string& provider_name)
  {
    std::unique_lock<std::recursive_mutex> lock(_providers_mtx);
    if (this->_providers.count(provider_name) == 0)
    {
      return {};
    }
    return this->_providers[provider_name];
  }

private:
  bool
  provider_can_be_removed(const std::string& name)
  {
    if (name == RENDER_ENGINE_INDEX || name == PLAYLIST_INDEX)
      return false;

    return true;
  }

  render_engine*
  get_render_engine()
  {
    auto* re =
      static_cast<render_engine*>(_providers[RENDER_ENGINE_INDEX].get());
    return re;
  }

  playlist_t*
  get_playlist()
  {
    auto* pl = static_cast<playlist_t*>(_providers[PLAYLIST_INDEX].get());
    return pl;
  }

  snake_frame_provider*
  get_snake_provider()
  {
    auto* pl = static_cast<snake_frame_provider*>(
      _providers[SNAKE_PROVIDER_INDEX].get());
    return pl;
  }

  const providers_map_t&
  get_providers()
  {
    std::unique_lock<std::recursive_mutex> lock(_providers_mtx);
    return this->_providers;
  }

  providers_map_t _providers;
  std::recursive_mutex _providers_mtx;
  std::recursive_mutex _render_engine_mtx;
  std::recursive_mutex _playlist_mtx;
  std::recursive_mutex _snake_provider_mtx;
};
