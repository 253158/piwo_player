#pragma once

#include <map>

#include <piwo/common_intersection_operators.h>
#include <piwo/frames_intersection.h>

// here you can implement common frame operators
// see libpiwo -> common_intersection_operators file to see the examples

inline std::map<std::string, piwo::frames_intersection_operator>
  effect_lookup = {
    { "copy (= operator)", piwo::frame_assign_op },
    { "copy (| operator)", piwo::frame_or_op },
    { "copy (& operator)", piwo::frame_add_op },
    { "substract", piwo::frame_sub_op },
  };
