#pragma once

#include <boost/asio/ip/udp.hpp>
#include <iterator>
#include <list>
#include <memory>
#include <mutex>
#include <optional>
#include <string>
#include <thread>
#include <vector>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/use_future.hpp>
#include <boost/asio/write.hpp>

#include <spdlog/spdlog.h>

#include <piwo/animation.h>
#include <piwo/color.h>
#include <piwo/frame.h>
#include <piwo/proto.h>

#include "frame_provider.hpp"
#include "suspendable_thread.hpp"

#include "test_iface.hpp"

using network_packet_buffer_t = std::basic_string<std::byte>;
using network_packet_queue_t = std::list<network_packet_buffer_t>;
using network_packet_queue_iterator_t = network_packet_queue_t::iterator;
constexpr size_t FORAWRDING_BUFFER_SIZE = 1500;

class tx_eth_connection;

struct network_packet_write_handler
{
  void
  operator()(boost::system::error_code error, std::size_t bytes_written);

  tx_eth_connection& master_connection;
  network_packet_queue_iterator_t packet_node;
};

struct tx_config
{
  /* here you can pass other tx configuration variables */
  unsigned int radios_on_board;
};

class tx_eth_connection
{
public:
  using packet_t = piwo::raw_packet;
  using mutex_t = std::mutex;
  using tcp_socket_t = boost::asio::ip::tcp::socket;
  using udp_socket_t = boost::asio::ip::udp::socket;

  friend struct network_packet_write_handler;

public:
  tx_eth_connection()
    : _tcp_io_context()
    , _udp_io_context()
    , _tcp_socket(this->_tcp_io_context)
    , _udp_socket(this->_udp_io_context)
    , _packet_queue_mtx()
    , _packet_queue()
  {
    // TODO move tx configuration into function that will
    // read the config from TX using piwo protcol
    this->_config = { .radios_on_board = 4 };
  }

  tx_eth_connection(const tx_eth_connection& other) = delete;
  tx_eth_connection(tx_eth_connection&& other) = delete;

  tx_eth_connection&
  operator=(const tx_eth_connection& other) = delete;
  tx_eth_connection&
  operator=(tx_eth_connection&& other) = delete;

  ~tx_eth_connection();

  bool
  open(const char* host, const char* tcp_port, const char* udp_port);

  bool
  tcp_send(packet_t packet);

  bool
  udp_send(packet_t packet);

  void
  close();

  void
  start();

  [[nodiscard]] bool
  is_opened() const
  {
    return this->_tcp_socket.is_open();
  }

  TEST_IFACE void
  enqueue(packet_t packet);

  const tx_config&
  get_config()
  {
    return this->_config;
  }

private:
  void
  remove_pending(network_packet_queue_iterator_t packet_it);

  network_packet_queue_iterator_t
  make_packet_queue_node_locked(packet_t packet);

  network_packet_write_handler
  make_write_packet_handler(network_packet_queue_iterator_t node_it);

  void
  join_io_worker();

  void
  restart_io_worker();

private:
  boost::asio::io_context _tcp_io_context;
  boost::asio::io_context _udp_io_context;
  boost::asio::ip::udp::endpoint _udp_endpoint;
  tcp_socket_t _tcp_socket;
  udp_socket_t _udp_socket;
  std::thread _udp_io_context_runner;

  // TODO: provide mutexless packet queue?
  mutex_t _packet_queue_mtx;
  mutex_t _udp_write_mtx;
  network_packet_queue_t _packet_queue;
  tx_config _config;
};

struct tx_pixel_owner_info
{
  using offset_array_t = std::vector<size_t>;

  void
  own(const piwo::frame& frame, size_t x, size_t y)
  {
    owned_offsets.emplace_back(x + frame.width * y);
  }

  void
  clear_owned()
  {
    this->owned_offsets.clear();
  }

  offset_array_t owned_offsets;
};

struct lightshow_config
{
  static constexpr uint8_t DEFAULT_TTF = 50;
  static constexpr auto UNDEFINED_ID = std::numeric_limits<uint32_t>::max();

  struct owned_pixel_info
  {
    size_t real_offset;
    piwo::color color;
  };

  struct config_id
  {
    unsigned int tx, radio;
  };

  [[nodiscard]] std::optional<owned_pixel_info>
  get_nth(size_t n) const
  {
    if (n >= this->owner_info.owned_offsets.size())
      return std::nullopt;

    const auto pixel_offset = this->owner_info.owned_offsets[n];
    return owned_pixel_info{ .real_offset = this->device_offset + n,
                             .color = this->frame->at_flat_(pixel_offset) };
  }

  size_t device_offset = 0;
  tx_pixel_owner_info owner_info;
  config_id id = { UNDEFINED_ID, UNDEFINED_ID };
  std::shared_ptr<piwo::frame> frame;
};

void
send_frame_via_lightshow(const lightshow_config& config,
                         tx_eth_connection& output_connection);

class ls_sending_task
{
public:
  using ls_config_array = std::vector<lightshow_config>;
  using ms_t = std::chrono::milliseconds;
  static constexpr ms_t DEFAULT_FRAME_DURATION{ 1000 };

  void
  stop();

  bool
  stop_and_configure(std::shared_ptr<frame_provider> p,
                     ls_config_array configs);

  bool
  run()
  {
    if (!this->_provider)
      return false;
    this->_ls_send_task.resume();
    return true;
  }

private:
  void
  ls_send_loop();

private:
  ls_config_array _configs;
  std::shared_ptr<frame_provider> _provider;
  ms_t _frame_duration{ DEFAULT_FRAME_DURATION };

  suspendable_thread _ls_send_task{ [this] { this->ls_send_loop(); } };
};
