#pragma once
#include <algorithm>
#include <map>
#include <memory>
#include <mutex>
#include <optional>

#include "frame_provider.hpp"

using providers_map_t = std::map<std::string, std::shared_ptr<frame_provider>>;
