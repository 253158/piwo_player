#pragma once

#include "ui_iface.hpp"

enum class application_state_t
{
  IDLE = 0,
  DISPLAY_AUTO_CONFIG,
  LIGHTSHOW
};

template<typename C>
void
state_handler_scope(C&& body,
                    application_state_t& state) requires std::invocable<C>
{
  body();
  state = application_state_t::IDLE;
  application_state_changed(state);
}
