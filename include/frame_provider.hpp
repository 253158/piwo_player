#pragma once
#include <atomic>
#include <memory>
#include <mutex>
#include <vector>

#include <piwo/frame.h>
#include <piwo/frames_intersection.h>

#include "effects.hpp"

class frame_provider
{
public:
  using id = std::string;

  frame_provider(bool is_waitable)
    : is_waitable(is_waitable)
  {
  }

  virtual ~frame_provider() = default;

  virtual id
  get_name() = 0;

  void
  resize(size_t width, size_t height)
  {
    // today we will use the mutex over shared_ptr to
    // enforce the thread safe shared_ptr modification
    // there is atomic_shared_ptr in experimental std library
    // but we need to wait for the moment of it's implementation
    std::unique_lock<std::mutex> lck(mtx);
    resize_(width, height);
  }

  piwo::frames_intersection::frame_t
  get_frame()
  {
    // same as above
    std::unique_lock<std::mutex> lck(mtx);
    return get_frame_();
  }

  bool
  waitable()
  {
    return is_waitable;
  }

  virtual void
  wait()
  {
    assert(false && "Frame provider does not implement the wait function");
  }

  bool is_waitable;
  std::mutex mtx;

protected:
  virtual piwo::frames_intersection::frame_t
  get_frame_() = 0;

  virtual void
  resize_(size_t width, size_t height) = 0;
};
