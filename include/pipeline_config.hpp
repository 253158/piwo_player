#pragma once
#include <array>
#include <memory>
#include <optional>
#include <piwo/frames_intersection.h>
#include <string>
#include <utility>

#include "frame_provider.hpp"

using step_id = int64_t;
static constexpr step_id INVALID_STEP_ID = -1;

struct pipeline_config
{
  frame_provider::id source_provider_name;
  frame_provider::id destination_provider_name;
  int64_t off_x, off_y;
  std::string effect;
  step_id pipeline_id = INVALID_STEP_ID; // changed only when pipeline is
                                         // correctly added
  bool is_highlighted = false;
  bool state = true;

  bool
  operator==(const step_id id)
  {
    return this->pipeline_id == id;
  }
};

struct render_engine_step
{
  static inline step_id next_id = 0;
  friend class render_engine;

private:
  render_engine_step(std::weak_ptr<frame_provider> fp1_w,
                     std::weak_ptr<frame_provider> fp2_w,
                     const piwo::frames_intersection::frame_t& f1,
                     const piwo::frames_intersection::frame_t& f2,
                     int64_t offx,
                     int64_t offy,
                     piwo::frames_intersection_operator e)
    : id(next_id++)
    , off_x(offx)
    , off_y(offy)
    , active(true)
    , effect(e)
    , _intersection(f2, f1, offx, offy)
    , _source_provider(std::move(fp1_w))
    , _destination_provider(std::move(fp2_w))
  {
  }

public:
  static std::optional<render_engine_step>
  make_step(std::weak_ptr<frame_provider> fp1_w,
            std::weak_ptr<frame_provider> fp2_w,
            int64_t offx,
            int64_t offy,
            piwo::frames_intersection_operator e)
  {
    auto fp1 = fp1_w.lock();
    if (!fp1)
      return std::nullopt;

    auto fp2 = fp2_w.lock();
    if (!fp2)
      return std::nullopt;

    return render_engine_step(
      fp1, fp2, fp1->get_frame(), fp2->get_frame(), offx, offy, e);
  }

  bool
  operator==(const step_id id)
  {
    return this->id == id;
  }

  bool
  refresh()
  {

    auto fp1 = _source_provider.lock();
    if (!fp1)
      return false;

    auto fp2 = _destination_provider.lock();
    if (!fp2)
      return false;

    this->_intersection = piwo::frames_intersection(
      fp2->get_frame(), fp1->get_frame(), off_x, off_y);
    return true;
  }

  step_id id;
  int64_t off_x, off_y;
  bool active;
  piwo::frames_intersection_operator effect;

private:
  piwo::frames_intersection _intersection;
  std::weak_ptr<frame_provider> _source_provider, _destination_provider;
};
