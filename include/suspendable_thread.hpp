#pragma once

#include <condition_variable>
#include <mutex>
#include <thread>

#include <spdlog/spdlog.h>

#ifdef __WIN32
#include <processthreadsapi.h>
#elif __linux__
#include <signal.h>
#endif

#include "threading.hpp"

class synchronizable
{
private:
  using event_t = std::condition_variable;
  using mutex_t = std::mutex;
  using lock_t = std::unique_lock<mutex_t>;

  // Mutex for changing sleep / wake state
  mutex_t event_mutex;

  // Event that tells synchronized object to go to sleep
  event_t wake_up_event;

  // Event that tells user of synchronizer that object is sleeping
  event_t going_sleep_event;

  // Event that tells user of synchronizer that object is waking up
  event_t waking_up_event;

  bool stop_requested{ true };
  bool stop_done{ false };
  bool alive{ true };

private:
  void
  _stop(MOS_OWN_PROOF lock_t& lck)
    // clang-format off
  MOS_REQUIRES(this->event_mutex)
  // clang-format on
  {
    this->stop_requested = true;
  }

  void
  _stop_and_wait(MOS_OWN_PROOF lock_t& lck)
    // clang-format off
  MOS_REQUIRES(this->event_mutex)
  // clang-format on
  {
    // Mark stop request
    this->_stop(lck);

    // Wait for thread to go sleep
    this->going_sleep_event.wait(lck,
                                 [this]() -> bool { return this->stop_done; });
  }

  void
  _resume(MOS_OWN_PROOF lock_t& lck)
    // clang-format off
  MOS_REQUIRES(this->event_mutex)
  // clang-format on
  {
    this->stop_requested = false;
    this->wake_up_event.notify_all();
  }

  void
  _resume_and_wait(MOS_OWN_PROOF lock_t& lck)
    // clang-format off
  MOS_REQUIRES(this->event_mutex)
  // clang-format on
  {
    this->_resume(lck);

    this->waking_up_event.wait(lck, [this] { return !this->stop_done; });
  }

  bool
  _is_stopped(MOS_OWN_PROOF lock_t& lck)
    // clang-format off
  MOS_REQUIRES(this->event_mutex)
  // clang-format on
  {
    return this->stop_done;
  }

  void
  _wait(MOS_OWN_PROOF lock_t& lck)
    // clang-format off
  MOS_REQUIRES(this->event_mutex)
  // clang-format on
  {
    if (this->stop_requested)
    {
      // Mark sleep state
      this->stop_done = true;

      // Notify all that is waiting for this thread to sleep
      this->going_sleep_event.notify_all();

      // Wait until someone wakes this thread, wait will also
      // release event mutex
      this->wake_up_event.wait(lck, [this] { return !this->stop_requested; });

      this->stop_done = false;
      this->waking_up_event.notify_all();
    }
  }

  bool
  _is_stop_requested(MOS_OWN_PROOF lock_t& lck)
    // clang-format off
  MOS_REQUIRES(this->event_mutex)
  // clang-format on
  {
    return this->stop_requested;
  }

  void
  _kill(MOS_OWN_PROOF lock_t& lck)
    // clang-format off
  MOS_REQUIRES(this->event_mutex)
  // clang-format on
  {
    this->alive = false;
    this->_resume(lck);
  }

  bool
  _is_alive(MOS_OWN_PROOF lock_t& lck)
    // clang-format off
  MOS_REQUIRES(this->event_mutex)
  // clang-format on
  {
    return this->alive;
  }

  void
  _on_thread_exit(MOS_OWN_PROOF lock_t& lck)
    // clang-format off
  MOS_REQUIRES(this->event_mutex)
  // clang-format on
  {
    this->stop_done = true;
    this->going_sleep_event.notify_all();
  }

public:
  void
  stop()
    // clang-format off
  MOS_ACQUIRES(this->event_mutex)
  MOS_RELEASES(this->event_mutex)
  // clang-format on
  {
    std::unique_lock lck(this->event_mutex);
    this->_stop(lck);
  }

  void
  stop_and_wait()
    // clang-format off
  MOS_ACQUIRES(this->event_mutex)
  MOS_RELEASES(this->event_mutex)
  // clang-format on
  {
    std::unique_lock lck(this->event_mutex);
    this->_stop_and_wait(lck);
  }

  void
  resume()
    // clang-format off
  MOS_ACQUIRES(this->event_mutex)
  MOS_RELEASES(this->event_mutex)
  // clang-format on
  {
    std::unique_lock lck(this->event_mutex);
    this->_resume(lck);
  }

  void
  resume_and_wait()
    // clang-format off
  MOS_ACQUIRES(this->event_mutex)
  MOS_RELEASES(this->event_mutex)
  // clang-format on
  {
    std::unique_lock lck(this->event_mutex);
    this->_resume(lck);
  }

  bool
  is_stopped()
    // clang-format off
  MOS_ACQUIRES(this->event_mutex)
  MOS_RELEASES(this->event_mutex)
  // clang-format on
  {
    std::unique_lock lck(this->event_mutex);
    return this->_is_stopped(lck);
  }

  void
  wait()
    // clang-format off
  MOS_ACQUIRES(this->event_mutex)
  MOS_RELEASES(this->event_mutex)
  // clang-format on
  {
    std::unique_lock lck(this->event_mutex);
    this->_wait(lck);
  }

  bool
  is_stop_requested()
    // clang-format off
  MOS_ACQUIRES(this->event_mutex)
  MOS_RELEASES(this->event_mutex)
  // clang-format on
  {
    std::unique_lock lck(this->event_mutex);
    return this->_is_stop_requested(lck);
  }

  void
  kill()
    // clang-format off
  MOS_ACQUIRES(this->event_mutex)
  MOS_RELEASES(this->event_mutex)
  // clang-format on
  {
    std::unique_lock lck(this->event_mutex);
    this->_kill(lck);
  }

  bool
  is_alive()
    // clang-format off
  MOS_ACQUIRES(this->event_mutex)
  MOS_RELEASES(this->event_mutex)
  // clang-format on
  {
    std::unique_lock lck(this->event_mutex);
    return this->_is_alive(lck);
  }

  // base class is responsible for
  // invoking this function in worker thread
  // on thread exit
  void
  on_thread_exit()
    // clang-format off
  MOS_ACQUIRES(this->event_mutex)
  MOS_RELEASES(this->event_mutex)
  // clang-format on
  {
    std::unique_lock lck(this->event_mutex);
    this->_on_thread_exit(lck);
  }
};

class suspendable_thread : public synchronizable
{
private:
  std::thread worker;

public:
  suspendable_thread() = default;

  template<class T, class... Args>
  suspendable_thread(T&& callable, Args&&... args)
  {
    this->worker = std::thread(
      [this](T&& callable, Args&&... args)
      {
        while (1)
        {
          this->wait();

          if (!this->is_alive())
            break;

          std::invoke(std::forward<T>(callable), std::forward<Args>(args)...);
        }
        this->on_thread_exit();
      },
      std::forward<T>(callable),
      std::forward<Args>(args)...);
  }

  suspendable_thread&
  operator=(const suspendable_thread& thread) = delete;
  suspendable_thread&
  operator=(suspendable_thread&& thread) = delete;

  ~suspendable_thread()
  {
    this->kill();
    if (this->worker.joinable())
      this->worker.join();
  }

  void
  join() noexcept
  {
    worker.join();
  }

  auto
  native_handle() noexcept
  {
    return this->worker.native_handle();
  }

  void
  _terminate() noexcept
  {
#ifdef __WIN32
    ::TerminateThread(pthread_gethandle(this->worker.native_handle()), 1);
#elif __linux__
    ::pthread_kill(this->worker.native_handle(), SIGUSR1);
#endif
    this->worker.detach();
  }
};
