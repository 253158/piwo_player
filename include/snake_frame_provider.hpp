#pragma once

#include <memory>
#include <vector>

#include <piwo/frame.h>

#include "frame_provider.hpp"

using snake_data_t = std::vector<uint8_t>;

class snake_frame_provider : public frame_provider
{
public:
  snake_frame_provider(const std::string& name, size_t width, size_t height)
    : frame_provider(false)
    , _name(name)
  {
    this->_frame = piwo::alloc_frame(width, height);
  }

  frame_provider::id
  get_name() override
  {
    return this->_name;
  }

  piwo::frames_intersection::frame_t
  get_frame_() override
  {
    return this->_frame;
  }

  void
  resize_(size_t width, size_t height) override
  {
    spdlog::info("Changing frame resolution of\
        snake frame provider to width->{} height->{}",
                 width,
                 height);
    this->_frame = piwo::alloc_frame_shared(width, height);
  }

  void
  fill_frame(const snake_data_t& snake_data)
  {
    constexpr piwo::color background_color{ 0, 0, 0 }; // black
    constexpr piwo::color snake_color{ 255, 255, 0 };  // yellow

    std::lock_guard lg(this->mtx);
    this->_frame->fill(background_color);

    for (size_t i = 0; i < snake_data.size() - 1; i += 2)
    {
      if (snake_data[i] < this->_frame->height &&
          snake_data[i + 1] < this->_frame->width)
      {
        this->_frame->at_(snake_data[i + 1], snake_data[i]) = snake_color;
      }
    }
  }

private:
  std::shared_ptr<piwo::frame> _frame;
  const std::string _name;
};
