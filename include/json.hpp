#pragma once

#include <rapidjson/document.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/rapidjson.h>
#include <rapidjson/writer.h>

#include <iostream>
#include <string>

#ifdef _WIN32
#undef GetObject
#endif

using serialization_writer = rapidjson::PrettyWriter<rapidjson::StringBuffer>;
using json_object = rapidjson::Document::Object;

template<typename T>
std::string
serialize(T&& serializable)
{
  rapidjson::StringBuffer ss;
  rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(ss);
  serialize(std::forward<T>(serializable), writer);
  return ss.GetString();
}

template<typename T>
void
serialize(T&& serializable, std::ostream& f)
{
  f << serialize(std::forward<T>(serializable));
}

template<typename T>
void
serialize(T&& serializable, std::ostream&& f)
{
  f << serialize(std::forward<T>(serializable));
}

template<typename T>
bool
deserialize(T&& serializable, std::istream& f)
{
  rapidjson::IStreamWrapper isw(f);

  rapidjson::Document d;
  d.ParseStream(isw);

  return deserialize(std::forward<T>(serializable), d.GetObject());
}

template<typename T>
bool
deserialize(T&& serializable, std::istream&& f)
{
  rapidjson::IStreamWrapper isw(f);

  rapidjson::Document d;
  d.ParseStream(isw);

  return deserialize(std::forward<T>(serializable), d.GetObject());
}

template<typename T>
bool
deserialize(T& t, json_object d);

template<typename T>
void
serialize(T& t, serialization_writer& writer);
