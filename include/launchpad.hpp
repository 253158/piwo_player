#pragma once

#include <algorithm>
#include <atomic>
#include <chrono>
#include <condition_variable>

#include <mipc/base.h>
#include <mipc/file.h>
#include <mipc/pipe_conn.h>
#include <mipc/shmem.h>
#include <mipc/utils.h>
#include <mutex>
#include <piwo/common_intersection_operators.h>
#include <piwo/frame.h>
#include <piwo/frames_intersection.h>
#include <piwo/lp_proto.h>
#include <piwo/lp_protodef.h>
#include <piwo/protodef.h>
#include <piwo/shm_wrapper.h>
#include <spdlog/spdlog.h>

#include "frame_provider.hpp"
#include "suspendable_thread.hpp"
#include "typeutils.hpp"

class launchpad : public frame_provider
{
  constexpr static auto TRANSACTION_TIMEOUT = std::chrono::seconds(2);
  const char* _fifo_name = "lpd_fifo";
  const char* _shm_name = "lpd_display_buffer";

public:
  enum class transaction
  {
    ok,
    rejected,
    busy,
    timed_out,
    failure,
    shm_reload,
  };
  enum class shmem_info
  {
    display_width = 0,
    display_height = 1,
    row_size = 2,
    fps = 3,
    data_offset = 4,
  };

  template<typename Conn = mipc::pipe::conn_t,
           typename Cv = std::condition_variable>
  class state_machine
  {
    friend class launchpad;

    enum class state_t : unsigned int
    {
      player_request_ownership =
        piwo::underlay_cast(piwo::lp_packet::PLAYER_GET_OWNERSHIP),
      launchpad_request_ownership =
        piwo::underlay_cast(piwo::lp_packet::LAUNCH_GET_OWNERSHIP),
      player_send_resize = piwo::underlay_cast(piwo::lp_packet::PLAYER_RESIZE),
      player_send_accept = piwo::underlay_cast(piwo::lp_packet::PLAYER_ACCEPT),
      player_send_reject = piwo::underlay_cast(piwo::lp_packet::PLAYER_REJECT),
      player_received_accept =
        piwo::underlay_cast(piwo::lp_packet::LAUNCH_ACCEPT),
      player_received_reject =
        piwo::underlay_cast(piwo::lp_packet::LAUNCH_REJECT),
      player_received_shmreload =
        piwo::underlay_cast(piwo::lp_packet::LAUNCH_RELOAD_SHM),
      idle = 0xff,
    };

  private:
    std::atomic<state_t> _current_state = state_t::idle;
    Cv _cv;
    std::mutex _mtx;
    std::mutex _mtx_rst;
    piwo::lp_data_type _packet_buffer[piwo::lp_max_packet_size];

    // the method can be invoked only inside the methods
    // that uniquely owned the mutex
    bool
    get_ownership(Conn& p)
    {
      if (this->_current_state != state_t::idle)
      {
        spdlog::warn("Can't own owenership. Current state {}",
                     piwo::underlay_cast(this->_current_state.load()));
        return false;
      }
      this->_current_state = state_t::player_request_ownership;

      piwo::lp_raw_packet raw_packet(this->_packet_buffer,
                                     piwo::player_ownership_length);

      auto ownership_builder_opt =
        piwo::p_ownership_builder::make_pownership_builder(raw_packet);
      if (!ownership_builder_opt.has_value())
      {
        spdlog::warn("Building ownership packet failed");
        return false;
      }

      auto ownership_builder = *ownership_builder_opt;
      piwo::p_ownership ownership_packet(ownership_builder);
      if (p.write(
            from_bytes<const char>(ownership_packet.bytes().begin().base()),
            ownership_packet.bsize()) == -1)
      {
        spdlog::warn("Writting ownership packet to pipe failed");
        return false;
      }
      spdlog::info("Ownership has been requested ");
      return true;
    }

    bool
    s_accept(Conn& p, piwo::lp_packet s)
    {
      piwo::lp_raw_packet raw_packet(_packet_buffer,
                                     piwo::player_resize_length);

      auto accept_builder_opt =
        piwo::p_accept_builder::make_paccept_builder(raw_packet);
      if (!accept_builder_opt.has_value())
      {
        spdlog::warn("Building accept packet failed");
        return false;
      }
      auto accept_builder = *accept_builder_opt;
      accept_builder.set_cmd(piwo::underlay_cast(s));
      piwo::p_accept accept_packet(accept_builder);
      if (p.write(from_bytes<const char>(accept_packet.bytes().data()),
                  accept_packet.bsize()) == -1)
      {
        spdlog::warn("Writting accept packet to pipe failed");
        return false;
      }
      spdlog::info("Accept packet send correctly");
      return true;
    }

    bool
    s_reject(Conn& p, piwo::lp_packet s)
    {
      piwo::lp_raw_packet raw_packet(_packet_buffer,
                                     piwo::player_resize_length);

      auto reject_builder_opt =
        piwo::p_reject_builder::make_preject_builder(raw_packet);
      if (!reject_builder_opt.has_value())
      {
        spdlog::warn("Building reject packet failed");
        return false;
      }
      auto reject_builder = *reject_builder_opt;
      reject_builder.set_cmd(piwo::underlay_cast(s));
      piwo::p_reject reject_packet(reject_builder);
      if (p.write(from_bytes<const char>(reject_packet.bytes().data()),
                  reject_packet.bsize()) == -1)
      {
        spdlog::warn("Writting reject packet to pipe failed");
        return false;
      }
      spdlog::info("Reject packet send correctly");
      return true;
    }

  public:
    [[nodiscard]] transaction
    handle_received(Conn& p, piwo::lp_raw_packet& raw_packet)
    {
      std::unique_lock<std::mutex> lck(this->_mtx_rst);

      switch (raw_packet.data()[piwo::lp_common_type_pos])
      {
        case piwo::underlay_cast(piwo::lp_packet::LAUNCH_GET_OWNERSHIP):
        {
          if (!this->_mtx.try_lock() || this->_current_state != state_t::idle)
          {
            spdlog::warn(
              "Player is currently under transaction. Ownership rejected");
            if (!this->s_reject(p, piwo::lp_packet::LAUNCH_GET_OWNERSHIP))
            {
              return transaction::failure;
            }
            return transaction::busy;
          }
          if (!this->s_accept(p, piwo::lp_packet::LAUNCH_GET_OWNERSHIP))
          {
            this->_mtx.unlock();
            return transaction::failure;
          }
          this->_current_state = state_t::launchpad_request_ownership;
          spdlog::info("Launchpad ownership request handled");
          this->_mtx.unlock();
          return transaction::ok;
        }
        case piwo::underlay_cast(piwo::lp_packet::LAUNCH_ACCEPT):
        {
          if (this->_current_state == state_t::idle)
          {
            spdlog::warn("Accept packet to old.");
            return transaction::timed_out;
          }

          if (piwo::underlay_cast(this->_current_state.load()) !=
              raw_packet.data()[piwo::launch_accept_cmd_pos])
          {
            spdlog::warn(
              "Received command does not match the send one. {} != {}",
              raw_packet.data()[piwo::player_accept_cmd_pos],
              piwo::underlay_cast(this->_current_state.load()));
            return transaction::failure;
          }
          spdlog::info("Launchpad accepted request for command {}",
                       piwo::underlay_cast(this->_current_state.load()));
          this->_current_state = state_t::player_received_accept;
          this->_cv.notify_all();
          return transaction::ok;
        }
        case piwo::underlay_cast(piwo::lp_packet::LAUNCH_REJECT):
        {
          if (this->_current_state == state_t::idle)
          {
            spdlog::warn("Reject packet to old.");
            return transaction::timed_out;
          }
          if (piwo::underlay_cast(this->_current_state.load()) !=
              raw_packet.data()[piwo::launch_reject_cmd_pos])
          {
            spdlog::warn(
              "Received command does not match the send one. {} != {}",
              raw_packet.data()[piwo::player_reject_cmd_pos],
              piwo::underlay_cast(_current_state.load()));
            return transaction::failure;
          }
          spdlog::info("Launchpad rejected request for command {}",
                       piwo::underlay_cast(this->_current_state.load()));
          this->_current_state = state_t::player_received_reject;
          this->_cv.notify_all();
          return transaction::ok;
        }
        case piwo::underlay_cast(piwo::lp_packet::LAUNCH_RELOAD_SHM):
        {
          spdlog::info("Launchpad reloaded shm");
          this->_current_state = state_t::player_received_shmreload;
          this->_cv.notify_all();
          return transaction::shm_reload;
        }
        default:
        {
          spdlog::warn("Received unknown command {}",
                       raw_packet.data()[piwo::common_type_pos]);
          return transaction::failure;
        }
      }
    }

    transaction
    resize(Conn& p)
    {
      std::unique_lock<std::mutex> lck(this->_mtx);
      if (!this->get_ownership(p))
      {
        return transaction::rejected;
      }
      this->_current_state = state_t::player_send_resize;
      piwo::lp_raw_packet raw_packet(this->_packet_buffer,
                                     piwo::player_resize_length);

      auto resize_builder_opt =
        piwo::p_resize_builder::make_presize_builder(raw_packet);
      if (!resize_builder_opt.has_value())
      {
        spdlog::warn("Building resize packet failed {}");
        return transaction::failure;
      }

      auto resize_builder = *resize_builder_opt;
      piwo::p_resize resize_packet(resize_builder);
      p.write(from_bytes<const char>(resize_packet.bytes().data()),
              resize_packet.bsize());

      bool ret = this->_cv.wait_for(
        lck,
        TRANSACTION_TIMEOUT,
        [this] { return this->_current_state != state_t::player_send_resize; });
      if (!ret)
      {
        spdlog::warn(
          "Waiting for response from launchpad to long. Resize rejected");
        spdlog::warn("This might be caused by some communication error. "
                     "Checkout the pipe connection");
        this->_current_state = state_t::idle;
        return transaction::timed_out;
      }

      if (this->_current_state != state_t::player_received_accept)
      {
        spdlog::warn("Launchpad reject the transaction");
        this->_current_state = state_t::idle;
        return transaction::rejected;
      }

      spdlog::info("Resize packet send correctly");
      this->_current_state = state_t::idle;
      return transaction::ok;
    }

    void
    reset()
    {
      std::unique_lock<std::mutex> lck(this->_mtx_rst);
      this->_current_state = state_t::idle;
      this->_cv.notify_all();
    }
  };

public:
  launchpad(const std::string& name)
    : frame_provider(false)
    , _lp_connection_handler(&launchpad::run, std::ref(*this))
    , _name(name)
  {
    mipc::init();
  }

  ~launchpad() { this->disconnect(); }

  bool
  connect()
  {
    this->_pipe = mipc::pipe::conn_t::open(this->_fifo_name);
    if (!this->shm_init())
    {
      return false;
    }
    this->_lp_connection_handler.resume();
    return true;
  }

  bool
  disconnect()
  {
    this->_lp_connection_handler.stop_and_wait();
    this->_sm.reset();
    this->_pipe.close();
    return true;
  }

  piwo::frames_intersection::frame_t
  get_frame_() override
  {
    return {};
  }

  void
  resize_(size_t width, size_t height) override
  {
    // pause reading from shm
    this->_lhs = piwo::alloc_frame_shared(width, height);
    // start renderinf from shm
  }

  frame_provider::id
  get_name() override
  {
    return this->_name;
  }

private:
  transaction
  make_transaction(state_machine<mipc::pipe::conn_t>::state_t s)
  {
    switch (s)
    {
      case state_machine<mipc::pipe::conn_t>::state_t::player_send_resize:
        return this->_sm.resize(this->_pipe);
      default:
        return transaction::rejected;
    }
  }

private:
  void
  run()
  {
    if (this->_pipe.is_valid())
    {
      constexpr int wait_time = 1000;
      auto wait_status = this->_pipe.wait(wait_time);

      if (wait_status == -1)
      {
        spdlog::warn("pipe receiving error");
        this->_sm.reset();
        this->_lp_connection_handler.stop();
        return;
      }

      if (wait_status == 0)
      {
        spdlog::warn("pipe timed out");
        return;
      }
      piwo::lp_data_type psize = 0;
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      auto ret =
        this->_pipe.read(reinterpret_cast<char*>(&psize), sizeof(psize));
      if (ret == -1 || ret == 0)
      {
        spdlog::warn("Errc: {}", mipc::pipe::conn_t::host_error());
        this->_sm.reset();
        this->_lp_connection_handler.stop();
        return;
      }
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      std::copy_n(
        reinterpret_cast<char*>(&psize), sizeof(psize), this->_read_packet);

      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      ret = this->_pipe.read(
        reinterpret_cast<char*>(this->_read_packet) + sizeof(psize), psize - 1);
      if (ret == -1 || ret == 0)
      {
        spdlog::warn("Errc: {}", mipc::pipe::conn_t::host_error());
        this->_sm.reset();
        this->_lp_connection_handler.stop();
        return;
      }
      auto packet_opt =
        piwo::lp_raw_packet_safe::make_packet(this->_read_packet, psize);
      if (!packet_opt.has_value())
      {
        spdlog::warn("Can't construct packet");
        this->_sm.reset();
        return;
      }
      auto res = this->_sm.handle_received(_pipe, *packet_opt);
      if (res == transaction::timed_out || res == transaction::shm_reload)
      {
        this->_sm.reset();
        this->_lp_connection_handler.stop();
        spdlog::warn("shm requested reload");
      }
    }
  }

  bool
  shm_init()
  {
    const std::lock_guard<std::mutex> lock(_sm._mtx);
    this->_shmem = mipc::shmem::shmem_t::open(_shm_name);
    if (!this->_shmem.is_valid())
    {
      spdlog::debug("Failed to open shared memory.");
      return false;
    }
    const auto& config = this->_shmem.get_meminfo();
    uint32_t data_off =
      config[piwo::underlay_cast(shmem_info::data_offset)].GetUint();

    auto* shm_data_begin = from_bytes<uint32_t>(this->_shmem.get_payload());
    shm_data_begin += data_off;

    this->_rhs.reset(reinterpret_cast<piwo::frame*>(shm_data_begin));

    return true;
  }

private:
  state_machine<mipc::pipe::conn_t> _sm;
  uint8_t _read_packet[piwo::lp_max_packet_size];
  suspendable_thread _lp_connection_handler;
  mipc::pipe::conn_t _pipe;
  mipc::shmem::shmem_t _shmem;
  piwo::frames_intersection::frame_t _lhs;
  piwo::frames_intersection::frame_t _rhs;
  std::string _name;
};
