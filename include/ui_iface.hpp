#pragma once

#include "typeutils.hpp"
#include <piwo/protodef.h>

#include <spdlog/spdlog.h>

#include "frame_provider_utilities.hpp"
#include "pipeline_config.hpp"

#ifdef _WIN32
#undef GetObject
#endif

enum class application_state_t;

void
start_ui(int argc, char** argv);

/* General global config interface */

void
config_module_added(const piwo::uid uid);

void
config_module_removed(const piwo::uid uid);

void
config_module_data_changed(const piwo::uid uid);

void
config_resolution_changed();

void
application_state_changed(const application_state_t state);

/* Player main display interface */

void
display_animation_changed();

void
playlist_changed();

void
light_show_started();

void
light_show_stopped();

void
animation_started();

void
animation_paused();

void
animation_stopped();

void
provider_list_changed(const providers_map_t& providers);

void
render_engine_state_chnaged(bool);

void
render_engine_configured(const pipeline_config& config);

void
render_engine_step_removed(step_id id);

void
render_engine_step_state_changed(step_id id, bool state);

void
snake_disconnected();

void
snake_connection_established();

void
snake_refresh_clients_count(int clients_count);

void
snake_move_state_changed(bool state);
/* Ethernet configuration */
void
eth_state_changed(bool new_state);
