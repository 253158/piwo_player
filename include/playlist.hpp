#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include <mipc/timer.h>
#include <piwo/animation.h>
#include <spdlog/spdlog.h>

#include "common_types.hpp"
#include "frame_provider.hpp"
#include "ui_iface.hpp"

struct config_t;

namespace
{
constexpr int64_t PLAYLIST_INVALID_ANIMATION_INDEX = -1;
}

struct named_animation
{
  std::string name;
  piwo::animation anim;
};

struct animation_info
{
  using dur_t = decltype(std::declval<piwo::animation&>().total_time());

  std::string name;
  dur_t duration;
};

class playlist_t : public frame_provider
{

public:
  playlist_t(resolution_t<size_t>, id name);
  ~playlist_t();

  virtual id
  get_name() override;

  void
  framerate_thread_handler();

  void
  reset_current_animation();

  bool
  next();

  bool
  prev();

  bool
  set_animation(size_t index);

  bool
  set_frame_no(size_t frame_no);

  std::optional<size_t>
  get_current_animation_index();

  std::optional<piwo::animation>
  get_current_animation();

  size_t
  get_current_animation_frame_no();

  bool
  add_animation(named_animation anim);

  bool
  remove_animation(int idx);

  // TODO make posible to reorder animations inside the
  // container of animations
  void
  change_animation_order(int idx1, int idx2);

  auto
  acquire_animations()
  {
    return std::make_tuple(std::unique_lock(this->_animations_mtx),
                           std::cref(this->_animations));
  }

  bool
  is_running()
  {
    return this->_frame_rate_timer.is_active();
  }

  void
  wait() override;

  bool
  start();

  void
  pause();

  void
  stop();

private:
  piwo::frames_intersection::frame_t
  get_frame_() override;

  virtual void
  resize_(size_t width, size_t height) override;

  void
  buffer_resolution_check(const named_animation& animation);

  void
  internal_reset_non_blocking();

  void
  internal_start_thread_if_not_running();

  void
  internal_stop_thread_and_wait();

private:
  std::vector<named_animation> _animations;

  std::thread _frame_rate_thread;
  mipc::timer _frame_rate_timer;

  int64_t _current_animation = PLAYLIST_INVALID_ANIMATION_INDEX;
  size_t _current_frame = 0;

  std::recursive_mutex _animations_mtx;

  std::mutex _framesync_mtx;
  std::condition_variable _cv;
  bool _rendered = false;
  bool _is_finished = false;

  piwo::frames_intersection::frame_t _frame_buffer_ptr;

  id _name;
};
