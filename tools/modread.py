import serial
import sys
import json

if len(sys.argv) != 2:
    print(f'Usage: {sys.argv[0]} <path to serial device>')
    exit(1)

_UID_HEADER = 'UID [HEX]:'
_MODULES = {}

ser = serial.Serial(sys.argv[1], 921600)

try:

    while 1:
        line = ser.readline().decode('utf-8', errors='ignore')

        if _UID_HEADER not in line:
            continue

        uid = line[line.find(_UID_HEADER)+_UID_HEADER.__len__():].strip()
        print(f'Found uid: {uid}')

        while 1:
            try:
                id = int(input('Give id: '))
            except ValueError:
                print('Error: id must be integer')
                continue

            _MODULES[uid] = id

            print(_MODULES)
            break

except KeyboardInterrupt:
    out = []
    for uid in _MODULES:
        out.append({"id":_MODULES[uid], "uid":uid})

    print('')
    print(json.dumps(out, indent=4))
    sys.exit(0)

