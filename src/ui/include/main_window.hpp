#pragma once

#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "player.hpp"
#include "snake/snake_model.hpp"
#include <modules_configuration.hpp>
#include <settings.hpp>

#include "main_window/footer_model.hpp"

namespace gui
{
class main_window : public QObject
{
public:
  main_window(QQmlApplicationEngine& e, QObject* p = nullptr);

private:
  footer_model _footer_model;
  modules_configuration _configuration_widget;
  settings _settings_widget;
  player _player_widget;
  snake_model _snake_model;
};
} // namespace gui
