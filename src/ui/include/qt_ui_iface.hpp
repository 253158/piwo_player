#pragma once

#include "state.hpp"
#include <piwo/protodef.h>

#include <QObject>
#include <qqmlapplicationengine.h>

#include "common_types.hpp"
#include "pipeline_config.hpp"

inline QQmlApplicationEngine* global_qml_engine;

class qt_ui_iface : public QObject
{
  Q_OBJECT

public:
  void
  config_module_added(const piwo::uid uid)
  {
    emit signal_config_module_added(uid);
  }

  void
  config_module_removed(const piwo::uid uid)
  {
    emit signal_config_module_removed(uid);
  }

  void
  config_module_data_changed(const piwo::uid uid)
  {
    emit signal_config_module_data_changed(uid);
  }

  void
  config_resolution_changed()
  {
    emit signal_config_resolution_changed();
  }

  void
  application_state_changed(const application_state_t state)
  {
    emit signal_application_state_changed(state);
  }

  void
  display_animation_changed()
  {
    emit signal_animation_changed();
  }

  void
  playlist_changed()
  {
    emit signal_playlist_changed();
  }

  void
  light_show_started()
  {
    emit signal_light_show_started();
  }

  void
  light_show_stopped()
  {
    emit signal_light_show_stopped();
  }

  void
  animation_started()
  {
    emit signal_animation_started();
  }

  void
  animation_paused()
  {
    emit signal_animation_paused();
  }

  void
  animation_stopped()
  {
    emit signal_animation_stopped();
  }

  void
  render_engine_configured(const pipeline_config& config)
  {
    emit signal_render_engine_configured(config);
  }

  void
  render_engine_step_removed(step_id id)
  {
    emit signal_render_engine_step_removed(id);
  }

  void
  render_engine_step_state_changed([[maybe_unused]] step_id id, bool state)
  {
    emit signal_render_engine_step_state_changed(id, state);
  }

  void
  render_engine_state_chnaged(bool state)
  {
    emit signal_render_engine_state_changed(state);
  }

  void
  provider_list_changed(const providers_map_t& providers)
  {
    emit signal_provider_list_changed(providers);
  }

  void
  snake_disconnected()
  {
    emit signal_snake_disconnected();
  }

  void
  snake_connection_established()
  {
    emit signal_snake_connection_established();
  }

  void
  snake_refresh_clients_count(int clients_count)
  {
    emit signal_snake_refresh_clients_count(clients_count);
  }

  void
  snake_move_state_changed(bool state)
  {
    emit signal_snake_move_state_changed(state);
  }

  void
  eth_state_changed(bool new_state)
  {
    emit signal_eth_state_changed(new_state);
  }

signals:
  void
  signal_config_module_added(const piwo::uid uid);

  void
  signal_config_module_removed(const piwo::uid uid);

  void
  signal_config_module_data_changed(const piwo::uid uid);

  void
  signal_config_resolution_changed();

  void
  signal_application_state_changed(const application_state_t state);

  void
  signal_animation_changed();

  void
  signal_playlist_changed();

  void
  signal_light_show_started();

  void
  signal_light_show_stopped();

  void
  signal_animation_started();

  void
  signal_animation_paused();

  void
  signal_animation_stopped();

  void
  signal_render_engine_configured(const pipeline_config& config);

  void
  signal_render_engine_step_removed(step_id id);

  void
  signal_render_engine_step_state_changed(step_id id, bool state);

  void
  signal_render_engine_state_changed(bool);

  void
  signal_provider_list_changed(const providers_map_t&);

  void
  signal_snake_disconnected();

  void
  signal_snake_connection_established();

  void
  signal_snake_refresh_clients_count(int clients_count);

  void
  signal_snake_move_state_changed(bool state);

  void
  signal_eth_state_changed(bool new_state);

} inline qt_ui_if; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
