#pragma once

#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QWidget>

#include "launchpad.hpp"
#include "settings/ethernet_settings_model.hpp"
#include "settings/general_settings_model.hpp"
#include "settings/logger_model.hpp"
#include "settings/pipeline_model.hpp"
#include "settings/render_engine_model.hpp"

namespace gui
{
// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class settings : public QObject
{
  Q_OBJECT

public:
  explicit settings(QQmlContext* context, QWidget* parent = nullptr);
  ~settings() override = default;

public slots:
  void
  apply();

  void
  cancel();

  void
  lp_connect();

  void
  lp_disconnect();

private:
  general_settings_model _general_settings_model;
  ethernet_settings_model _ethernet_settings_model;
  logger_model _logger_model;
  render_engine_model _render_engine_model;
  pipeline_model _pipeline_model;
};
} // namespace gui
