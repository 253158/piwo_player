#include "main_window.hpp"

#include <optional>

#include <QApplication>
#include <QString>
#include <QtWidgets>

#include "piwo.hpp"
#include "qml_debug.hpp"
#include "qt_ui_iface.hpp"

void
start_ui(int argc, char** argv)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

  QApplication app(argc, argv);

  app.setOrganizationName("MOS");
  app.setOrganizationDomain("MOS");

  qmlRegisterType<gui::qml_debugger>(
    "com.mycompany.myapplication", 1, 0, "QMLDebugger");
  gui::qml_debugger qdbg;

  QQmlApplicationEngine engine;
  gui::main_window main_widget(engine);

  const QUrl url(QStringLiteral("qrc:/MainWindow.qml"));

  engine.rootContext()->setContextProperty("qdbg", &qdbg);

  // NOLINTNEXTLINE QT ownership joke
  engine.addImageProvider(QLatin1String("piwo7"), new piwo7_image_provider);

  QObject::connect(
    &engine,
    &QQmlApplicationEngine::objectCreated,
    &app,
    [url](QObject* obj, const QUrl& obj_url)
    {
      if (!obj && url == obj_url)
        QCoreApplication::exit(-1);
    },
    Qt::QueuedConnection);
  engine.load(url);

  global_qml_engine = &engine;
  app.exec();
}

void
config_module_added(const piwo::uid uid)
{
  qt_ui_if.config_module_added(uid);
}

void
config_module_removed(const piwo::uid uid)
{
  qt_ui_if.config_module_removed(uid);
}

void
config_module_data_changed(const piwo::uid uid)
{
  qt_ui_if.config_module_data_changed(uid);
}

void
config_resolution_changed()
{
  qt_ui_if.config_resolution_changed();
}

void
application_state_changed(const application_state_t state)
{
  qt_ui_if.application_state_changed(state);
}

void
display_animation_changed()
{
  qt_ui_if.display_animation_changed();
}

void
playlist_changed()
{
  qt_ui_if.playlist_changed();
}

void
light_show_started()
{
  qt_ui_if.light_show_started();
}

void
light_show_stopped()
{
  qt_ui_if.light_show_stopped();
}

void
animation_started()
{
  qt_ui_if.animation_started();
}

void
animation_paused()
{
  qt_ui_if.animation_paused();
}

void
animation_stopped()
{
  qt_ui_if.animation_stopped();
}

void
render_engine_configured(const pipeline_config& config)
{
  qt_ui_if.render_engine_configured(config);
}

void
render_engine_step_removed(step_id id)
{
  qt_ui_if.render_engine_step_removed(id);
}

void
render_engine_step_state_changed(step_id id, bool state)
{
  qt_ui_if.render_engine_step_state_changed(id, state);
}

void
render_engine_state_chnaged(bool state)
{
  qt_ui_if.render_engine_state_chnaged(state);
}

void
provider_list_changed(const providers_map_t& providers)
{
  qt_ui_if.provider_list_changed(providers);
}

void
snake_disconnected()
{
  qt_ui_if.snake_disconnected();
}

void
snake_connection_established()
{
  qt_ui_if.snake_connection_established();
}

void
snake_refresh_clients_count(int clients_count)
{
  qt_ui_if.snake_refresh_clients_count(clients_count);
}

void
snake_move_state_changed(bool state)
{
  qt_ui_if.snake_move_state_changed(state);
}

void
eth_state_changed(bool new_state)
{
  qt_ui_if.eth_state_changed(new_state);
}
