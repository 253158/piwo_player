#include "qml_debug.hpp"

namespace gui
{

QString
qml_debugger::print_object(QQuickItem* item)
{
  const QMetaObject* meta = item->metaObject();

  QString out = item->objectName();
  out.append(" {\n");

  for (int i = 0; i < meta->propertyCount(); i++)
  {
    QMetaProperty property = meta->property(i);
    const char* name = property.name();
    QVariant value = item->property(name);

    out.append("\t");
    out.append(name);
    out.append(": ");
    out.append(value.toString());
    out.append("\n");
  }

  out.append("\n");
  for (int i = 0; i < meta->methodCount(); i++)
  {
    QMetaMethod method = meta->method(i);
    const char* name = method.name();
    QByteArray signature = method.methodSignature();

    out.append("\t");
    out.append(name);
    out.append(": ");
    out.append(signature);
    out.append("\n");
  }

  out.append("}\n");

  return out;
}

} // namespace gui
