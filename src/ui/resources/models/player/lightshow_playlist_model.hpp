#pragma once

#include <QAbstractItemModel>
#include <QAbstractListModel>

#include "playlist.hpp"

namespace
{
constexpr int INVALID_ACTIVE_ANIMATION_INDEX = -1;
}

namespace gui
{
class lightshow_playlist_model : public QAbstractListModel
{
  Q_OBJECT

public:
  enum playlist_role
  {
    NAME_ROLE = Qt::UserRole,
    DURATION_ROLE,
    CURRENTLY_PLAYED,
  };

public:
  explicit lightshow_playlist_model(QObject* parent = nullptr);

  [[nodiscard]] int
  rowCount(const QModelIndex& parent = QModelIndex()) const override;

  [[nodiscard]] QVariant
  data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

  bool
  setData(const QModelIndex& index,
          const QVariant& value,
          int role = Qt::EditRole) override;

  [[nodiscard]] Qt::ItemFlags
  flags(const QModelIndex& index) const override;

  [[nodiscard]] QHash<int, QByteArray>
  roleNames() const override;

  Q_INVOKABLE void
  change_animation_to(int index);

public slots:
  void
  on_playlist_changed();

  void
  on_animation_changed();

private:
  int _row_count = 0;
  int _active_animation = INVALID_ACTIVE_ANIMATION_INDEX;
};

} // namespace gui
