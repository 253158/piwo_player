#include "lightshow_playlist_model.hpp"

#include "config.hpp"
#include "qt_ui_iface.hpp"

#include <piwo/protodef.h>

namespace gui
{

lightshow_playlist_model::lightshow_playlist_model(QObject* parent)
  : QAbstractListModel(parent)
{
  spdlog::trace(__PRETTY_FUNCTION__);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_playlist_changed,
                   this,
                   &lightshow_playlist_model::on_playlist_changed);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_animation_changed,
                   this,
                   &lightshow_playlist_model::on_animation_changed);
}

int
lightshow_playlist_model::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid())
    return 0;

  return this->_row_count;
}

QVariant
lightshow_playlist_model::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
    return QVariant();

  spdlog::trace(
    "{} ({} {}) {}", __PRETTY_FUNCTION__, index.column(), index.row(), role);

  auto [playlist_lock, playlist] = global_config.acquire_playlist();
  auto [animations_lock, animations] = playlist.acquire_animations();

  std::string animation_path = animations[index.row()].name;

  auto index_of_filename = animation_path.find_last_of('/');
  if (index_of_filename == std::string::npos)
  {
    index_of_filename = animation_path.find_last_of('\\');
    // No need to check if it is npos
    // It is incremented in next step so is going to be 0 anyway
  }
  auto index_of_ext = animation_path.find_last_of('.');

  std::string animation_name;

  if (index_of_ext != std::string::npos)
  {
    // Increment index of filename to get rid of '/' or '\'
    // Decrement index of ext to get rid of '.'
    animation_name.append(animation_path.substr(
      index_of_filename + 1, index_of_ext - index_of_filename - 1));
  }
  else
  {
    // Increment index of filename to get rid of '/' or '\'
    animation_name.append(
      animation_path.substr(index_of_filename + 1, index_of_ext));
  }

  switch (role)
  {
    case NAME_ROLE:
      return QString::fromStdString(animation_name);
    case DURATION_ROLE:
      return static_cast<qulonglong>(animations[index.row()].anim.total_time());
    case CURRENTLY_PLAYED:
      return index.row() == this->_active_animation;
  }

  return QVariant();
}

bool
lightshow_playlist_model::setData([[maybe_unused]] const QModelIndex& index,
                                  [[maybe_unused]] const QVariant& value,
                                  [[maybe_unused]] int role)
{
  // Not needed now. Implement when needed
  return false;
}

Qt::ItemFlags
lightshow_playlist_model::flags([[maybe_unused]] const QModelIndex& index) const
{
  return Qt::NoItemFlags;
}

QHash<int, QByteArray>
lightshow_playlist_model::roleNames() const
{
  QHash<int, QByteArray> names;
  names[NAME_ROLE] = "name";
  names[DURATION_ROLE] = "duration";
  names[CURRENTLY_PLAYED] = "is_currently_played";
  return names;
}

void
lightshow_playlist_model::on_playlist_changed()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  this->beginResetModel();

  {
    auto [playlist_lock, playlist] = global_config.acquire_playlist();
    auto [animations_lock, animations] = playlist.acquire_animations();
    std::optional narrowed_playlist_size = narrow<int>(animations.size());
    if (!narrowed_playlist_size.has_value()) [[unlikely]]
    {
      spdlog::critical("Unsupported playlist size = {}", animations.size());
      return;
    }
    this->_row_count = narrowed_playlist_size.value();
    this->_active_animation = INVALID_ACTIVE_ANIMATION_INDEX;
  }

  this->endResetModel();
}

void
lightshow_playlist_model::on_animation_changed()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  auto previously_active_animation_index = this->_active_animation;

  auto [lock, playlist] = global_config.acquire_playlist();

  std::optional current_animation_index_opt =
    playlist.get_current_animation_index();
  if (current_animation_index_opt.has_value())
  {
    auto current_animation_index = current_animation_index_opt.value();
    std::optional narrowed_active_anim_index =
      narrow<int>(current_animation_index);
    if (!narrowed_active_anim_index.has_value()) [[unlikely]]
    {
      spdlog::critical("Unsupported animation index = {}",
                       current_animation_index);
      return;
    }
    this->_active_animation = narrowed_active_anim_index.value();
  }
  else
  {
    this->_active_animation = INVALID_ACTIVE_ANIMATION_INDEX;
  }

  if (previously_active_animation_index >= 0)
  {
    emit this->dataChanged(this->index(previously_active_animation_index, 0),
                           this->index(previously_active_animation_index, 0),
                           { CURRENTLY_PLAYED });
  }

  emit this->dataChanged(this->index(this->_active_animation, 0),
                         this->index(this->_active_animation, 0),
                         { CURRENTLY_PLAYED });
}

void
lightshow_playlist_model::change_animation_to(int index)
{
  std::optional uindex = narrow<size_t>(index);

  if (!uindex.has_value())
  {
    spdlog::warn("Got invalid size_t index == {}. Ignoring.", index);
    return;
  }

  global_config.change_animation_to(uindex.value());
}

} // namespace gui
