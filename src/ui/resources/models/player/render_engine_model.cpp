#include "render_engine_model.hpp"
#include "qt_ui_iface.hpp"
#include <spdlog/spdlog.h>

namespace gui
{

render_engine_scene_model::render_engine_scene_model(QObject* parent)
  : QObject(parent)
{
  constexpr int timeout_ms = 50;

  this->_framerate_timer_up = std::make_unique<QTimer>(this);
  this->_framerate_timer_up->setInterval(timeout_ms);
  this->_image_source = "image://piwo7//pipeline";

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_light_show_started,
                   this,
                   &render_engine_scene_model::on_light_show_started);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_light_show_stopped,
                   this,
                   &render_engine_scene_model::on_light_show_stopped);

  QObject::connect(_framerate_timer_up.get(),
                   &QTimer::timeout,
                   this,
                   &render_engine_scene_model::on_display_refresh);

  emit this->render_engine_state_changed(this->_light_show_running);
}

QString
render_engine_scene_model::get_image_source()
{
  return this->_image_source;
}

bool
render_engine_scene_model::get_render_engine_state()
{
  return this->_light_show_running;
}

int
render_engine_scene_model::get_dummy_frame()
{
  return this->_dummy_frame;
}

void
render_engine_scene_model::on_light_show_stopped()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  this->_light_show_running = false;
  emit this->render_engine_state_changed(this->_light_show_running);

  this->_framerate_timer_up->stop();
}

void
render_engine_scene_model::on_light_show_started()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  this->_light_show_running = true;
  this->_dummy_frame = 0;
  emit this->render_engine_state_changed(this->_light_show_running);

  this->_framerate_timer_up->start();
}

void
render_engine_scene_model::on_display_refresh()
{
  // there we are doing a trict to force frame reload
  // in qml. This is neccessary cause QImageProvider reacts
  // only on path change, so to refresh the image
  // we are sending the path /pipeline/0 and /pipeline/1
  // interchangeably
  emit this->dummy_frame_changed(this->_dummy_frame);
  this->_dummy_frame ^= 1;
}
} // namespace gui
