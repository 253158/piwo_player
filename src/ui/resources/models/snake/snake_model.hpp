#pragma once

#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QString>
#include <QWidget>

#include <atomic>

#include "config.hpp"

namespace gui
{
class snake_model : public QObject
{
  Q_OBJECT
  Q_PROPERTY(bool connected READ get_connection_state WRITE set_connection_state
               NOTIFY connection_state_changed);
  Q_PROPERTY(int clients_count READ get_clients_count WRITE set_clients_count
               NOTIFY clients_count_changed);
  Q_PROPERTY(bool started READ get_move_state WRITE set_move_state NOTIFY
               move_state_changed);

public:
  snake_model();

  void
  set_connection_state(bool state);

  void
  set_clients_count(int clients_count);

  void
  set_move_state(bool snake_started);

public slots:
  bool
  get_connection_state() const;

  void
  connect(const QString& ip);

  void
  disconnect();

  int
  get_clients_count() const;

  bool
  get_move_state() const;

  void
  set_snake_move_time(int move_time);

  int
  get_initial_move_time() const
  {
    return INITIAL_SNAKE_MOVE_TIME_MS;
  }

  void
  stop();
  void
  start();

signals:
  void
  connection_state_changed();

  void
  clients_count_changed();

  void
  move_state_changed();

private:
  std::atomic_bool _is_connected{ false };
  std::atomic_bool _snake_started{ false };
  std::atomic_int _clients_count{ 0 };
};
} // namespace gui
