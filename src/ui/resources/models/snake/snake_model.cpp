#include "snake_model.hpp"

#include "qt_ui_iface.hpp"

namespace gui
{
snake_model::snake_model()
{
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_snake_disconnected,
                   this,
                   [&]() { this->set_connection_state(false); });
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_snake_connection_established,
                   this,
                   [&]() { this->set_connection_state(true); });
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_snake_refresh_clients_count,
                   this,
                   &snake_model::set_clients_count);
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_snake_move_state_changed,
                   this,
                   &snake_model::set_move_state);
}

bool
snake_model::get_connection_state() const
{
  return this->_is_connected;
}

bool
snake_model::get_move_state() const
{
  return this->_snake_started;
}

void
snake_model::set_snake_move_time(int move_time)
{
  global_config.set_snake_move_time(move_time);
}

void
snake_model::set_connection_state(bool state)
{
  this->_is_connected = state;
  emit this->connection_state_changed();
}

void
snake_model::set_move_state(bool snake_started)
{
  this->_snake_started = snake_started;
  emit this->move_state_changed();
}

void
snake_model::connect(const QString& ip)
{
  auto ip_std_string = ip.toStdString();
  auto [mtx, ev] = global_config.acquire_event_loop();
  event_loop::snake_conn_data data{ ip_std_string };
  ev.send_event({ event_loop::event_t::snake_connect, data });
}

void
snake_model::disconnect()
{
  auto [mtx, ev] = global_config.acquire_event_loop();
  event_loop::no_args data;
  ev.send_event({ event_loop::event_t::snake_disconnect, data });
}

void
snake_model::set_clients_count(int clients_count)
{
  this->_clients_count = clients_count;
  emit this->clients_count_changed();
}

int
snake_model::get_clients_count() const
{
  return this->_clients_count;
}

void
snake_model::stop()
{
  global_config.stop_snake_game();
}

void
snake_model::start()
{
  global_config.restart_snake_game();
}
} // namespace gui
