#pragma once

#include <QObject>
#include <QQmlApplicationEngine>
#include <QString>

#include <spdlog/spdlog.h>

#include "shared_enums.hpp"

namespace gui
{
// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class logger_model : public QObject
{
  Q_OBJECT
  Q_PROPERTY(int logging_level READ get_logging_level WRITE set_logging_level
               NOTIFY logging_level_changed)
public:
  logger_model();
  ~logger_model() override = default;

  int
  get_logging_level();
  void
  set_logging_level(int logging_level);

  Q_INVOKABLE void
  log_trace(QString msg) // NOLINT(performance-unnecessary-value-param)
  {
    spdlog::trace("GUI: {:s}", msg.toStdString());
  }

  Q_INVOKABLE void
  log_debug(QString msg) // NOLINT(performance-unnecessary-value-param)
  {
    spdlog::debug("GUI: {:s}", msg.toStdString());
  }

  Q_INVOKABLE void
  log_info(QString msg) // NOLINT(performance-unnecessary-value-param)
  {
    spdlog::info("GUI: {:s}", msg.toStdString());
  }

  Q_INVOKABLE void
  log_warn(QString msg) // NOLINT(performance-unnecessary-value-param)
  {
    spdlog::warn("GUI: {:s}", msg.toStdString());
  }

  Q_INVOKABLE void
  log_error(QString msg) // NOLINT(performance-unnecessary-value-param)
  {
    spdlog::error("GUI: {:s}", msg.toStdString());
  }

  Q_INVOKABLE void
  log_critical(QString msg) // NOLINT(performance-unnecessary-value-param)
  {
    spdlog::critical("GUI: {:s}", msg.toStdString());
  }

signals:
  void
  logging_level_changed(int logging_level);
};
} // namespace gui
