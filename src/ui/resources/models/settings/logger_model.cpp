#include "logger_model.hpp"

#include "spdlog/spdlog.h"

namespace gui
{
logger_model::logger_model()
{
  qmlRegisterUncreatableMetaObject(gui::cpp_enum::staticMetaObject,
                                   "log_level_enum",
                                   1,
                                   0,
                                   "LogLevelEnum",
                                   "Error: only enums");
}

int
logger_model::get_logging_level()
{
  return static_cast<int>(spdlog::get_level());
}

void
logger_model::set_logging_level(int logging_level)
{
  spdlog::set_level(static_cast<spdlog::level::level_enum>(logging_level));
  emit logging_level_changed(logging_level);
}
} // namespace gui