#pragma once

#include "config.hpp"
#include <QObject>

namespace gui
{
class general_settings_model : public QObject
{
  Q_OBJECT
  Q_PROPERTY(int width READ get_resolution_width WRITE set_resolution_width
               NOTIFY resolution_width_changed)
  Q_PROPERTY(int height READ get_resolution_height WRITE set_resolution_height
               NOTIFY resolution_height_changed)

public:
  explicit general_settings_model(QObject* parent = nullptr);

  void
  apply();

  void
  cancel();

  int
  get_resolution_width();

  void
  set_resolution_width(int width);

  int
  get_resolution_height();

  void
  set_resolution_height(int height);

signals:
  void
  resolution_width_changed(int width);

  void
  resolution_height_changed(int height);

public slots:
  void
  on_resolution_changed();

private:
  config_t::uresolution_t _resolution;

  bool _resolution_was_changed = false;
};
} // namespace gui
