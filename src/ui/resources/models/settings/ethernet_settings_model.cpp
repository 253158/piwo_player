#include <spdlog/spdlog.h>

#include "qt_ui_iface.hpp"
#include "settings/ethernet_settings_model.hpp"

namespace gui
{

ethernet_settings_model::ethernet_settings_model(
  [[maybe_unused]] QObject* parent)
  : QObject(parent)
{
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_eth_state_changed,
                   this,
                   &ethernet_settings_model::on_eth_status_changed);
}

bool
ethernet_settings_model::get_eth_connected()
{
  return this->_eth_connection_state;
}

void
ethernet_settings_model::set_eth_connected(bool new_state)
{
  this->_eth_connection_state = new_state;
  emit eth_connection_state_changed();
}

Q_INVOKABLE void
ethernet_settings_model::tx_connect(QString ip,
                                    QString tcp_port,
                                    QString udp_port)
{
  auto ip_std_string = ip.toStdString();
  auto tcp_port_std_string = tcp_port.toStdString();
  auto udp_port_std_string = udp_port.toStdString();

  spdlog::debug("Connecting to tx {}:{}", ip_std_string, tcp_port_std_string);
  auto [mtx, ev] = global_config.acquire_event_loop();
  event_loop::eth_data data{ ip_std_string,
                             tcp_port_std_string,
                             udp_port_std_string };
  ev.send_event({ event_loop::event_t::eth_connect_tx, data });
}

Q_INVOKABLE void
ethernet_settings_model::tx_disconnect()
{
  spdlog::debug("Disconnecting from tx");
  auto [mtx, ev] = global_config.acquire_event_loop();
  event_loop::no_args data;
  ev.send_event({ event_loop::event_t::eth_disconnect_tx, data });
}

void
ethernet_settings_model::on_eth_status_changed(bool new_state)
{
  if (this->_eth_connection_state == new_state)
    return;

  this->_eth_connection_state = new_state;
  emit eth_connection_state_changed();
}

} // namespace gui
