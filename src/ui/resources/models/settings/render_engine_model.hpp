#pragma once

#include "config.hpp"
#include <QObject>
#include <QString>

namespace gui
{
class render_engine_model : public QObject
{
  Q_OBJECT
  Q_PROPERTY(
    QStringList provider_list READ get_providers NOTIFY provider_list_changed)
  Q_PROPERTY(
    QStringList effects_list READ get_effects NOTIFY effects_list_changed)

  Q_PROPERTY(bool re_enabled READ get_re_state NOTIFY re_state_chnaged)

  Q_PROPERTY(
    QString source_provider READ get_source_provider WRITE set_source_provider)
  Q_PROPERTY(QString destination_provider READ get_destination_provider WRITE
               set_destination_provider)
  Q_PROPERTY(QString effect READ get_effect WRITE set_effect)
  Q_PROPERTY(int off_x READ get_off_x WRITE set_off_x NOTIFY off_x_changed)
  Q_PROPERTY(int off_y READ get_off_y WRITE set_off_y NOTIFY off_y_changed)
public:
  explicit render_engine_model(QObject* parent = nullptr);

  ~render_engine_model() = default;

  void
  set_source_provider(const QString& p)
  {
    this->_config.source_provider_name = p.toStdString();
  }

  QString
  get_source_provider()
  {
    return this->_config.source_provider_name.c_str();
  }

  void
  set_destination_provider(const QString& p)
  {
    this->_config.destination_provider_name = p.toStdString();
  }

  QString
  get_destination_provider()
  {
    return this->_config.destination_provider_name.c_str();
  }

  QString
  get_effect()
  {
    return this->_config.effect.c_str();
  }

  void
  set_effect(const QString& e)
  {
    this->_config.effect = e.toStdString();
  }

  void
  set_off_x(int x)
  {
    this->_config.off_x = x;
    emit this->off_x_changed(this->_config.off_x);
  }

  int
  get_off_x()
  {
    return this->_config.off_x;
  }

  void
  set_off_y(int y)
  {
    this->_config.off_y = y;
    emit this->off_y_changed(this->_config.off_y);
  }

  int
  get_off_y()
  {
    return this->_config.off_y;
  }

  bool
  get_re_state()
  {
    return _re_enabled;
  }

  QStringList
  get_providers()
  {
    return this->_provider_list;
  }

  QStringList
  get_effects()
  {
    return this->_effects_list;
  }

signals:
  void
  provider_list_changed(const QStringList& providers);
  void
  effects_list_changed(const QStringList& effects);
  void
  re_state_chnaged();
  void
  off_x_changed(int);
  void
  off_y_changed(int);

public slots:
  void
  toggle_re()
  {
    if (!_re_enabled)
    {
      global_config.enable_render_engine_configuration();
    }
    else
    {
      global_config.disable_render_engine_configuration();
    }
  }

  void
  apply()
  {
    global_config.add_step(this->_config);
  }

  void
  on_render_engine_state_changed(bool state)
  {
    this->_re_enabled = state;
    emit this->re_state_chnaged();
  }

  void
  on_provider_list_changed(const providers_map_t& providers)
  {
    this->_provider_list.clear();
    for (const auto& [name, provider] : providers)
    {
      this->_provider_list.push_back(name.c_str());
    }

    if (providers.size())
    {
      // just to make current pipeline config up to date with the ui
      const std::string& basic_provider_name = providers.begin()->first;
      this->_config.source_provider_name = basic_provider_name;
      this->_config.destination_provider_name = basic_provider_name;
    }
    emit this->provider_list_changed(this->_provider_list);
  }

private:
  QStringList _provider_list;
  QStringList _effects_list;
  pipeline_config _config;

  bool _re_enabled = false;
};
} // namespace gui
