#pragma once

#include <QAbstractTableModel>
#include <memory>
#include <mutex>
#include <qnamespace.h>
#include <qqml.h>

#include "config.hpp"
#include "qt_ui_iface.hpp"

namespace gui
{
class pipeline_model : public QAbstractTableModel
{
  Q_OBJECT

public:
  static constexpr int COL_COUNT = 6;

  explicit pipeline_model(QObject* parent = nullptr);

  enum module_role
  {
    DISPLAY_DATA = Qt::DisplayRole,
    STEP_STATE,
    IS_HIGHLIGHTED
  };

  [[nodiscard]] int
  rowCount(const QModelIndex& parent = QModelIndex()) const override;

  [[nodiscard]] int
  columnCount(const QModelIndex& parent = QModelIndex()) const override;

  [[nodiscard]] QVariant
  data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

  Q_INVOKABLE QVariant
  headerData(int section,
             Qt::Orientation orientation,
             int role = Qt::DisplayRole) const override;

  bool
  setData(const QModelIndex& index,
          const QVariant& value,
          int role = Qt::EditRole) override;

  [[nodiscard]] Qt::ItemFlags
  flags(const QModelIndex& index) const override;

  [[nodiscard]] QHash<int, QByteArray>
  roleNames() const override;

  Q_INVOKABLE void
  highlight(int row);

  Q_INVOKABLE void
  dehighlight(int row);

  Q_INVOKABLE void
  remove_highlighted();

  Q_INVOKABLE void
  refresh_highlighted();

  Q_INVOKABLE void
  change_state_of_highlighted(bool);

public slots:
  void
  on_pipeline_added(const pipeline_config& config);

  void
  on_pipeline_removed(int id);

  void
  on_pipeline_step_state_changed(int, bool);

private:
  std::vector<pipeline_config> _list;
  mutable std::recursive_mutex _mtx;
};
} // namespace gui
