#pragma once

#include "config.hpp"
#include <QObject>

namespace gui
{

class ethernet_settings_model : public QObject
{
  Q_OBJECT
  Q_PROPERTY(bool eth_connected READ get_eth_connected WRITE set_eth_connected
               NOTIFY eth_connection_state_changed)

public:
  explicit ethernet_settings_model(QObject* parent = nullptr);

  Q_INVOKABLE void
  tx_connect(QString ip, QString tcp_port, QString udp_port);

  Q_INVOKABLE void
  tx_disconnect();

  bool
  get_eth_connected();

  void
  set_eth_connected(bool new_state);

signals:
  void
  eth_connection_state_changed();

public slots:
  void
  on_eth_status_changed(bool new_state);

private:
  bool _eth_connection_state = false;
};

} // namespace gui
