#include "render_engine_model.hpp"

#include <algorithm>

#include <spdlog/spdlog.h>

#include "qt_ui_iface.hpp"

namespace gui
{
render_engine_model::render_engine_model(QObject* parent)
  : QObject(parent)
{
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_provider_list_changed,
                   this,
                   &render_engine_model::on_provider_list_changed);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_render_engine_state_changed,
                   this,
                   &render_engine_model::on_render_engine_state_changed);

  for (auto& [n, e] : effect_lookup)
  {
    this->_effects_list.push_back(n.c_str());
  }
  this->_config.off_x = 0;
  this->_config.off_y = 0;
  if (effect_lookup.size())
  {
    _config.effect = effect_lookup.begin()->first;
  }
  emit this->effects_list_changed(this->_effects_list);
}

} // namespace gui
