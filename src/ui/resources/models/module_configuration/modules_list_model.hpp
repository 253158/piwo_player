#pragma once

#include <QAbstractItemModel>
#include <QAbstractListModel>

#include <memory>

#include "config.hpp"
#include "module_edit_popup_model.hpp"
#include "qt_ui_iface.hpp"

namespace gui
{
class modules_list_model : public QAbstractListModel
{
  Q_OBJECT

public:
  explicit modules_list_model(QObject* parent = nullptr);

  enum module_role
  {
    ID_ROLE = Qt::UserRole,
    UID_ROLE,
    IS_HIGHLIGHTED,
  };

  void
  set_module_edit_popup_model(
    std::shared_ptr<module_edit_popup_model> module_edit_popup);

  [[nodiscard]] int
  rowCount(const QModelIndex& parent = QModelIndex()) const override;

  [[nodiscard]] QVariant
  data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

  bool
  setData(const QModelIndex& index,
          const QVariant& value,
          int role = Qt::EditRole) override;

  [[nodiscard]] Qt::ItemFlags
  flags(const QModelIndex& index) const override;

  [[nodiscard]] QHash<int, QByteArray>
  roleNames() const override;

  Q_INVOKABLE void
  module_dropped(QString uid);

  Q_INVOKABLE void
  auto_configure_modules();

  Q_INVOKABLE void
  highlight(int row);

  Q_INVOKABLE void
  dehighlight(int row);

  Q_INVOKABLE void
  dehighlight_all();

  Q_INVOKABLE void
  edit_highlighted_modules();

public slots:
  void
  on_module_added(const piwo::uid& uid);
  void
  on_module_removed(const piwo::uid& uid);
  void
  on_module_data_changed(const piwo::uid& uid);

private:
  std::shared_ptr<module_edit_popup_model> _module_edit_popup;
  std::vector<piwo::uid> _list;
  std::vector<int> _highlighted_modules;
};
} // namespace gui
