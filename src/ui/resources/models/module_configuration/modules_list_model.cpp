#include "modules_list_model.hpp"

#include <spdlog/spdlog.h>

#include "modinfo.hpp"

namespace gui
{
modules_list_model::modules_list_model(QObject* parent)
  : QAbstractListModel(parent)
{
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_config_module_added,
                   this,
                   &modules_list_model::on_module_added);
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_config_module_removed,
                   this,
                   &modules_list_model::on_module_removed);
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_config_module_data_changed,
                   this,
                   &modules_list_model::on_module_data_changed);
  beginResetModel();
  endResetModel();
}

void
modules_list_model::set_module_edit_popup_model(
  std::shared_ptr<module_edit_popup_model> module_edit_popup)
{
  this->_module_edit_popup = std::move(module_edit_popup);
}

int
modules_list_model::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid())
    return 0;

  return static_cast<int>(this->_list.size());
}

QVariant
modules_list_model::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
    return QVariant();

  if (role == IS_HIGHLIGHTED)
  {
    return std::find(this->_highlighted_modules.begin(),
                     this->_highlighted_modules.end(),
                     index.row()) != _highlighted_modules.end();
  }

  auto uid = this->_list.at(index.row());
  modinfo key{};
  key.base.uid = uid;
  const auto& [mtx, modules] = global_config.acquire_modules();
  auto module = modules.find(key);

  if (module == modules.end())
  {
    return QVariant();
  }

  switch (role)
  {
    case ID_ROLE:
      return QVariant(qlonglong(module->base.id));
    case UID_ROLE:
      return QVariant(QString::fromStdString(module->base.uid.to_ascii()));
  }
  return QVariant();
}

bool
modules_list_model::setData([[maybe_unused]] const QModelIndex& index,
                            [[maybe_unused]] const QVariant& value,
                            [[maybe_unused]] int role)
{
  // Not needed now. Implement when needed
  return false;
}

Qt::ItemFlags
modules_list_model::flags(const QModelIndex& index) const
{
  if (!index.isValid())
    return Qt::NoItemFlags;

  return Qt::ItemIsDragEnabled;
}

QHash<int, QByteArray>
modules_list_model::roleNames() const
{
  QHash<int, QByteArray> names;
  names[ID_ROLE] = "id";
  names[UID_ROLE] = "uid";
  names[IS_HIGHLIGHTED] = "is_highlighted";
  return names;
}

void
modules_list_model::on_module_added(const piwo::uid& uid)
{
  spdlog::trace(__PRETTY_FUNCTION__);

  modinfo key{};
  key.base.uid = uid;
  const auto& [mtx, modules] = global_config.acquire_modules();
  auto module = modules.find(key);
  if (module == modules.end())
  {
    return;
  }

  std::optional narrowed_list_size = narrow<int>(_list.size());
  if (!narrowed_list_size.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported modules count = {}", _list.size());
    return;
  }

  if (module->position.x <= 0 && module->position.y <= 0)
  {
    beginInsertRows(
      QModelIndex(), narrowed_list_size.value(), narrowed_list_size.value());
    _list.emplace_back(uid);
    endInsertRows();
    spdlog::trace("Module uid[{}] added to row [{}]",
                  uid.to_ascii(),
                  narrowed_list_size.value());
  }
}

void
modules_list_model::on_module_removed(const piwo::uid& uid)
{
  spdlog::trace(__PRETTY_FUNCTION__);

  std::optional narrowed_list_size = narrow<int>(_list.size());
  if (!narrowed_list_size.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported modules count = {}", _list.size());
    return;
  }

  for (int index = 0; index < narrowed_list_size.value(); ++index)
  {
    if (this->_list[index].packed() == uid.packed())
    {
      beginRemoveRows(QModelIndex(), index, index);
      this->_list.erase(this->_list.begin() + index);
      endRemoveRows();
      spdlog::trace(
        "Module uid[{}] removed from row [{}]", uid.to_ascii(), index);
      return;
    }
  }
}

void
modules_list_model::on_module_data_changed(const piwo::uid& uid)
{
  spdlog::trace(__PRETTY_FUNCTION__);

  std::optional narrowed_list_size = narrow<int>(_list.size());
  if (!narrowed_list_size.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported modules count = {}", _list.size());
    return;
  }

  modinfo key{};
  key.base.uid = uid;
  const auto& [mtx, modules] = global_config.acquire_modules();
  auto module = modules.find(key);
  if (module == modules.end())
  {
    return;
  }

  for (int index = 0; index < narrowed_list_size.value(); ++index)
  {
    if (this->_list[index].packed() == uid.packed())
    {
      if (module->position.x >= 0 && module->position.y >= 0)
      {
        beginRemoveRows(QModelIndex(), index, index);
        this->_list.erase(this->_list.begin() + index);
        endRemoveRows();
        spdlog::trace("Module uid[{}] removed from list", uid.to_ascii());
        return;
      }
      emit this->dataChanged(
        this->index(index, 0), this->index(index, 0), { ID_ROLE, UID_ROLE });
      spdlog::trace(
        "Module uid[{}] updated in row [{}]", uid.to_ascii(), index);
      return;
    }
  }
  if (module->position.x < 0 && module->position.y < 0)
  {
    this->on_module_added(uid);
  }
}

Q_INVOKABLE void
modules_list_model::module_dropped(
  QString uid) // NOLINT(performance-unnecessary-value-param)
{
  spdlog::info("Module uid[{:s}] dropped at raw modules list",
               uid.toStdString());
  std::optional uid_opt = piwo::uid::from_ascii(uid.toStdString());
  if (!uid_opt.has_value())
  {
    spdlog::warn("Failed to convert UID");
    return;
  }
  ipos_t position{ -1, -1 };
  global_config.assign_module_position(uid_opt.value(), position);
}

Q_INVOKABLE void
modules_list_model::auto_configure_modules()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  if (this->_highlighted_modules.empty())
  {
    return;
  }

  std::vector<piwo::uid> modules_to_be_found;

  for (auto row : this->_highlighted_modules)
  {
    if (static_cast<size_t>(row) <= this->_list.size())
    {
      modules_to_be_found.emplace_back(this->_list[row]);
    }
    else
    {
      spdlog::error("Model corrupted");
      return;
    }
  }

  global_config.auto_configure_modules(modules_to_be_found);
}

Q_INVOKABLE void
modules_list_model::highlight(int row)
{
  spdlog::trace("{} {}", __PRETTY_FUNCTION__, row);

  this->_highlighted_modules.emplace_back(row);
  emit this->dataChanged(
    this->index(row, 0), this->index(row, 0), { IS_HIGHLIGHTED });
}

Q_INVOKABLE void
modules_list_model::dehighlight(int row)
{
  spdlog::trace("{} {}", __PRETTY_FUNCTION__, row);

  if (auto row_it = std::find(this->_highlighted_modules.begin(),
                              this->_highlighted_modules.end(),
                              row);
      row_it != _highlighted_modules.end())
  {
    this->_highlighted_modules.erase(row_it);
    emit this->dataChanged(
      this->index(row, 0), this->index(row, 0), { IS_HIGHLIGHTED });
  }
}

Q_INVOKABLE void
modules_list_model::dehighlight_all()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  auto highlighted_modules_copy = std::move(this->_highlighted_modules);
  this->_highlighted_modules.clear();

  for (auto dehighlighted_module : highlighted_modules_copy)
  {
    emit this->dataChanged(this->index(dehighlighted_module, 0),
                           this->index(dehighlighted_module, 0),
                           { IS_HIGHLIGHTED });
  }
}

Q_INVOKABLE void
modules_list_model::edit_highlighted_modules()
{
  if (this->_highlighted_modules.empty())
  {
    return;
  }

  if (!this->_module_edit_popup)
  {
    spdlog::warn("Modules edit popup model is not available!");
    return;
  }

  std::vector<piwo::uid> modules_to_be_edited;

  for (auto row : this->_highlighted_modules)
  {
    if (static_cast<size_t>(row) <= this->_list.size())
    {
      modules_to_be_edited.emplace_back(this->_list[row]);
    }
    else
    {
      spdlog::error("Model corrupted");
      return;
    }
  }

  this->_module_edit_popup->open(modules_to_be_edited);
}

} // namespace gui
