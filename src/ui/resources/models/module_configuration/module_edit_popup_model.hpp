#pragma once

#include <QColor>
#include <QObject>
#include <atomic>

#include "config.hpp"
#include "modinfo.hpp"

namespace gui
{
class module_edit_popup_model : public QObject
{
  Q_OBJECT
  Q_PROPERTY(bool show_dialog READ get_show_dialog WRITE set_show_dialog NOTIFY
               show_dialog_changed)
  Q_PROPERTY(QString uid READ get_uid NOTIFY uid_changed)
  Q_PROPERTY(int id READ get_id WRITE set_id NOTIFY id_changed)
  Q_PROPERTY(int logic_address READ get_logic_address WRITE set_logic_address
               NOTIFY logic_address_changed)
  Q_PROPERTY(int gateway_id READ get_gateway_id WRITE set_gateway_id NOTIFY
               gateway_id_changed)
  Q_PROPERTY(
    int radio_id READ get_radio_id WRITE set_radio_id NOTIFY radio_id_changed)
  Q_PROPERTY(int pos_x READ get_pos_x WRITE set_pos_x NOTIFY pos_x_changed)
  Q_PROPERTY(int pos_y READ get_pos_y WRITE set_pos_y NOTIFY pos_y_changed)
  Q_PROPERTY(QColor static_color READ get_static_color WRITE set_static_color
               NOTIFY static_color_changed)

public:
  explicit module_edit_popup_model(QObject* parent = nullptr);
  ~module_edit_popup_model() override = default;

  void
  open(const std::vector<piwo::uid>& modules);

  Q_INVOKABLE void
  close();

  Q_INVOKABLE void
  apply();

  QString
  get_uid();

  int
  get_id();

  void
  set_id(int id);

  int
  get_logic_address();

  void
  set_logic_address(int logic_address);

  int
  get_gateway_id();

  void
  set_gateway_id(int gateway_id);

  int
  get_radio_id();

  void
  set_radio_id(int radio_id);

  int
  get_pos_x();

  void
  set_pos_x(int pos_x);

  int
  get_pos_y();

  void
  set_pos_y(int pos_y);

  QColor
  get_static_color();

  void
  set_static_color(QColor static_color);

  bool
  get_show_dialog();

  void
  set_show_dialog(bool show);

signals:
  void
  uid_changed(QString uid);

  void
  id_changed(int id);

  void
  logic_address_changed(int logic_address);

  void
  gateway_id_changed(int gateway_id);

  void
  radio_id_changed(int gateway_id);

  void
  pos_x_changed(int pos_x);

  void
  pos_y_changed(int pos_y);

  void
  static_color_changed(QColor static_color);

  void
  show_dialog_changed(bool show);

private:
  std::atomic_bool _dialog_opened = false;
  std::vector<piwo::uid> _modules_uids;
  modinfo _modinfo;

  bool _id_was_changed = false;
  bool _logic_address_was_changed = false;
  bool _gateway_id_was_changed = false;
  bool _radio_id_was_changed = false;
  bool _pos_x_was_changed = false;
  bool _pos_y_was_changed = false;
  bool _static_color_was_changed = false;
};
} // namespace gui