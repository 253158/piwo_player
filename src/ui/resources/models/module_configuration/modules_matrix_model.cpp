#include "modules_matrix_model.hpp"

#include "qt_ui_iface.hpp"

namespace gui
{
modules_matrix_model::modules_matrix_model()
{
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_config_module_added,
                   this,
                   &modules_matrix_model::on_module_added);
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_config_resolution_changed,
                   this,
                   &modules_matrix_model::on_config_resolution_changed);
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_config_module_data_changed,
                   this,
                   &modules_matrix_model::on_module_data_changed);

  beginResetModel();
  endResetModel();
}

void
modules_matrix_model::set_module_edit_popup_model(
  std::shared_ptr<module_edit_popup_model> module_edit_popup)
{
  this->_module_edit_popup = std::move(module_edit_popup);
}

int
modules_matrix_model::rowCount([[maybe_unused]] const QModelIndex& parent) const
{
  const auto& [mtx, resolution] = global_config.acquire_resolution();
  std::optional narrowed_height = narrow<int>(resolution.height);

  if (!narrowed_height.has_value())
  {
    spdlog::critical("Unsupported resolution height = {}", resolution.height);
    return 0;
  }
  return narrowed_height.value();
}

int
modules_matrix_model::columnCount(
  [[maybe_unused]] const QModelIndex& parent) const
{
  const auto& [mtx, resolution] = global_config.acquire_resolution();
  std::optional narrowed_width = narrow<int>(resolution.width);

  if (!narrowed_width.has_value())
  {
    spdlog::critical("Unsupported resolution width = {}", resolution.width);
    return 0;
  }
  return narrowed_width.value();
}

QModelIndex
modules_matrix_model::index(int row,
                            int column,
                            [[maybe_unused]] const QModelIndex& parent) const
{
  // Swapping attributes order is intentional
  // TODO(all) find out why orientation is flipped
  return this->createIndex(column, row);
}

QModelIndex
modules_matrix_model::parent([[maybe_unused]] const QModelIndex& index) const
{
  return QModelIndex();
}

QVariant
modules_matrix_model::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
    return QVariant();

  if (role == IS_HIGHLIGHTED)
  {
    return std::find_if(_highlighted_modules.begin(),
                        _highlighted_modules.end(),
                        [&index](const ipos_t& pos) {
                          return pos.x == index.row() &&
                                 pos.y == index.column();
                        }) != _highlighted_modules.end();
  }

  const auto& [mtx, modules] = global_config.acquire_modules();

  for (auto module : modules)
  {
    // Rows are vertical
    // Columns are horizontal
    // TODO(all) find out why orientation is flipped
    if (module.position.x == index.row() && module.position.y == index.column())
    {
      switch (role)
      {
        case ID_ROLE:
          return QVariant(qlonglong(module.base.id));
        case UID_ROLE:
          return QVariant(QString::fromStdString(module.base.uid.to_ascii()));
        case LOGIC_ADDRESS_ROLE:
          return QVariant(module.logic_address);
        case GATEWAY_ID_ROLE:
          return QVariant(module.gateway_id);
        case RADIO_ID_ROLE:
          return QVariant(module.radio_id);
        case STATIC_COLOR_ROLE:
          return QVariant(
            static_cast<unsigned int>(module.static_color.packed()));
        case POSITION_X_ROLE:
          return QVariant(module.position.x);
        case POSITION_Y_ROLE:
          return QVariant(module.position.y);
      }
      break;
    }
  }

  return QVariant();
}

bool
modules_matrix_model::setData([[maybe_unused]] const QModelIndex& index,
                              [[maybe_unused]] const QVariant& value,
                              [[maybe_unused]] int role)
{
  return true;
}

Qt::ItemFlags
modules_matrix_model::flags([[maybe_unused]] const QModelIndex& index) const
{
  return Qt::ItemIsDragEnabled;
}

QHash<int, QByteArray>
modules_matrix_model::roleNames() const
{
  QHash<int, QByteArray> names;
  names[ID_ROLE] = "id";
  names[UID_ROLE] = "uid";
  names[LOGIC_ADDRESS_ROLE] = "logic_address";
  names[GATEWAY_ID_ROLE] = "gateway_id";
  names[RADIO_ID_ROLE] = "radio_id";
  names[STATIC_COLOR_ROLE] = "static_color";
  names[POSITION_X_ROLE] = "position_x";
  names[POSITION_Y_ROLE] = "position_y";
  names[IS_HIGHLIGHTED] = "is_highlighted";
  return names;
}

void
modules_matrix_model::on_config_resolution_changed()
{
  beginResetModel();
  endResetModel();
}

void
modules_matrix_model::on_module_added(const piwo::uid& uid)
{
  modinfo key{};
  key.base.uid = uid;
  const auto& [mtx, modules] = global_config.acquire_modules();
  auto module = modules.find(key);
  if (module == modules.end())
  {
    spdlog::warn("Failed to find uid[{}]. Model is now invalid",
                 uid.to_ascii());
    return;
  }

  if (module->position.x <= 0 && module->position.y <= 0)
  {
    spdlog::debug(
      "Module uid[{}] unassigned. Will not be added to modules matrix.",
      uid.to_ascii());
    return;
  }

  emit this->dataChanged(this->index(module->position.x, module->position.y),
                         this->index(module->position.x, module->position.y));
  this->_modules_positions.insert_or_assign(uid.packed(), module->position);
}

void
modules_matrix_model::on_module_data_changed(const piwo::uid& uid)
{
  modinfo key{};
  key.base.uid = uid;
  const auto& [mtx, modules] = global_config.acquire_modules();
  auto module = modules.find(key);
  if (module == modules.end())
  {
    spdlog::warn("Failed to find uid[{}]. Model is now invalid",
                 uid.to_ascii());
    return;
  }

  if (this->_modules_positions.contains(uid.packed()))
  {
    auto old_position = this->_modules_positions[uid.packed()];
    emit this->dataChanged(this->index(old_position.x, old_position.y),
                           this->index(old_position.x, old_position.y));
  }

  if (module->position.x >= 0 && module->position.y >= 0)
  {
    emit this->dataChanged(this->index(module->position.x, module->position.y),
                           this->index(module->position.x, module->position.y));
    this->_modules_positions.insert_or_assign(uid.packed(), module->position);
  }
}

Q_INVOKABLE void
modules_matrix_model::module_dropped(
  QString uid, // NOLINT(performance-unnecessary-value-param)
  int x,
  int y)
{
  spdlog::debug(
    "Module uid[{:s}] dropped at x: {} y: {}", uid.toStdString(), x, y);
  auto uid_opt = piwo::uid::from_ascii(uid.toStdString());
  if (!uid_opt.has_value())
  {
    spdlog::warn("Failed to convert UID");
    return;
  }
  ipos_t position{ x, y };
  global_config.assign_module_position(*uid_opt, position);
}

Q_INVOKABLE void
modules_matrix_model::match_tiles_to_modules()
{
  std::vector<piwo::uid> modules_to_found;

  {
    const auto& [mtx, modules] = global_config.acquire_modules();

    for (auto module : modules)
    {
      if (auto tile_it = std::find_if(this->_highlighted_modules.begin(),
                                      this->_highlighted_modules.end(),
                                      [&module](const ipos_t& pos) {
                                        return pos.x == module.position.x &&
                                               pos.y == module.position.y;
                                      });
          tile_it != _highlighted_modules.end())
      {
        modules_to_found.emplace_back(module.base.uid);
      }
    }
  }

  global_config.auto_configure_modules(modules_to_found);
}

Q_INVOKABLE void
modules_matrix_model::match_modules_to_tiles()
{
  global_config.auto_configure_modules(this->_highlighted_modules);
}

Q_INVOKABLE void
modules_matrix_model::highlight(int x, int y)
{
  spdlog::trace("Highlight tile {},{} requested", x, y);
  this->_highlighted_modules.emplace_back(ipos_t{ x, y });
  emit this->dataChanged(
    this->index(x, y), this->index(x, y), { IS_HIGHLIGHTED });
}

Q_INVOKABLE void
modules_matrix_model::highlight_all()
{
  auto [lck, resolution] = global_config.acquire_resolution();
  spdlog::trace(
    "Highlight tile {},{} requested", resolution.width, resolution.height);

  std::optional narrowed_width = narrow<int>(resolution.width);

  if (!narrowed_width.has_value())
  {
    spdlog::critical("Unsupported resolution width = {}", resolution.width);
    return;
  }

  std::optional narrowed_height = narrow<int>(resolution.height);

  if (!narrowed_height.has_value())
  {
    spdlog::critical("Unsupported resolution height = {}", resolution.height);
    return;
  }

  this->_highlighted_modules.clear();
  for (int x = 0; x < *narrowed_width; x++)
  {
    for (int y = 0; y < *narrowed_height; y++)
    {
      this->_highlighted_modules.emplace_back(ipos_t{ x, y });
    }
  }
  emit this->dataChanged(this->index(0, 0),
                         this->index(*narrowed_width, *narrowed_height),
                         { IS_HIGHLIGHTED });
}

Q_INVOKABLE void
modules_matrix_model::dehighlight(int x, int y)
{
  spdlog::trace("Dehighlight tile {},{} requested", x, y);

  if (auto tile_it = std::find_if(_highlighted_modules.begin(),
                                  _highlighted_modules.end(),
                                  [x, y](const ipos_t& pos)
                                  { return pos.x == x && pos.y == y; });
      tile_it != _highlighted_modules.end())
  {
    this->_highlighted_modules.erase(tile_it);
    emit this->dataChanged(
      this->index(x, y), this->index(x, y), { IS_HIGHLIGHTED });
  }
}

Q_INVOKABLE void
modules_matrix_model::dehighlight_all()
{
  spdlog::trace("Dehighlight all tiles requested");

  auto highlighted_modules_copy = std::move(this->_highlighted_modules);
  this->_highlighted_modules.clear();

  for (auto dehighlighted_module : highlighted_modules_copy)
  {
    emit this->dataChanged(
      this->index(dehighlighted_module.x, dehighlighted_module.y),
      this->index(dehighlighted_module.x, dehighlighted_module.y),
      { IS_HIGHLIGHTED });
  }
}

Q_INVOKABLE void
modules_matrix_model::edit_highlighted_modules()
{
  if (this->_highlighted_modules.empty())
  {
    return;
  }

  if (!this->_module_edit_popup)
  {
    spdlog::warn("Modules edit popup model is not available!");
    return;
  }

  const auto& [mtx, modules] = global_config.acquire_modules();

  std::vector<piwo::uid> modules_to_be_edited;

  for (auto module : modules)
  {
    if (auto tile_it = std::find_if(this->_highlighted_modules.begin(),
                                    this->_highlighted_modules.end(),
                                    [&module](const ipos_t& pos) {
                                      return pos.x == module.position.x &&
                                             pos.y == module.position.y;
                                    });
        tile_it != _highlighted_modules.end())
    {
      modules_to_be_edited.emplace_back(module.base.uid);
    }
  }

  this->_module_edit_popup->open(modules_to_be_edited);
}

Q_INVOKABLE void
modules_matrix_model::blink_highlighted_modules()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  if (this->_highlighted_modules.empty())
  {
    return;
  }

  const auto& [mtx, modules] = global_config.acquire_modules();

  std::vector<piwo::uid> modules_to_be_blinked;

  for (auto module : modules)
  {
    if (auto tile_it = std::find_if(this->_highlighted_modules.begin(),
                                    this->_highlighted_modules.end(),
                                    [&module](const ipos_t& pos) {
                                      return pos.x == module.position.x &&
                                             pos.y == module.position.y;
                                    });
        tile_it != _highlighted_modules.end())
    {
      modules_to_be_blinked.emplace_back(module.base.uid);
    }
  }

  global_config.blink_modules(modules_to_be_blinked);
}

Q_INVOKABLE void
modules_matrix_model::color_highlighted_modules()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  if (this->_highlighted_modules.empty())
  {
    return;
  }

  const auto& [mtx, modules] = global_config.acquire_modules();

  std::vector<piwo::uid> modules_to_be_colored;

  for (auto module : modules)
  {
    if (auto tile_it = std::find_if(this->_highlighted_modules.begin(),
                                    this->_highlighted_modules.end(),
                                    [&module](const ipos_t& pos) {
                                      return pos.x == module.position.x &&
                                             pos.y == module.position.y;
                                    });
        tile_it != _highlighted_modules.end())
    {
      modules_to_be_colored.emplace_back(module.base.uid);
    }
  }

  global_config.color_modules(modules_to_be_colored);
}

Q_INVOKABLE void
modules_matrix_model::assing_la_for_highlighted_modules()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  if (this->_highlighted_modules.empty())
  {
    return;
  }

  const auto& [mtx, modules] = global_config.acquire_modules();

  std::vector<piwo::uid> modules_to_mark_with_la;

  for (auto module : modules)
  {
    if (auto tile_it = std::find_if(this->_highlighted_modules.begin(),
                                    this->_highlighted_modules.end(),
                                    [&module](const ipos_t& pos) {
                                      return pos.x == module.position.x &&
                                             pos.y == module.position.y;
                                    });
        tile_it != _highlighted_modules.end())
    {
      modules_to_mark_with_la.emplace_back(module.base.uid);
    }
  }

  global_config.assign_la_for_modules(modules_to_mark_with_la);
}

} // namespace gui
