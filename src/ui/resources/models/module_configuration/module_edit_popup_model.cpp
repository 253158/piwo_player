#include "module_edit_popup_model.hpp"
namespace gui
{
module_edit_popup_model::module_edit_popup_model(QObject* parent)
  : QObject(parent)
{
}

void
module_edit_popup_model::open(const std::vector<piwo::uid>& modules)
{
  spdlog::trace(__PRETTY_FUNCTION__);

  if (modules.empty())
  {
    return;
  }
  if (auto expected_state = false;
      !this->_dialog_opened.compare_exchange_strong(expected_state, true))
  {
    spdlog::warn("Popup already opened!");
    return;
  }

  this->_modules_uids = modules;

  if (modules.size() == 1)
  {
    modinfo key{};
    key.base.uid = *modules.begin();
    const auto& [mtx, modules] = global_config.acquire_modules();
    auto module = modules.find(key);

    if (module == modules.end())
    {
      spdlog::warn("Failed to find uid[{}]",
                   modules.begin()->base.uid.to_ascii());
      return;
    }
    this->_modinfo = *module;
  }
  else
  {
    this->_modinfo = modinfo{};
  }

  this->_id_was_changed = false;
  this->_logic_address_was_changed = false;
  this->_gateway_id_was_changed = false;
  this->_radio_id_was_changed = false;
  this->_pos_x_was_changed = false;
  this->_pos_y_was_changed = false;
  this->_static_color_was_changed = false;

  // TODO(adam) 8 bytes ID should be supported by UI
  // Consider Integer64

  emit this->uid_changed(this->get_uid());

  auto narrowed_id = narrow<int>(this->_modinfo.base.id);
  if (!narrowed_id) [[unlikely]]
  {
    spdlog::critical("Unsupported ID = {}", *narrowed_id);
    return;
  }

  emit this->id_changed(static_cast<int>(this->_modinfo.base.id));
  emit this->logic_address_changed(this->_modinfo.logic_address);
  emit this->gateway_id_changed(this->_modinfo.gateway_id);
  emit this->radio_id_changed(this->_modinfo.radio_id);
  emit this->pos_x_changed(this->_modinfo.position.x);
  emit this->pos_y_changed(this->_modinfo.position.y);
  emit this->static_color_changed(QColor(this->_modinfo.static_color.packed()));

  this->set_show_dialog(true);
}

Q_INVOKABLE void
module_edit_popup_model::close()
{
  this->set_show_dialog(false);
}

Q_INVOKABLE void
module_edit_popup_model::apply()
{
  if (this->_modules_uids.size() == 1)
  {
    if (this->_id_was_changed)
    {
      global_config.set_module_id(this->_modinfo.base.uid,
                                  this->_modinfo.base.id);
    }

    if (this->_logic_address_was_changed)
    {
      global_config.set_module_logic_address(this->_modinfo.base.uid,
                                             this->_modinfo.logic_address);
    }

    if (this->_pos_x_was_changed || this->_pos_y_was_changed)
    {
      global_config.assign_module_position(this->_modinfo.base.uid,
                                           this->_modinfo.position);
    }
  }

  if (this->_gateway_id_was_changed)
  {
    global_config.set_modules_gateway_id(this->_modules_uids,
                                         this->_modinfo.gateway_id);
  }

  if (this->_radio_id_was_changed)
  {
    global_config.set_modules_radio_id(this->_modules_uids,
                                       this->_modinfo.radio_id);
  }

  if (this->_static_color_was_changed)
  {
    global_config.set_modules_static_color(this->_modules_uids,
                                           this->_modinfo.static_color);
  }
}

QString
module_edit_popup_model::get_uid()
{
  if (this->_modules_uids.size() == 1)
  {
    return QString::fromStdString(this->_modinfo.base.uid.to_ascii());
  }

  return QString();
}

int
module_edit_popup_model::get_id()
{
  if (this->_modules_uids.size() == 1)
  {
    return static_cast<int>(this->_modinfo.base.id);
  }

  return -1;
}

void
module_edit_popup_model::set_id(int id)
{
  if (id != this->_modinfo.base.id && this->_modules_uids.size() == 1 && id > 0)
  {
    this->_id_was_changed = true;
    this->_modinfo.base.id = id;
    emit this->id_changed(static_cast<int>(this->_modinfo.base.id));
  }
}

int
module_edit_popup_model::get_logic_address()
{
  if (this->_modules_uids.size() == 1)
  {
    return this->_modinfo.logic_address;
  }

  return -1;
}

void
module_edit_popup_model::set_logic_address(int logic_address)
{
  auto new_logic_address =
    std::clamp(logic_address, MIN_LOGIC_ADDRESS, MAX_LOGIC_ADDRESS);

  if (this->_modinfo.logic_address == new_logic_address ||
      this->_modules_uids.size() != 1)
  {
    return;
  }

  this->_logic_address_was_changed = true;
  this->_modinfo.logic_address = new_logic_address;
  emit this->logic_address_changed(this->_modinfo.logic_address);
}

int
module_edit_popup_model::get_gateway_id()
{
  return this->_modinfo.gateway_id;
}

void
module_edit_popup_model::set_gateway_id(int gateway_id)
{
  auto new_gateway_id = std::clamp(gateway_id, MIN_GATEWAY_ID, MAX_GATEWAY_ID);

  if (this->_modinfo.gateway_id == new_gateway_id)
  {
    return;
  }

  this->_gateway_id_was_changed = true;
  this->_modinfo.gateway_id = new_gateway_id;
  emit this->gateway_id_changed(this->_modinfo.gateway_id);
}

int
module_edit_popup_model::get_radio_id()
{
  return this->_modinfo.radio_id;
}

void
module_edit_popup_model::set_radio_id(int radio_id)
{
  auto new_radio_id = std::clamp(radio_id, MIN_RADIO_ID, MAX_RADIO_ID);

  if (this->_modinfo.radio_id == new_radio_id)
  {
    return;
  }

  this->_radio_id_was_changed = true;
  this->_modinfo.radio_id = new_radio_id;
  emit this->radio_id_changed(this->_modinfo.radio_id);
}

int
module_edit_popup_model::get_pos_x()
{
  if (this->_modules_uids.size() == 1)
  {
    return this->_modinfo.position.x;
  }

  return -2;
}

void
module_edit_popup_model::set_pos_x(int pos_x)
{
  auto new_pos_x =
    std::clamp(pos_x, MIN_RESOLUTION_WIDTH, MAX_RESOLUTION_WIDTH);

  if (this->_modinfo.position.x == new_pos_x || this->_modules_uids.size() != 1)
  {
    return;
  }

  this->_pos_x_was_changed = true;
  this->_modinfo.position.x = new_pos_x;
  emit this->pos_x_changed(this->_modinfo.position.x);
}

int
module_edit_popup_model::get_pos_y()
{
  if (this->_modules_uids.size() == 1)
  {
    return this->_modinfo.position.y;
  }

  return -2;
}

void
module_edit_popup_model::set_pos_y(int pos_y)
{
  auto new_pos_y =
    std::clamp(pos_y, MIN_RESOLUTION_HEIGHT, MAX_RESOLUTION_HEIGHT);

  if (this->_modinfo.position.y == new_pos_y || this->_modules_uids.size() != 1)
  {
    return;
  }

  this->_pos_y_was_changed = true;
  this->_modinfo.position.y = new_pos_y;
  emit this->pos_y_changed(this->_modinfo.position.y);
}

QColor
module_edit_popup_model::get_static_color()
{
  return QColor(this->_modinfo.static_color.packed());
}

void
module_edit_popup_model::set_static_color(
  QColor static_color) // NOLINT(performance-unnecessary-value-param)
{
  if (static_color.rgb() != this->_modinfo.static_color.packed())
  {
    this->_static_color_was_changed = true;
    this->_modinfo.static_color = static_color.rgb();
    emit this->static_color_changed(
      QColor(this->_modinfo.static_color.packed()));
  }
}

bool
module_edit_popup_model::get_show_dialog()
{
  return this->_dialog_opened;
}

void
module_edit_popup_model::set_show_dialog(bool show)
{
  this->_dialog_opened = show;
  emit this->show_dialog_changed(this->_dialog_opened);
}
} // namespace gui
