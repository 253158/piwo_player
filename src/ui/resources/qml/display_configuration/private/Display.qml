import Qt.labs.qmlmodels 1.0
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

Rectangle {
  id: container

  color: palette.dark

  Menu {
    id: contextMenu

    MenuItem {
      text: "Edit"
      onTriggered: {
        display_layout_model.edit_highlighted_modules();
      }
    }

    MenuItem {
      text: "Blink"
      onTriggered: {
        display_layout_model.blink_highlighted_modules();
      }
    }

    MenuItem {
      text: "Send Color"
      onTriggered: {
        display_layout_model.color_highlighted_modules();
      }
    }

    MenuItem {
      text: "Send Logic Address"
      onTriggered: {
        display_layout_model.assing_la_for_highlighted_modules();
      }
    }

    Menu {
      title: "Automation"
      MenuItem {
        text: "Match modules to selected tile/s"
        onTriggered: {
          display_layout_model.match_modules_to_tiles();
        }
      }

      MenuItem {
        text: "Match tiles to selected module/s"
        onTriggered: {
          display_layout_model.match_tiles_to_modules();
        }
      }
    }

  }

  TableView {
    id: table

    property var elementSize: 100

    anchors.fill: container
    columnSpacing: table.elementSize / 25
    rowSpacing: table.elementSize / 25
    clip: true
    focus: true
    state: "NORM"
    model: display_layout_model

    delegate: Rectangle {
      id: moduleTile

      implicitWidth: table.elementSize
      implicitHeight: table.elementSize
      Drag.active: moduleMouseArea.drag.active
      Drag.dragType: Drag.Automatic
      radius: table.elementSize / 25

      color: {
        if(model.is_highlighted == true) {
          palette.mid;
        }
        else {
          palette.midlight;
        }
      }

      states: [
        State {
          name: "NORM"

          PropertyChanges {
            target: moduleMouseArea
            drag.target: undefined
          }

          PropertyChanges {
            target: moduleTile
          }
        },
        State {
          name: "DRAG"

          PropertyChanges {
            target: moduleMouseArea
            drag.target: drag_data
          }

          PropertyChanges {
            target: moduleTile
          }
        }
      ]

      Text {
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        color: palette.text
        font.pointSize: table.elementSize / 5
        font.family: "Ubuntu"
        text: {
          if (model.id) {
            model.id.toString();
          }
          else {
            "";
          }
        }
      }

      MouseArea {
        id: moduleMouseArea

        pressAndHoldInterval: 500
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        propagateComposedEvents: true
        drag.threshold: 0
        anchors.fill: parent
        hoverEnabled: true
        Component.onCompleted: {
          moduleTile.state = "NORM";
        }
        onEntered: {
          if(model.id) {
            detailsPopup.open();
          }
        }
        onExited: {
          detailsPopup.close();
        }
        onPressed: {
          if (mouse.button == Qt.RightButton) {
            contextMenu.popup();
          }
          else {
            if((mouse.modifiers & Qt.ControlModifier) && model.is_highlighted == true) {
              display_layout_model.dehighlight(index / table.rows, index % table.rows);
            }
            else if(mouse.button & Qt.LeftButton) {
              display_layout_model.highlight(index / table.rows, index % table.rows);
            }
          }
        }
        onPressAndHold: {
          mouse.accepted = false;
          if (model.uid) {
            parent.grabToImage(function(result) {
              parent.Drag.imageSource = result.url;
            });
            display_layout_model.dehighlight(index / table.rows, index % table.rows)
            moduleTile.state = "DRAG";
          }
        }
      }

      Popup {
        id: detailsPopup
        x: table.elementSize
        y: table.elementSize
        modal: false
        focus: false
        contentItem: GridLayout {
          columns: 2
          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              "Id: ";
            }
          }
          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              if (typeof model.id != "undefined") {
                model.id.toString();
              }
              else {
                "nil";
              }
            }
          }

          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              "Logic address: ";
            }
          }
          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              if (typeof model.logic_address != "undefined") {
                model.logic_address.toString();
              }
              else {
                "nil";
              }
            }
          }

          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              "Gateway id: ";
            }
          }
          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              if (typeof model.gateway_id != "undefined") {
                model.gateway_id.toString();
              }
              else {
                "nil";
              }
            }
          }

          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              "Radio id: ";
            }
          }
          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              if (typeof model.radio_id != "undefined") {
                model.radio_id.toString();
              }
              else {
                "nil";
              }
            }
          }

          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              "UID: ";
            }
          }
          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              if (model.uid) {
                model.uid.toString();
              }
              else {
                "nil";
              }
            }
          }

          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              "X: ";
            }
          }
          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              if (typeof model.position_x != "undefined") {
                model.position_x.toString();
              }
              else {
                "nil";
              }
            }
          }

          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              "Y: ";
            }
          }
          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              if (typeof model.position_y != "undefined") {
                model.position_y.toString();
              }
              else {
                "nil";
              }
            }
          }

          Text {
            color: palette.text
            font.pointSize: 10
            font.family: "Ubuntu"
            text: {
              "Static color: ";
            }
          }
          RowLayout
          {
            Text {
              color: palette.text
              font.pointSize: 10
              font.family: "Ubuntu"
              text: {
                if (typeof model.static_color != "undefined") {
                  "#" + model.static_color.toString(16);
                }
                else {
                  "nil";
                }
              }
            }
            Rectangle {
              Layout.preferredWidth: 40
              Layout.fillHeight: true
              color: {
                if (typeof model.static_color != "undefined") {
                  "#" + model.static_color.toString(16);
                }
                else
                {
                  "transparent";
                }
              }
            }
          }
        }
      }

      DropArea {
        id: dragTarget

        anchors.fill: parent
        onDropped: {
          display_layout_model.module_dropped(drag.source.uid, index / table.rows, index % table.rows);
        }
      }

      Drag.source: ModuleDragData {
        id: drag_data
        uid: model.uid
      }
    }
  }

  MouseArea {
    acceptedButtons: Qt.LeftButton | Qt.RightButton
    anchors.fill: parent
    onPressed: {
      mouse.accepted = false;
      if (mouse.button & Qt.RightButton) {
        contextMenu.popup();
      }
      else if(!(mouse.modifiers & Qt.ControlModifier)) {
        display_layout_model.dehighlight_all();
      }
    }
    onReleased: {
      mouse.accepted = false;
    }
    onWheel: {
      wheel.accepted = false;
      propagateComposedEvents: true;
      if (wheel.modifiers & Qt.ControlModifier) {
        if (wheel.angleDelta.y > 0 && table.elementSize < 300) {
          table.elementSize += 10;
          wheel.accepted = true;
        }
        if (wheel.angleDelta.y < 0 && table.elementSize > 30) {
          table.elementSize -= 10;
          wheel.accepted = true;
        }
        table.forceLayout();
      }
    }
  }
}
