import Qt.labs.qmlmodels 1.0
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.0

Popup {

  function isModuleIdValid() {
    if(module_edit_popup_model
      && typeof module_edit_popup_model.id != "undefined"
      && module_edit_popup_model.id >= 0){
        return true;
    }
    else {
      return false;
    }
  }

  function isLogicAddressValid() {
    if(module_edit_popup_model
      && typeof module_edit_popup_model.logic_address != "undefined"
      && module_edit_popup_model.logic_address >= 0){
        return true;
    }
    else {
      return false;
    }
  }

  function isPositionXValid() {
    if(module_edit_popup_model
      && typeof module_edit_popup_model.pos_x != "undefined"
      && module_edit_popup_model.pos_x >= -1) {
        return true;
    }
    else {
      return false;
    }
  }

  function isPositionYValid() {
    if(module_edit_popup_model
      && typeof module_edit_popup_model.pos_y != "undefined"
      && module_edit_popup_model.pos_y >= -1) {
        return true;
    }
    else {
      return false;
    }
  }

  id: moduleEditPopup

  visible: {
    if(module_edit_popup_model) {
      module_edit_popup_model.show_dialog;
    }
    else {
      false;
    }
  }

  onClosed: {
    colorDialog.close();
    module_edit_popup_model.close();
  }

  anchors.centerIn: Overlay.overlay
  padding: 30
  modal: true
  focus: true
  contentItem: ColumnLayout {
    spacing: {30, 30}
    ColumnLayout {
      Text {
        Layout.preferredWidth: 400
        color: palette.text
        font.pointSize: 15
        font.family: "Ubuntu"
        text: {
          if(isModuleIdValid()) {
            "Single module edit";
          }
          else {
            "Multiple modules edit";
          }
        }
      }
      RowLayout {
        Text {
          visible: {
            isModuleIdValid();
          }
          color: palette.text
          font.pointSize: 11
          font.family: "Ubuntu"
          text: {
            "UID: ";
          }
        }
        Text {
          visible: {
            isModuleIdValid();
          }
          color: palette.text
          font.pointSize: 11
          font.family: "Ubuntu"
          text: {
            if(module_edit_popup_model && typeof module_edit_popup_model.uid != "undefined") {
              module_edit_popup_model.uid;
            }
            else {
              "";
            }
          }
        }
      }
    }

    GridLayout {
      Layout.fillHeight: true
      Layout.fillWidth: true
      columns: 2
      Text {
        visible: {
          isModuleIdValid();
        }
        color: palette.text
        font.pointSize: 10
        font.family: "Ubuntu"
        text: {
          "ID: "
        }
      }
      TextField {
        Layout.fillWidth: true
        visible: {
          isModuleIdValid();
        }
        text: {
          if (module_edit_popup_model && typeof module_edit_popup_model.id != "undefined") {
            module_edit_popup_model.id.toString();
          }
          else {
            "";
          }
        }

        onEditingFinished: {
          module_edit_popup_model.id = parseInt(text);
        }
      }

      Text {
        visible: {
          isLogicAddressValid();
        }
        color: palette.text
        font.pointSize: 10
        font.family: "Ubuntu"
        text: {
          "Logic address: ";
        }
      }
      TextField {
        Layout.fillWidth: true
        visible: {
          isLogicAddressValid();
        }
        text: {
          if (module_edit_popup_model && typeof module_edit_popup_model.logic_address != "undefined") {
            module_edit_popup_model.logic_address.toString();
          }
          else {
            "";
          }
        }

        onEditingFinished: {
          module_edit_popup_model.logic_address = parseInt(text);
        }
      }

      Text {
        color: palette.text
        font.pointSize: 10
        font.family: "Ubuntu"
        text: {
          "Gateway id: ";
        }
      }
      TextField {
        Layout.fillWidth: true
        text: {
          if (module_edit_popup_model && typeof module_edit_popup_model.gateway_id != "undefined") {
            module_edit_popup_model.gateway_id.toString();
          }
          else {
            "";
          }
        }

        onEditingFinished: {
          module_edit_popup_model.gateway_id = parseInt(text);
        }
      }

      Text {
        color: palette.text
        font.pointSize: 10
        font.family: "Ubuntu"
        text: {
          "Radio id: ";
        }
      }
      TextField {
        Layout.fillWidth: true
        text: {
          if (module_edit_popup_model && typeof module_edit_popup_model.radio_id != "undefined") {
            module_edit_popup_model.radio_id.toString();
          }
          else {
            "";
          }
        }

        onEditingFinished: {
          module_edit_popup_model.radio_id = parseInt(text);
        }
      }

      Text {
        visible: {
          isPositionXValid();
        }
        color: palette.text
        font.pointSize: 10
        font.family: "Ubuntu"
        text: {
          "Position X: ";
        }
      }
      TextField {
        Layout.fillWidth: true
        visible: {
          isPositionXValid();
        }
        text: {
          if (module_edit_popup_model && typeof module_edit_popup_model.pos_x != "undefined") {
            module_edit_popup_model.pos_x.toString();
          }
          else {
            "";
          }
        }

        onEditingFinished: {
          module_edit_popup_model.pos_x = parseInt(text);
        }
      }

      Text {
        visible: {
          isPositionYValid();
        }
        color: palette.text
        font.pointSize: 10
        font.family: "Ubuntu"
        text: {
          "Position Y: ";
        }
      }
      TextField {
        Layout.fillWidth: true
        visible: {
          isPositionYValid();
        }
        text: {
          if (module_edit_popup_model && typeof module_edit_popup_model.pos_y != "undefined") {
            module_edit_popup_model.pos_y.toString();
          }
          else {
            "";
          }
        }

        onEditingFinished: {
          module_edit_popup_model.pos_y = parseInt(text);
        }
      }

      Text {
        color: palette.text
        font.pointSize: 10
        font.family: "Ubuntu"
        text: {
          "Static color: ";
        }
      }
      RowLayout
      {
        ColorDialog {
          id: colorDialog
          title: "Please choose a color"
          onAccepted: {
            module_edit_popup_model.static_color = color;
          }
        }
        Text {
          color: palette.text
          font.pointSize: 10
          font.family: "Ubuntu"
          text: {
            if (module_edit_popup_model && typeof module_edit_popup_model.static_color != "undefined") {
              module_edit_popup_model.static_color.toString(16);
            }
            else {
              "nil";
            }
          }
        }
        Rectangle {
          Layout.fillWidth: true
          Layout.preferredHeight: 40
          color: {
            if (module_edit_popup_model && typeof module_edit_popup_model.static_color != "undefined") {
              module_edit_popup_model.static_color;
            }
            else
            {
              "transparent";
            }
          }
          MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: {
              colorDialog.visible = true;
            }
          }
        }
      }
    }

    RowLayout {
      Layout.alignment: Qt.AlignRight
      Button {
        Text {
          anchors.horizontalCenter: parent.horizontalCenter
          anchors.verticalCenter: parent.verticalCenter
          text: "Apply"
          font.pixelSize: 14
          color: palette.text
        }

        onClicked: {
          module_edit_popup_model.apply();
          moduleEditPopup.close();
        }
      }

      Button {
        Text {
          anchors.horizontalCenter: parent.horizontalCenter
          anchors.verticalCenter: parent.verticalCenter
          text: "Cancel"
          font.pixelSize: 14
          color: palette.text
        }

        onClicked: {
          moduleEditPopup.close();
        }
      }
    }
  }
}