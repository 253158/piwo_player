
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQml.Models 2.12
import QtQuick.Layouts 1.12
import QtSensors 5.15
import QtQuick.Dialogs 1.3

ApplicationWindow {
  id: mainWindow
  visible: true
  title: qsTr("P.I.W.O Player")

  palette.button: "#1d1d1d";
  palette.alternateBase: "black";
  palette.base: "#1d1d1d";
  palette.buttonText: "white";
  palette.dark: "#3e3e3e";
  palette.highlight: "white";
  palette.highlightedText: "black";
  palette.light: "#006ab1";
  palette.link: "blue";
  palette.linkVisited: "grey";
  palette.mid: "#006ab1";
  palette.midlight: "#606060";
  palette.shadow: "black";
  palette.text: "white";
  palette.toolTipBase: "white";
  palette.toolTipText: "black";
  palette.window: "#000000";
  palette.windowText: "white";

  function switchToLightMode() {
    palette.button = qsTr("#929292");
    palette.alternateBase = qsTr("black");
    palette.base = qsTr("#929292");
    palette.buttonText = qsTr("black");
    palette.dark = qsTr("#d4d4d4");
    palette.highlight = qsTr("black");
    palette.highlightedText = qsTr("white");
    palette.light = qsTr("#ca3333");
    palette.link = qsTr("blue");
    palette.linkVisited = qsTr("grey");
    palette.mid = qsTr("#ca3333");
    palette.midlight = qsTr("#ededed");
    palette.shadow = qsTr("black");
    palette.text = qsTr("black");
    palette.toolTipBase = qsTr("black");
    palette.toolTipText = qsTr("white");
    palette.window = qsTr("white");
    palette.windowText = qsTr("black");
  }

  function switchToDarkMode() {
    palette.button = qsTr("#1d1d1d");
    palette.alternateBase = qsTr("black");
    palette.base = qsTr("#1d1d1d");
    palette.buttonText = qsTr("white");
    palette.dark = qsTr("#3e3e3e");
    palette.highlight = qsTr("white");
    palette.highlightedText = qsTr("black");
    palette.light = qsTr("#006ab1");
    palette.link = qsTr("blue");
    palette.linkVisited = qsTr("grey");
    palette.mid = qsTr("#006ab1");
    palette.midlight = qsTr("#606060");
    palette.shadow = qsTr("black");
    palette.text = qsTr("white");
    palette.toolTipBase = qsTr("white");
    palette.toolTipText = qsTr("black");
    palette.window = qsTr("black");
    palette.windowText = qsTr("white");
  }

  menuBar: MainWindowMenuBar {
  }

  FileDialog {
    id: importModulesInfoFileDialog
    height: 500
    width: 500
    title: "Read modules info"
    folder: shortcuts.home
    nameFilters: [ "Modules info files (*.json)" ]
    selectExisting: true
    selectFolder: false
    selectMultiple: false
    sidebarVisible: true
    onAccepted: {
      module_configuration_widget.import_modules(importModulesInfoFileDialog.fileUrls);
      importModulesInfoFileDialog.close();
    }
  }

  FileDialog {
    id: importDisplayConfigurationFileDialog
    height: 500
    width: 500
    title: "Read display configuration"
    folder: shortcuts.home
    nameFilters: [ "Display configuration files (*.json)" ]
    selectExisting: true
    selectFolder: false
    selectMultiple: false
    sidebarVisible: true
    onAccepted: {
      module_configuration_widget.import_configuration(importDisplayConfigurationFileDialog.fileUrls);
      importDisplayConfigurationFileDialog.close();
    }
  }

  FileDialog {
    id: exportDisplayConfigurationFileDialog
    height: 500
    width: 500
    title: "Save display configuration"
    folder: shortcuts.home
    nameFilters: [ "Display configuration files (*.json)" ]
    selectExisting: false
    selectFolder: false
    selectMultiple: false
    sidebarVisible: true
    onAccepted: {
      module_configuration_widget.export_configuration(exportDisplayConfigurationFileDialog.fileUrls);
      exportDisplayConfigurationFileDialog.close();
    }
  }

  FileDialog {
    id: importAnimationFileDialog
    height: 500
    width: 500
    title: "Choose animation(s)"
    folder: shortcuts.home
    nameFilters: [ "PIWO7 animation files (*.piwo7)" ]
    selectExisting: true
    selectFolder: false
    selectMultiple: true
    sidebarVisible: true
    onAccepted: {
      exportDisplayConfigurationFileDialog.close();

      for (let i = 0; i < importAnimationFileDialog.fileUrls.length; ++i) {
        let file = importAnimationFileDialog.fileUrls[i]
        player_widget.load_animation(file)
      }

    }
  }

  header: TabBar {
    id: main_window_tap_bar
    spacing: 2

    MainWindowTabButton {
      mainWindowTabButtonTabName: qsTr("Display configuration")
    }
    MainWindowTabButton {
      mainWindowTabButtonTabName: qsTr("Player")
    }
    MainWindowTabButton {
      mainWindowTabButtonTabName: qsTr("Launchpad")
    }
    MainWindowTabButton {
      mainWindowTabButtonTabName: qsTr("Settings")
    }
    MainWindowTabButton {
      mainWindowTabButtonTabName: qsTr("Snake")
    }
  }

  footer: MainWindowFooter {
    height: 25
  }

  ColumnLayout {
      anchors.fill: parent
    StackLayout {
      id: main_window_stack_layout
      currentIndex: main_window_tap_bar.currentIndex
      Layout.fillHeight: true
      Layout.fillWidth: true
      Layout.margins: 50

      DisplayConfiguration {

      }

      Player {

      }

      Launchpad {

      }

      Settings {

      }

      Snake {

      }
    }
  }
}
