import QtQuick 2.12
import QtQuick.Controls 2.12

TabButton {
  property string mainWindowTabButtonTabName

  contentItem: Text {
    color: palette.text
    font.pointSize: 12
    font.family: "Ubuntu"
    text: mainWindowTabButtonTabName
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
    elide: Text.ElideRight
  }
}
