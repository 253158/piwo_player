import QtQuick 2.12
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.12

import "private"

Rectangle {
      RowLayout {
        Button {
          id: lp_connect_button
          Layout.alignment: Qt.AlignRight
          text: qsTr("LP connect")
          onPressed: {
            settings_widget.lp_connect();
          }
        }
        Button {
          id: lp_disconnect_button
          Layout.alignment: Qt.AlignRight
          text: qsTr("LP disconnect")
          onPressed: {
            settings_widget.lp_disconnect();
          }
        }
      }

}
