import QtQuick 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Rectangle {
    color: palette.dark

    RowLayout {
        id: connect_row
        Layout.fillWidth: true
        Layout.leftMargin: 50
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.top: parent.top
        anchors.topMargin: 10
        spacing: 10

        Label {
            text: "IP"
        }

        TextField {
            Layout.fillWidth: true
            id: text_server_ip
            text: "localhost"
        }

        Button {
            id: button_connect
            Layout.alignment: Qt.AlignRight
            text: snake_model ? snake_model.connected ? qsTr("Disconnect") : qsTr("Connect") : "";

            onPressed: {
                if (snake_model.connected) {
                    snake_model.disconnect();
                } else {
                    snake_model.connect(text_server_ip.text);
                }
            }
        }
    }

    Button {
        id: button_start
        anchors.horizontalCenter: connect_row.horizontalCenter
        anchors.top: connect_row.bottom
        anchors.topMargin: 30
        spacing: 10

        visible: snake_model ? snake_model.connected : "";
        text: snake_model ? snake_model.started ? qsTr("Stop snake") : qsTr("Start snake") : "";

        onPressed: {
            if (snake_model.started) {
                snake_model.stop();
            } else {
                snake_model.start();
            }
        }
    }

    Slider {
        id: slider_speed
        anchors.horizontalCenter: button_start.horizontalCenter
        anchors.top: button_start.bottom
        anchors.topMargin: 30
        snapMode: Slider.SnapOnRelease
        width: 255

        from: 50
        to: 500
        value: snake_model ? snake_model.get_initial_move_time() : 50
        stepSize: 50

        visible: snake_model ? snake_model.connected : false;

        onMoved: {
            snake_model.set_snake_move_time(value);
        }
    }

    Label {
        id: label_speed
        anchors.horizontalCenter: slider_speed.horizontalCenter
        anchors.top: slider_speed.bottom

        text: "Snake move time (ms): " + slider_speed.value
        visible: snake_model ? snake_model.connected : false;
    }

    Label {
        anchors.horizontalCenter: label_speed.horizontalCenter
        anchors.top: label_speed.bottom
        anchors.topMargin: 50

        text: "Clients connected: " + (snake_model ? snake_model.clients_count : 0)
        visible: snake_model ? snake_model.connected : "";
    }
}
