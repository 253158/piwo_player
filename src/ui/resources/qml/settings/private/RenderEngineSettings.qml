import QtQuick 2.12
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import Qt.labs.qmlmodels 1.0

import general_settings_model 1.0

import "../common"

RowLayout {
  Layout.topMargin: 10
  spacing: 2

  ColumnLayout {
    RowLayout{
    Label {
      text: "Render engine. "
      font.pixelSize: 16
      }
      FlatDesignSwitch {
        Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
        Layout.preferredHeight: 17
        onColor: palette.light
        onBorder: palette.light

        checkable: false
        checked: ( render_engine_model &&
                   render_engine_model.re_enabled == true)

        onClicked: {
          logger_model.log_trace("Render engine state changed requested");
          render_engine_model.toggle_re();
        }
      }
    }

    GridLayout
    {
      enabled: render_engine_model && render_engine_model.re_enabled
      Layout.fillWidth: true
      columns: 2

          Label {
            text: "Source: "
          }
          ComboBox {
              width: 500
              model: {
                if(render_engine_model) {
                  render_engine_model.provider_list
                }else{
                  []
                }
              }
              onActivated: render_engine_model.source_provider = currentText
          }

          Label {
            text: "Destination: "
          }
          ComboBox {
              model: {
                if(render_engine_model){
                  render_engine_model.provider_list
                }else{
                  []
                }
              }
              onActivated: render_engine_model.destination_provider = currentText
            }

          Label {
            text: "X offset: "
          }
          TextField {
            text: {
              if (render_engine_model && typeof render_engine_model.off_x!= "undefined") {
                render_engine_model.off_x.toString();
              }
              else {
                "";
              }
            }

            onEditingFinished: if(render_engine_model) {render_engine_model.off_x = parseInt(text)}
          }


          Label {
            text: "Y offset: "
          }
          TextField {
            text: {
              if (render_engine_model && typeof render_engine_model.off_y != "undefined") {
                render_engine_model.off_y.toString();
              }
              else {
                "";
              }
            }

            onEditingFinished: if(render_engine_model) {render_engine_model.off_y = parseInt(text)}
          }

          Label {
            text: "Effect: "
          }
          ComboBox {
              width: 500
              onActivated: render_engine_model.effect = currentValue
              model: {
                if(render_engine_model) {
                  render_engine_model.effects_list
                } else {
                  []
                }
              }
          }

      Button {
        Layout.alignment: Qt.AlignRight
        text: qsTr("Append")
        onClicked: {
          logger_model.log_trace("Append to render engine pipeline requested");
          render_engine_model.apply();
        }
      }
    }
  }
}
