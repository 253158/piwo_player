import QtQuick 2.12
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12

import ethernet_settings_model 1.0

ColumnLayout {
  Layout.fillWidth: true
  Layout.topMargin: 10

  Label {
    text: "Connection info"
    font.pixelSize: 16
  }

  RowLayout
  {
    Layout.fillWidth: true
    Layout.leftMargin: 50
    spacing: 5

    Label {
      text: "IP"
    }

    TextField {
      Layout.fillWidth: true
      id: settings_ethernet_ip
      text: "localhost"
    }

    Label {
      text: "TCP Port"
    }

    TextField {
      Layout.fillWidth: true
      id: settings_ethernet_port_tcp
      text: "1337"
    }

    Label {
      text: "UDP Port"
    }

    TextField {
      Layout.fillWidth: true
      id: settings_ethernet_port_udp
      text: "31337"
    }

    Button {
      id: settings_ethernet_connect_button
      Layout.alignment: Qt.AlignRight

      text: ethernet_settings_model ? ethernet_settings_model.eth_connected ? qsTr("Disconnect") : qsTr("Connect") : "";

      onPressed: {
        if (ethernet_settings_model.eth_connected) {
          ethernet_settings_model.tx_disconnect();
        }

        else {
          ethernet_settings_model.tx_connect(settings_ethernet_ip.text, settings_ethernet_port_tcp.text, settings_ethernet_port_udp.text)
        }
      }
    }
  }
}
