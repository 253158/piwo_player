import QtQuick 2.12
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.12

import "common"
import "private"

Rectangle {
  id: settings_widget_container

  RowLayout
  {
    anchors.fill: settings_widget_container
    SideBar {
      id: settings_widget_side_bar
      Layout.fillHeight: true
    }

    ColumnLayout {
      Layout.fillHeight: true
      Layout.fillWidth: true
      StackLayout {
        id: settings_widget_stack_layout
        currentIndex: settings_widget_side_bar.currentIndex
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.margins: 50

        GeneralSettings {
        }

        RenderSettings {
        }

        EthernetSettings {
        }
      }

      RowLayout {
        Layout.alignment: Qt.AlignRight
        Layout.margins: 10
        Button {
          id: cancel_settings_button
          Layout.alignment: Qt.AlignRight
          text: qsTr("Cancel")
          onPressed: {
            settings_widget.cancel();
          }
        }

        Button {
          id: apply_settings_button
          Layout.alignment: Qt.AlignRight
          text: qsTr("Apply")
          onPressed: {
            settings_widget.apply();
          }
        }
      }
    }
  }
  color: palette.dark
}
