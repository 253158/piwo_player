#include "ui_iface.hpp"

void
start_ui([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
  spdlog::warn("No UI connected");
}

void
config_module_added([[maybe_unused]] const piwo::uid uid)
{
  spdlog::warn("No UI connected");
}

void
config_module_removed([[maybe_unused]] const piwo::uid uid)
{
  spdlog::warn("No UI connected");
}

void
config_module_data_changed([[maybe_unused]] const piwo::uid uid)
{
  spdlog::warn("No UI connected");
}

void
config_resolution_changed()
{
  spdlog::warn("No UI connected");
}

void
application_state_changed([[maybe_unused]] const application_state_t state)
{
  spdlog::warn("No UI connected");
}

void
display_animation_changed()
{
  spdlog::warn("No UI connected");
}

void
playlist_changed()
{
  spdlog::warn("No UI connected");
}

void
light_show_started()
{
  spdlog::warn("No UI connected");
}

void
light_show_stopped()
{
  spdlog::warn("No UI connected");
}

void
animation_started()
{
  spdlog::warn("No UI connected");
}

void
animation_paused()
{
  spdlog::warn("No UI connected");
}

void
animation_stopped()
{
  spdlog::warn("No UI connected");
}

void
render_engine_state_chnaged(bool)
{
  spdlog::warn("No UI connected");
}

void
render_engine_configured([[maybe_unused]] const pipeline_config& config)
{
  spdlog::warn("No UI connected");
}

void
render_engine_step_removed([[maybe_unused]] step_id id)
{
  spdlog::warn("No UI connected");
}

void
render_engine_step_state_changed([[maybe_unused]] step_id id,
                                 [[maybe_unused]] bool state)
{
  spdlog::warn("No UI connected");
}

void
provider_list_changed(const providers_map_t&)
{
  spdlog::warn("No UI connected");
}

void
snake_disconnected()
{
  spdlog::warn("No UI connected");
}

void
snake_connection_established()
{
  spdlog::warn("No UI connected");
}

void
snake_refresh_clients_count([[maybe_unused]] int clients_count)
{
  spdlog::warn("No UI connected");
}

void
snake_move_state_changed([[maybe_unused]] bool state)
{
  spdlog::warn("No UI connected");
}

void
eth_state_changed(bool)
{
  spdlog::warn("No UI connected");
}
