#include "event_loop.hpp"
#include "config.hpp"
#include <chrono>
#include <piwo/common_intersection_operators.h>
#include <piwo/frames_intersection.h>

void
event_loop::send_event(event_packet_t event)
{
  locked_scope(
    [&]
    {
      this->_events.push(event);
      this->_cv.notify_all();
    },
    this->_mtx);
}

event_loop::event_loop()
{
  this->_th = std::thread(&event_loop::process_events, std::ref(*this));
}

event_loop::~event_loop()
{
  locked_scope(
    [&]
    {
      this->_events = queue_t();
      this->_events.push({ event_t::finish_loop, no_args() });
      this->_cv.notify_all();
    },
    this->_mtx);

  if (this->_th.joinable())
    this->_th.join();
}

event_loop::event_packet_t
event_loop::pop_event()
{
  std::unique_lock<std::mutex> lk(this->_mtx);
  this->_cv.wait(lk, [this]() { return (this->_events.size() > 0); });

  event_packet_t ev = this->_events.front();
  this->_events.pop();
  return ev;
}

void
event_loop::process_events()
{
  while (true)
  {
    event_packet_t ev = this->pop_event();
    switch (ev.ev)
    {
      case event_t::resize:
      {
        auto [width, height] = std::get<resize_data>(ev.data);
        handle_resize(width, height);
      }
      break;
      case event_t::eth_connect_tx:
      {
        auto [ip, tcp_port, udp_port] = std::get<eth_data>(ev.data);
        handle_tx_connect(ip, tcp_port, udp_port);
      }
      break;
      case event_t::eth_disconnect_tx:
      {
        handle_tx_disconnect();
      }
      break;
      case event_t::snake_connect:
      {
        auto data = std::get<snake_conn_data>(ev.data);
        handle_snake_connect(data.ip);
      }
      break;
      case event_t::snake_disconnect:
      {
        handle_snake_disconnect();
      }
      break;
      case event_t::finish_loop:
        return;
      default:
        break;
    };
  }
}

void
event_loop::handle_resize(size_t width, size_t height)
{
  // TODO take application state mutex
  auto [mtx, providers] = global_config.acquire_providers();
  for (const auto& [name, provider] : providers)
  {
    provider->resize(width, height);
  }
}

void
event_loop::handle_tx_connect(const std::string& ip,
                              const std::string& tcp_port,
                              const std::string& udp_port)
{
  global_config.tx_connect(ip, tcp_port, udp_port);
}

void
event_loop::handle_tx_disconnect()
{
  global_config.tx_disconnect();
}

void
event_loop::handle_snake_connect(const std::string& ip)
{
  global_config.reconnect_snake(ip);
}

void
event_loop::handle_snake_disconnect()
{
  global_config.disconnect_snake();
}
