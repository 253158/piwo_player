#include "network.hpp"

#include <boost/asio.hpp>
#include <boost/asio/ip/address.hpp>
#include <fmt/core.h>

#include <piwo/proto.h>
#include <piwo/protodef.h>
#include <string>

#include "config.hpp"

template<typename T>
static void
log_bytes(const T& arr, spdlog::level::level_enum level)
{
  constexpr size_t line_width = 16;

  spdlog::log(level, "Dump of {} bytes", arr.size());

  size_t i = 0;

  for (;;)
  {
    const auto beg = i;
    const auto end = std::min(i + line_width, arr.size());

    spdlog::log(
      level,
      "{:04X}: {:02X}",
      i,
      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
      fmt::join(std::begin(arr) + beg, std::begin(arr) + end, " "));

    i += line_width;

    if (end == arr.size())
      break;
  }
}

void
tx_eth_connection::join_io_worker()
{
  if (this->_udp_io_context_runner.joinable())
    this->_udp_io_context_runner.join();
}

void
tx_eth_connection::restart_io_worker()
{
  spdlog::debug("Starting eth IO worker");

  this->join_io_worker();
  this->_udp_io_context_runner = std::thread(
    [this]
    {
      using work_guard_type = boost::asio::executor_work_guard<
        boost::asio::io_context::executor_type>;

      spdlog::debug("eth IO worker started");

      work_guard_type work_guard(this->_udp_io_context.get_executor());

      this->_udp_io_context.restart();
      this->_udp_io_context.run();

      spdlog::debug("eth IO worker finished");
    });
}

tx_eth_connection::~tx_eth_connection()
{
  try
  {
    this->close();
    this->join_io_worker();
  }
  catch (...)
  {
    fmt::print("An expection occured in tx_eth_connection destructor");
  }
}

bool
tx_eth_connection::open(const char* host,
                        const char* tcp_port,
                        const char* udp_port)
{
  using boost::asio::ip::tcp;
  using boost::asio::ip::udp;

  bool ret = false;

  try
  {
    tcp::resolver tcp_resolver(this->_tcp_io_context);
    boost::asio::connect(this->_tcp_socket,
                         tcp_resolver.resolve(host, tcp_port));
    ret = this->_tcp_socket.is_open();
    if (ret)
    {
      this->_tcp_io_context.run();

      udp::resolver udp_resolver(this->_udp_io_context);
      this->_udp_endpoint =
        *udp_resolver.resolve(udp::v4(), host, udp_port).begin();
      this->_udp_socket.open(boost::asio::ip::udp::v4());
    }
  }
  catch (const boost::system::system_error& ex)
  {
    spdlog::error("Connecting to TX failed: \"{}\"", ex.what());
    ret = false;
  }
  catch (...)
  {
    spdlog::error("Connecting to TX failed with unexpected error.");
    ret = false;
  }

  if (ret)
    this->restart_io_worker();

  return ret;
}

void
tx_eth_connection::close()
{
  spdlog::debug("Stopping eth IO worker");
  this->_udp_io_context.stop();
  this->_tcp_io_context.stop();
  this->_udp_socket.close();
  this->_tcp_socket.close();
}

void
network_packet_write_handler::operator()(boost::system::error_code error,
                                         std::size_t bytes_written)
{
  if (error)
  {
    spdlog::debug("Packet write handler @ {}: Failed to send bytes, with: {}",
                  (void*)this,
                  error.message());
  }
  else
  {
    spdlog::debug("Packet write handler @ {}: Successfully send {} bytes",
                  (void*)this,
                  bytes_written);
  }

  this->master_connection.remove_pending(packet_node);
}

static network_packet_buffer_t
make_network_packet_buffer_t(size_t size)
{
  return network_packet_buffer_t(size, static_cast<std::byte>(0));
}

network_packet_queue_iterator_t
tx_eth_connection::make_packet_queue_node_locked(packet_t packet)
{
  std::unique_lock lck(this->_packet_queue_mtx);
  this->_packet_queue.emplace_back(make_network_packet_buffer_t(packet.size()));
  std::copy(std::begin(packet),
            std::end(packet),
            std::begin(this->_packet_queue.back()));
  return --this->_packet_queue.end();
}

inline network_packet_write_handler
tx_eth_connection::make_write_packet_handler(
  network_packet_queue_iterator_t node_it)
{
  return network_packet_write_handler{ .master_connection = *this,
                                       .packet_node = node_it };
}

bool
tx_eth_connection::tcp_send(packet_t packet)
{
  if (!this->is_opened())
    return false;

  boost::asio::async_write(
    this->_tcp_socket,
    boost::asio::buffer(packet.data(), packet.size()),
    [](const boost::system::error_code& error, std::size_t bytes_written)
    {
      if (error)
      {
        spdlog::debug("TCP write failed. An error occured {}", error.message());
      }
      else
      {
        spdlog::debug("Bytes {} written via TCP", bytes_written);
      }
    });

  return true;
}

void
tx_eth_connection::enqueue(packet_t packet)
{
  network_packet_queue_iterator_t packet_it =
    make_packet_queue_node_locked(packet);

  network_packet_write_handler handler = make_write_packet_handler(packet_it);
  spdlog::debug("Created write handler for packet @ {}", (void*)&(*packet_it));

  spdlog::debug("Enqueued packet:");
  log_bytes(packet, spdlog::level::level_enum::debug);

  std::unique_lock lck(this->_udp_write_mtx);
  this->_udp_socket.async_send_to(
    boost::asio::buffer(packet_it->data(), packet_it->size()),
    this->_udp_endpoint,
    handler);
}

bool
tx_eth_connection::udp_send(packet_t packet)
{
  if (!this->is_opened())
    return false;

  std::unique_lock lck(this->_udp_write_mtx);
  this->_udp_socket.async_send_to(
    boost::asio::buffer(packet.data(), packet.size()),
    this->_udp_endpoint,
    [](const boost::system::error_code& error, std::size_t bytes_written)
    {
      if (error)
      {
        spdlog::debug("UDP write failed. An error occured {}", error.message());
      }
      else
      {
        spdlog::debug("Bytes {} written via UDP", bytes_written);
      }
    });

  return true;
}

template<typename Iter_>
constexpr static bool
invalidates_iterators_on_remove()
{
  // TODO: Find a better way to check if removing element from container
  //       that contains given iterator invalidates other iterators on removal.
  using Iter = std::remove_cv_t<std::remove_reference_t<Iter_>>;
  return std::is_same_v<typename Iter::iterator_category,
                        std::random_access_iterator_tag>;
}

void
tx_eth_connection::remove_pending(network_packet_queue_iterator_t packet_it)
{
  static_assert(!invalidates_iterators_on_remove<decltype(packet_it)>());

  std::unique_lock lck(this->_packet_queue_mtx);
  this->_packet_queue.erase(packet_it);
}

static piwo::raw_packet
trim_packet(piwo::raw_packet p, size_t s)
{
  return piwo::raw_packet(p.data(), std::min(s, p.size()));
}

static std::optional<piwo::lightshow>
prepare_lightshow_packet(const lightshow_config& config,
                         piwo::raw_packet raw_packet,
                         const size_t pixel_offset,
                         const uint8_t ttf)
{
  std::optional opt_ls_builder =
    piwo::lightshow_builder::make_lightshow_builder(raw_packet);

  if (!opt_ls_builder.has_value())
  {
    spdlog::error("Unexpected internal error: make_lightshow_builder with the "
                  "local packet buffer failed.");
    return std::nullopt;
  }

  auto& ls_builder = opt_ls_builder.value();
  ls_builder.set_ttf(ttf);
  ls_builder.set_first_la(config.device_offset + pixel_offset);

  for (size_t i = pixel_offset;; ++i)
  {
    std::optional pixel_info = config.get_nth(i);

    if (!pixel_info.has_value())
      break;

    const piwo::color& color = pixel_info.value().color;
    piwo::packed_color packed_color(color.r, color.g, color.b);
    if (!ls_builder.add_color(packed_color))
      break;
  }

  return piwo::lightshow(ls_builder);
}

static void
forward_packet_and_reset_builder(tx_eth_connection& output_connection,
                                 piwo::forward_w_cid_builder& forward_builder,
                                 piwo::raw_packet raw_packet)
{
  auto forward = piwo::forward_w_cid(forward_builder);
  output_connection.enqueue(piwo::raw_packet(forward.data(), forward.size()));

  std::optional forward_builder_opt =
    piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);

  forward_builder = forward_builder_opt.value();
}

void
send_frame_via_lightshow(const lightshow_config& config,
                         tx_eth_connection& output_connection)
{
  constexpr size_t lightshow_buffer_maximum_length =
    piwo::lightshow_buffer_maximum_length;

  // TODO: add creation of max length packet buffer for each type to libproto
  // TODO: check how much zeroing this array by default affects performance.
  std::array<piwo::net_byte_t, FORAWRDING_BUFFER_SIZE> packet_buffer{};
  piwo::raw_packet raw_packet(packet_buffer.data(), packet_buffer.size());

  std::optional forward_builder_opt =
    piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);

  auto forward_builder = forward_builder_opt.value();

  constexpr uint8_t ttf = lightshow_config::DEFAULT_TTF;

  for (size_t pixel_offset = 0u;;)
  {
    std::optional buff_slot_opt = forward_builder.get_next_slot();
    // no more memory send packet and continue
    if (!buff_slot_opt.has_value())
    {
      forward_packet_and_reset_builder(
        output_connection, forward_builder, raw_packet);
      continue;
    }

    auto buff_slot =
      trim_packet(buff_slot_opt.value(), lightshow_buffer_maximum_length);

    std::optional ls_packet_opt =
      prepare_lightshow_packet(config, buff_slot, pixel_offset, ttf);

    if (!ls_packet_opt.has_value())
    {
      forward_packet_and_reset_builder(
        output_connection, forward_builder, raw_packet);
      continue;
    }

    auto& ls_packet = *ls_packet_opt;

    pixel_offset += ls_packet.get_colors_count();

    if (ls_packet.get_colors_count() == 0)
      break;

    forward_builder.commit(ls_packet, config.id.radio);
  }

  // log_bytes(ls_packet, spdlog::level::level_enum::debug);
  auto forward = piwo::forward_w_cid(forward_builder);
  if (forward.size())
  {
    output_connection.enqueue(piwo::raw_packet(forward.data(), forward.size()));
  }
}

void
ls_sending_task::stop()
{
  this->_ls_send_task.stop_and_wait();
}

bool
ls_sending_task::stop_and_configure(std::shared_ptr<frame_provider> p,
                                    ls_config_array configs)
{
  this->stop();
  if (!p)
    return false;

  this->_provider = std::move(p);

  this->_configs = std::move(configs);
  return true;
}

static void
wait_for_next_frame(frame_provider* fp, ls_sending_task::ms_t frame_duration)
{
  if (fp->waitable())
    fp->wait();
  else
    std::this_thread::sleep_for(frame_duration);
}

void
ls_sending_task::ls_send_loop()
{
  spdlog::debug("Lightshow send loop enter");
  for (auto& config : this->_configs)
  {
    std::optional tx_conn_opt = global_config.get_tx_eth_conn(config.id.tx);
    if (!tx_conn_opt.has_value())
      continue;
    auto& tx_conn = *tx_conn_opt;

    send_frame_via_lightshow(config, *tx_conn);
  }
  wait_for_next_frame(this->_provider.get(), this->_frame_duration);
  spdlog::debug("Lightshow send loop exit");
}
