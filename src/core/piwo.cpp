#include "piwo_core.hpp"
#include "ui_iface.hpp"

#include <iostream>

using ls_config_array = ls_sending_task::ls_config_array;

ls_config_array
own_offsets(modinfo_t& modules, const std::shared_ptr<piwo::frame> frame)
{
  ls_config_array owners_info;
  // assign to each group
  for (const auto m : modules)
  {
    if (m.position.x == MODULE_INVALID_POS ||
        m.position.y == MODULE_INVALID_POS)
      continue;

    const unsigned int tx_id = m.gateway_id;
    const unsigned int radio_id = m.radio_id;
    auto config_f =
      std::find_if(owners_info.begin(),
                   owners_info.end(),
                   [&](const lightshow_config& c)
                   { return c.id.tx == tx_id && c.id.radio == radio_id; });

    if (config_f == owners_info.end())
    {
      lightshow_config conf{ .device_offset = 0,
                             .owner_info = {},
                             .id = { tx_id, radio_id },
                             .frame = frame };
      conf.owner_info.own(*frame, m.position.x, m.position.y);

      owners_info.push_back(conf);
      continue;
    }

    config_f->owner_info.own(*frame, m.position.x, m.position.y);
  }

  // assign offsets to each group
  size_t current_off = 0;
  for (auto& conf : owners_info)
  {
    conf.device_offset = current_off;
    current_off += conf.owner_info.owned_offsets.size();
  }

  return owners_info;
}

void
configure_logic_address(modinfo_t& modules,
                        ls_config_array& owners_info,
                        size_t width)
{
  for (auto& config : owners_info)
  {
    int off = 0;
    for (auto& m_pos : config.owner_info.owned_offsets)
    {
      auto mod =
        std::find_if(modules.begin(),
                     modules.end(),
                     [m_pos, width](const modinfo& m)
                     { return m.position.y * width + m.position.x == m_pos; });

      if (mod == modules.end())
        continue;
      modinfo mod_cp = *mod;
      modules.erase(mod);
      // since we are using the std set
      // we need to remove and insert modified element
      mod_cp.logic_address = config.device_offset + off;
      modules.insert(mod_cp);
      config_module_data_changed(mod_cp.base.uid);
      off++;
    }
  }
}
