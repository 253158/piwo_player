#include "playlist.hpp"

#include <piwo/color.h>
#include <spdlog/spdlog.h>

#include "lock.hpp"
#include "ui_iface.hpp"

#include <piwo/frame.h>

constexpr size_t PLAYLIST_FPS = 20;
constexpr piwo::color BLACK_PIXEL = { 0, 0, 0 };

constexpr static std::chrono::milliseconds
playlist_frame_rate(size_t fps)
{
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
  return std::chrono::milliseconds(1000 / fps);
}

playlist_t::playlist_t(resolution_t<size_t> resolution, id name)
  : frame_provider(true)
  , _name(std::move(name))
{
  if (resolution.width == 0 || resolution.height == 0)
  {
    this->_frame_buffer_ptr = piwo::alloc_frame_shared(1, 1);
  }
  else
  {
    this->_frame_buffer_ptr =
      piwo::alloc_frame_shared(resolution.width, resolution.height);
  }
}

void
playlist_t::reset_current_animation()
{
  if (this->_animations.size())
  {
    this->_current_animation = 0;
  }
  else
  {
    this->_current_animation = PLAYLIST_INVALID_ANIMATION_INDEX;
  }
}

playlist_t::~playlist_t()
{
  locked_scope(
    [&]
    {
      this->_is_finished = true;
      this->_cv.notify_all();
    },
    this->_framesync_mtx);
  this->_frame_buffer_ptr->fill(BLACK_PIXEL);
  internal_stop_thread_and_wait();
}

piwo::frames_intersection::frame_t
playlist_t::get_frame_()
{
  return this->_frame_buffer_ptr;
}

playlist_t::id
playlist_t::get_name()
{
  return this->_name;
}

void
playlist_t::framerate_thread_handler()
{
  while (this->_frame_rate_timer.is_valid() && this->_frame_rate_timer.wait())
  {
    if (this->_current_frame >=
        this->_animations[this->_current_animation].anim.frame_count())
    {
      if (!this->next())
      {
        this->internal_reset_non_blocking();
      }
    }
    else
    {
      locked_scope(
        [&]
        {
          this->_rendered = false;

          piwo::unsafe_intersection_apply(
            *this->_frame_buffer_ptr,
            this->_animations[this->_current_animation]
              .anim[this->_current_frame],
            0,
            0,
            piwo::frame_assign_op);
          this->_rendered = true;
          this->_cv.notify_all();
        },
        this->_framesync_mtx);
      ++this->_current_frame;
    }
  }

  locked_scope(
    [&]
    {
      this->_is_finished = true;
      this->_cv.notify_all();
    },
    this->_framesync_mtx);
}

bool
playlist_t::add_animation(named_animation anim)
{
  if (this->is_running())
    return false;
  locked_scope(
    [&]
    {
      this->buffer_resolution_check(anim);
      this->_animations.emplace_back(std::move(anim));
    },
    this->_animations_mtx);

  return true;
}

bool
playlist_t::remove_animation(int idx)
{
  if (this->is_running())
    return false;

  if (static_cast<size_t>(idx) > this->_animations.size())
    return false;

  std::unique_lock lock(this->_animations_mtx);

  this->_animations.erase(this->_animations.begin() + idx);
  for (const named_animation& anim : _animations)
  {
    buffer_resolution_check(anim);
  }

  return true;
}

std::optional<piwo::animation>
playlist_t::get_current_animation()
{
  std::unique_lock lock(this->_animations_mtx);
  if (this->_current_animation == PLAYLIST_INVALID_ANIMATION_INDEX)
    return std::nullopt;

  return this->_animations[this->_current_animation].anim;
}

bool
playlist_t::next()
{
  std::unique_lock lock(this->_animations_mtx);

  if (this->_current_animation < 0 ||
      (static_cast<size_t>(this->_current_animation) + 1) >=
        this->_animations.size())
    return false;

  ++this->_current_animation;
  this->_current_frame = 0;

  display_animation_changed();

  return true;
}

bool
playlist_t::prev()
{
  std::unique_lock lock(this->_animations_mtx);

  if (this->_current_animation <= 0)
    return false;

  --this->_current_animation;
  this->_current_frame = 0;

  display_animation_changed();

  return true;
}

bool
playlist_t::set_animation(size_t index)
{
  std::unique_lock lock(this->_animations_mtx);

  if (index >= this->_animations.size())
    return false;

  std::optional narrowed_index_opt = narrow<int64_t>(index);

  if (!narrowed_index_opt.has_value())
  {
    spdlog::error("Unsupported animation index = {}", index);
    return false;
  }
  this->_current_animation = narrowed_index_opt.value();
  this->_current_frame = 0;

  piwo::unsafe_intersection_apply(
    *this->_frame_buffer_ptr,
    this->_animations[this->_current_animation].anim[this->_current_frame],
    0,
    0,
    piwo::frame_assign_op);

  display_animation_changed();

  return true;
}

bool
playlist_t::set_frame_no(size_t frame_no)
{
  std::unique_lock lock(this->_animations_mtx);

  if (frame_no > this->_animations[this->_current_animation].anim.frame_count())
  {
    return false;
  }
  this->_current_frame = frame_no;

  return true;
}

void
playlist_t::wait()
{
  std::unique_lock lk(this->_framesync_mtx);
  this->_cv.wait(lk, [this] { return this->_rendered || this->_is_finished; });
}

std::optional<size_t>
playlist_t::get_current_animation_index()
{
  if (this->_current_animation < 0)
  {
    return std::nullopt;
  }

  return static_cast<size_t>(this->_current_animation);
}

size_t
playlist_t::get_current_animation_frame_no()
{
  return this->_current_frame;
}

bool
playlist_t::start()
{
  if (this->_current_animation < 0 ||
      static_cast<size_t>(this->_current_animation) > this->_animations.size())
  {
    return false;
  }

  internal_start_thread_if_not_running();
  spdlog::debug("Animation started");
  animation_started();
  display_animation_changed();

  return true;
}

void
playlist_t::pause()
{
  internal_stop_thread_and_wait();
  spdlog::debug("Animation paused");
  animation_paused();
}

void
playlist_t::stop()
{
  internal_stop_thread_and_wait();
  this->_frame_buffer_ptr->fill(BLACK_PIXEL);
  this->reset_current_animation();
  this->_current_frame = 0;
  spdlog::debug("Animation stopped");
  animation_paused();
  display_animation_changed();
}

void
playlist_t::resize_(size_t width, size_t height)
{
  spdlog::trace(
    "{} width = {} height = {}", __PRETTY_FUNCTION__, width, height);

  this->_frame_buffer_ptr = piwo::alloc_frame_shared(width, height);
}

void
playlist_t::buffer_resolution_check(const named_animation& animation)
{
  const auto anim_width = animation.anim.width();
  const auto anim_height = animation.anim.height();

  if (anim_width <= _frame_buffer_ptr->width &&
      anim_height <= _frame_buffer_ptr->height)
  {
    return;
  }

  const auto new_width = std::max(anim_width, _frame_buffer_ptr->width);
  const auto new_height = std::max(anim_height, _frame_buffer_ptr->height);

  resize_(new_width, new_height);
}

void
playlist_t::internal_start_thread_if_not_running()
{
  if (!this->is_running())
  {
    if (this->_frame_rate_thread.joinable())
    {
      this->_frame_rate_thread.join();
    }
    this->_frame_rate_timer.start(
      std::chrono::seconds(0), playlist_frame_rate(PLAYLIST_FPS), false);
    this->_frame_rate_thread =
      std::thread(&playlist_t::framerate_thread_handler, this);
  }
}

void
playlist_t::internal_stop_thread_and_wait()
{
  this->_frame_rate_timer.stop();
  if (this->_frame_rate_thread.joinable())
  {
    this->_frame_rate_thread.join();
  }
}

void
playlist_t::internal_reset_non_blocking()
{
  this->_frame_rate_timer.stop();
  this->reset_current_animation();
  this->_current_frame = 0;
  this->_frame_buffer_ptr->fill(BLACK_PIXEL);
  animation_stopped();
  display_animation_changed();
}
