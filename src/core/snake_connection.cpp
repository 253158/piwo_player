#include "snake_connection.hpp"

#include <spdlog/spdlog.h>

#include "config.hpp"
#include "iterator.hpp"
#include "lock.hpp"

snake_receive_task::snake_receive_task(ssl_socket_ptr_t& socket,
                                       snake_connection* observer)
  : _socket_ptr(socket)
  , _snake_observer(observer)
{
}

snake_receive_task::~snake_receive_task()
{
  this->notify_receive_task_finished();
}

void
snake_receive_task::replace_socket_ptr(const ssl_socket_ptr_t& socket)
{
  std::lock_guard lg(this->_socket_mx);
  this->_socket_ptr = socket;
}

void
snake_receive_task::refresh_data_buffer(size_t bytes_with_delimiter)
{
  this->_data_received.erase(
    this->_data_received.begin(),
    next(this->_data_received.begin(), bytes_with_delimiter));
}

void
snake_receive_task::notify_data_received(const snake_send_type& data,
                                         size_t size)
{
  if (size == 0)
    return;

  this->_snake_observer->update_data_received(
    std::vector(data.begin(), next(data.begin(), size)));
}

void
snake_receive_task::start_receive_data()
{
  this->wait_until_receive_task_finished();

  this->_data_received.clear();
  this->receive_data(this->_socket_ptr);
}

void
snake_receive_task::receive_data(const ssl_socket_ptr_t& socket)
{
  std::lock_guard lg(this->_socket_mx);
  async_read_until(*socket,
                   boost::asio::dynamic_buffer(this->_data_received),
                   data_delimiter,
                   [&, socket](const boost::system::error_code& er,
                               size_t bytes_with_delimiter)
                   {
                     if (!er)
                     {
                       this->notify_data_received(this->_data_received,
                                                  bytes_with_delimiter - 1);

                       this->refresh_data_buffer(bytes_with_delimiter);
                       this->receive_data(socket);
                     }
                     else
                     {
                       this->notify_receive_task_finished();
                     }
                   });
}

void
snake_receive_task::wait_until_receive_task_finished()
{
  std::unique_lock ul(this->_is_started_mx);
  this->_is_started_cv.wait(ul, [&]() { return !this->_is_receive_started; });
  this->_is_receive_started = true;
}

void
snake_receive_task::notify_receive_task_finished()
{
  locked_scope([&] { this->_is_receive_started = false; },
               this->_is_started_mx);

  this->_is_started_cv.notify_all();
}

snake_send_task::snake_send_task(ssl_socket_ptr_t& socket,
                                 snake_connection* observer)
  : _socket_ptr(socket)
  , _snake_observer(observer)
{
}

snake_send_task::~snake_send_task()
{
  this->stop_task();
}

void
snake_send_task::replace_socket_ptr(const ssl_socket_ptr_t& socket)
{
  std::lock_guard lg(this->_socket_mx);
  this->_socket_ptr = socket;
}

void
snake_send_task::start_task()
{
  this->_end_task = false;
  this->_send_executing = false;
  this->_send_loop_th = std::thread{ [&]() { this->send_loop(); } };
}

void
snake_send_task::stop_task()
{
  locked_scope([&] { this->_end_task = true; }, this->_send_mx);

  this->_send_data_cv.notify_all();

  if (this->_send_loop_th.joinable())
  {
    this->_send_loop_th.join();
  }
}

void
snake_send_task::send_data(const snake_send_type& data)
{
  locked_scope(
    [&]
    {
      this->_send_queue.push_back(data);
      this->_send_queue.back().push_back(data_delimiter);
    },
    this->_send_mx);

  this->_send_data_cv.notify_all();
}

void
snake_send_task::send_large_number(signals_to_server signal, size_t number)
{
  snake_send_type data_to_send{ static_cast<int8_t>(signal) };

  while (number >= data_delimiter)
  {
    data_to_send.emplace_back(data_delimiter - 1);
    number -= (data_delimiter - 1);
  }
  data_to_send.emplace_back(number);
  this->send_data(data_to_send);
}

void
snake_send_task::execute_send(std::unique_lock<std::mutex>&& ul_send_mx)
{
  this->_send_executing = true;
  boost::asio::mutable_buffer buf(this->_send_queue.front().data(),
                                  this->_send_queue.front().size() *
                                    sizeof(snake_send_type::value_type));
  auto it = this->_send_queue.begin();
  ul_send_mx.unlock();

  std::lock_guard lg(this->_socket_mx);
  boost::asio::async_write(*this->_socket_ptr,
                           buf,
                           [&, it](const boost::system::error_code&, size_t)
                           {
                             this->erase_el_from_queue(it);
                             this->_send_executing = false;
                             this->_send_data_cv.notify_all();
                           });
}

void
snake_send_task::send_loop()
{
  while (!this->_end_task)
  {
    std::unique_lock ul_send_mx(this->_send_mx);
    _send_data_cv.wait(ul_send_mx,
                       [&]()
                       {
                         return (this->_send_queue.size() > 0 &&
                                 !this->_send_executing) ||
                                this->_end_task;
                       });

    if (this->_end_task)
    {
      this->_send_executing = false;
      return;
    }
    this->execute_send(std::move(ul_send_mx));
  }
}

void
snake_send_task::erase_el_from_queue(const snake_send_iterator& it)
{
  std::lock_guard lg(this->_send_mx);
  this->_send_queue.erase(it);
}

snake_connection::snake_connection()
  : _ssl_context(boost::asio::ssl::context::sslv23)
  , _socket_ptr(
      std::make_shared<ssl_socket_t>(this->_io_context, this->_ssl_context))
  , _receive_task(_socket_ptr, this)
  , _send_task(_socket_ptr, this)
{

  try
  {
    this->_ssl_context.use_certificate_file("player_cert.pem",
                                            boost::asio::ssl::context::pem);
    this->_ssl_context.use_private_key_file("player_private_key.pem",
                                            boost::asio::ssl::context::pem);
  }
  catch (const boost::system::system_error& er)
  {
    spdlog::warn("Loading ssl cerificate or key failed: \"{}\"", er.what());
  }
  catch (...)
  {
    spdlog::warn("Loading ssl cerificate or key failed with unexpected error.");
  }

  std::lock_guard lg(this->_socket_mx);
  this->_socket_ptr->set_verify_mode(boost::asio::ssl::verify_none);
}

snake_connection::~snake_connection()
{
  this->close_socket();
  this->join_context_runner();
}

bool
snake_connection::connect(const char* host)
{
  using boost::asio::ip::tcp;

  this->realloc_socket_ptr();
  bool ret = false;

  try
  {
    tcp::resolver resolver(this->_io_context);
    std::lock_guard lg(this->_socket_mx);
    boost::asio::connect(this->_socket_ptr->lowest_layer(),
                         resolver.resolve(host, server_port));
    this->_socket_ptr->handshake(boost::asio::ssl::stream_base::client);

    ret = this->_socket_ptr->lowest_layer().is_open();
  }
  catch (const boost::system::system_error& ex)
  {
    spdlog::error("Connecting to games server failed: \"{}\"", ex.what());
    ret = false;
  }
  catch (...)
  {
    spdlog::error("Connecting to games server failed with unexpected error.");
    ret = false;
  }

  if (ret)
  {
    this->on_socket_connected();
  }

  return ret;
}

void
snake_connection::disconnect()
{
  this->_is_connected = false;
  this->_send_task.stop_task();
  this->close_socket();
}

void
snake_connection::send_data(const snake_send_type& data)
{
  this->_send_task.send_data(data);
}

void
snake_connection::send_signal_with_large_number(signals_to_server signal,
                                                size_t number)
{
  this->_send_task.send_large_number(signal, number);
}

void
snake_connection::update_data_received(const snake_send_type& data)
{
  if (data.front() == static_cast<int8_t>(signals_to_server::clients_count))
  {
    if (data.size() > 1)
    {
      size_t clients_count = this->decode_clients_count(data);
      global_config.on_snake_clients_count_received(clients_count);
    }
  }
  else
  {
    global_config.on_snake_frame_received(data);
  }
}

size_t
snake_connection::decode_clients_count(const snake_send_type& data_clients)
{
  size_t clients_count = 0;
  for (size_t i = 1; i < data_clients.size(); ++i)
  {
    clients_count += data_clients[i];
  }
  return clients_count;
}

bool
snake_connection::is_socket_connected() const
{
  return this->_is_connected;
}

void
snake_connection::close_socket()
{
  this->_io_context.stop();

  boost::system::error_code er;
  std::lock_guard lg(this->_socket_mx);
  this->_socket_ptr->lowest_layer().close(er);
}

void
snake_connection::restart_io_context()
{
  this->join_context_runner();

  this->_io_context_runner =
    std::thread{ [&]()
                 {
                   using work_guard_type = boost::asio::executor_work_guard<
                     boost::asio::io_context::executor_type>;
                   work_guard_type work_guard(_io_context.get_executor());
                   this->_io_context.restart();
                   this->_io_context.run();
                 } };
}

void
snake_connection::join_context_runner()
{
  if (this->_io_context_runner.joinable())
    this->_io_context_runner.join();
}

void
snake_connection::realloc_socket_ptr()
{
  ssl_socket_ptr_t tmp_ptr;
  locked_scope(
    [&]()
    {
      this->_socket_ptr =
        std::make_shared<ssl_socket_t>(this->_io_context, this->_ssl_context);

      tmp_ptr = this->_socket_ptr;
    },
    this->_socket_mx);

  this->_receive_task.replace_socket_ptr(tmp_ptr);
  this->_send_task.replace_socket_ptr(tmp_ptr);
}

void
snake_connection::on_socket_connected()
{
  this->restart_io_context();
  this->_receive_task.start_receive_data();
  this->_send_task.start_task();
  this->_is_connected = true;
}
