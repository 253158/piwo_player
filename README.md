# P.I.W.O. project

## Introduction
The P.I.W.O project is the open-source player for light shows performed of different kinds of buildings.
Historically the project used to make the shows on student dormitories, but there are no limits.
To make the show you need some extra components, but more about it in the project wiki.

## Our CI
You can find piwo\_player binaries for both: linux and windows on our CI build pipelines.
- The build-linux stage contains binary for linux called **Player**.
- The build-win stage contains binary for windows called **Player.exe**.


## Dependencies
To build the project, you need to download all dependecies by yourself.
The required dependecies are:
 - [fmt library](https://github.com/fmtlib/fmt)
 - [RapidJSON library](https://github.com/Tencent/rapidjson)
 - [spdlog library](https://github.com/gabime/spdlog)
 - [mipc library](https://gitlab.com/skn_MOS/piwo/libraries/mipc)
 - [piwo library](https://gitlab.com/skn_MOS/piwo/libraries/libpiwo)

You can check ci-scripts to see how to build them depends on target system.

## Build
### Windows
Probably the simplest way of building the project is to open it with qt-creator.
Mind you, that this hasn't been tested.

### Linux
* Install necessary packages
```bash
apt install qt5-default libqt5opengl5-dev qml-module-qtquick-extras \
            qml-module-qtquick-controls2 qml-module-qtquick-window2 \
	    qml-module-qt-labs-qmlmodels qtbase5-dev
```

## Dev notes
### Running project with clang-analyzer
Project has to be configured with scan-build like (user can also pass other usual defines/options):
```sh
scan-build cmake ..
```

### Running clang-tidy checkers over project
Project has to provide `RUN_CLANG_TIDY=ON` option. So the configuration line would be:
```sh
cmake -DRUN_CLANG_TIDY=ON ..
```

## Generating ssh certificates
This project is also used for communication with a server hosting snake game.\
Communication is encrypted using SSL protocol and server can authenticate **Player**. So to make connection possible
you need to have SSL certificate and a private key. You can generate them using script **generate_ssl_cert** from main project directory.
It will generate certificates and private keys for **Player** and the server. \
In order to use them you need to copy corresponding files to directories where your binary **Player** and server files lie.
To make authentication possible you also need to put **Player's** certificate to folder with server binary file. \
You can also generate files only for **Player** by running script with `--player-only` argument.
Or only for server with `--server-only` argument.

## Additional Links
[Launchpad protocol](https://docs.google.com/document/d/1KvSllQo9GsYYoJ09QXKexN2dBp_79TAcYuCTSapX41o)
[UDP protocol](https://docs.google.com/document/d/1Id8atthziZ64V6xowBAZt2FVeHfraQnJGya-9A8t3F8)
[TCP protocol](https://docs.google.com/document/d/1qdNjDpIlg-bFNPkFcjgMsGNjFXdrhj7mQ_jBvjID3Zw)
[PIWO protocol](https://docs.google.com/document/d/1Id8atthziZ64V6xowBAZt2FVeHfraQnJGya-9A8t3F8)
