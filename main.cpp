#include <fmt/format.h>

#include "config.hpp"
#include "json.hpp"
#include "modinfo.hpp"
#include "ui_iface.hpp"

#include <spdlog/cfg/env.h>
#include <spdlog/spdlog.h>

int
main(int argc, char* argv[])
{
  spdlog::cfg::load_env_levels();
  spdlog::info("P.I.W.O Player");

  start_ui(argc, argv);
}
